#!/usr/bin/env python

"""The setup script."""
import os

from setuptools import setup, find_packages

with open("README.md") as readme_file:
    readme = readme_file.read()

with open("CHANGELOG.md") as history_file:
    history = history_file.read()

requirements = [
    "numpy>=1.26.2",
    "pygame>=2.5.2",
    "scipy>=1.11.4",
    "pytest>=3",
    "pyyaml>=6.0.1",
    "pygame-gui==0.6.9",
    "pydantic>=2.6.3",
    "fastapi>=0.109.2",
    "uvicorn>=0.27.0",
    "websockets>=12.0",
    "requests>=2.31.0",
    "platformdirs>=4.1.0",
    "matplotlib>=3.8.0",
    "pygraphviz>=1.9",
    "pydot>=2.0.0",
    "networkx>=3.2.1",
    "tqdm>=4.65.0",
    "pytimeparse>=1.1.8",
    "networkx",
    "hydra-core",
]

test_requirements = ["pytest>=3", "pytest-cov>=4.1", "httpx"]


def package_files(directory):
    paths = []
    for (path, directories, filenames) in os.walk(os.path.join("cooperative_cuisine", directory)):
        for filename in filenames:
            paths.append(os.path.join('..', path, filename))
    return paths


extra_files = package_files('configs') + package_files('pygame_2d_vis')

setup(
    author="Florian Schröder, Fabian Heinrich",
    author_email="florian.schroeder@uni-bielefeld.de",
    python_requires=">=3.10",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.10",
    ],
    description="The real-time overcooked simulation for a cognitive cooperative system",
    entry_points={
        "console_scripts": ["cooperative-cuisine = cooperative_cuisine.__main__:main"]
    },
    install_requires=requirements,
    license="CC BY-NC-SA 4.0",
    long_description=readme + "\n\n" + history,
    include_package_data=True,
    package_data={'': extra_files, 'rl': extra_files + package_files('reinforcement_learning')},
    keywords=["cooperative_cuisine"],
    name="cooperative_cuisine",
    packages=find_packages(include=["cooperative_cuisine", "cooperative_cuisine.*"]),
    test_suite="tests",
    url="https://gitlab.ub.uni-bielefeld.de/scs/cocosy/cooperative-cuisine",
    version="1.2.0",
    zip_safe=False,
    extras_require={
        "rl": [
            "gymnasium>=0.28.1",
            "stable-baselines3[extra]>=2.2.1",
            "opencv-python>=4.9",
            "wandb>=0.16.3",
        ],
        "test": test_requirements,
    },
)
