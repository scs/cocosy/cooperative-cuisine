import pytest

from cooperative_cuisine.items import ItemInfo, Item, ItemType


@pytest.fixture
def items():
    item1_info = ItemInfo(type=ItemType.Ingredient, name="Tomato")
    item1 = Item("Tomato", item1_info)
    item2_info = ItemInfo(type=ItemType.Ingredient, name="Onion")
    item2 = Item("Onion", item2_info)
    item1_duplicate = Item("Tomato", item1_info)

    return item1, item1_info, item1_duplicate, item2, item2_info


def test_init(items):
    item1, item1_info, _, _, _ = items
    assert item1.name == "Tomato"
    assert item1.item_info == item1_info
    assert item1.progress_equipment is None
    assert item1.progress_percentage == 0.0


def test_repr(items):
    item1, _, _, _, _ = items
    assert str(item1) == "Tomato()"


def test_eq(items):
    item1, _, item1_duplicate, item2, _ = items
    assert item1 == item1_duplicate
    assert item1 != item2


def test_can_combine(items):
    item1, _, _, item2, _ = items
    assert not item1.can_combine(item2)


def test_combine(items):
    item1, _, _, item2, _ = items
    assert item1.combine(item2) is None


def test_progress(items):
    item1, _, _, _, _ = items
    item1.progress_equipment = "Oven"
    item1.progress("Oven", 0.2)
    assert item1.progress_percentage == 0.2
    item1.progress("Oven", 0.1)
    assert item1.progress_percentage == pytest.approx(0.3)
    item1.progress("NotOven", 0.2)
    assert item1.progress_percentage == pytest.approx(0.3)
    assert item1.progress_equipment == "Oven"


def test_reset(items):
    item1, _, _, _, _ = items
    item1.progress_equipment = "Oven"
    item1.progress("Oven", 0.2)
    item1.reset()
    assert item1.progress_equipment is None
    assert item1.progress_percentage == 0.0
