import json
import os
from argparse import ArgumentParser

import yaml

from cooperative_cuisine import ROOT_DIR
from cooperative_cuisine.argument_parser import create_screenshot_parser
from cooperative_cuisine.pygame_2d_vis.drawing import (
    calc_angle,
    Visualizer,
    main,
    save_screenshot,
)
from cooperative_cuisine.pygame_2d_vis.game_colors import RGB, WHITE, colors


def test_colors():
    assert RGB(red=255, green=255, blue=255).hex_format() == "#FFFFFF"
    assert WHITE.hex_format() == "#FFFFFF"
    assert len(colors) >= 552


def test_calc_angle():
    assert calc_angle([0.0, 1.0], [1.0, 0.0]) == -90.0
    assert calc_angle([1.0, 1.0], [1.0, -1.0]) == -90.0


def test_drawing():
    parser = ArgumentParser()
    create_screenshot_parser(parser)
    main(parser.parse_args([]))
    assert os.path.exists(os.path.join(ROOT_DIR, "generated", "screenshot.jpg"))


def test_visualizer():
    with open(ROOT_DIR / "pygame_2d_vis" / "visualization.yaml", "r") as file:
        visualization_config = yaml.safe_load(file)

    vis = Visualizer(visualization_config)

    vis.create_player_colors(10)
    vis.set_grid_size(40)
    
    assert len(vis.player_colors) >= 10
    assert len(set(vis.player_colors)) >= 10

    with open(ROOT_DIR / "pygame_2d_vis" / "sample_state.json", "r") as file:
        state = json.load(file)
    image = vis.get_state_image(state)
    assert image.shape == (360, 520, 3)


def test_drawing_debug():
    with open(ROOT_DIR / "pygame_2d_vis" / "visualization.yaml", "r") as f:
        viz_config = yaml.safe_load(f)

    viz_config["Gui"]["use_player_cook_sprites"] = False
    viz_config["Gui"]["show_interaction_range"] = True
    viz_config["Gui"]["show_counter_centers"] = True

    with open(ROOT_DIR / "pygame_2d_vis" / "sample_state.json", "r") as f:
        state = json.load(f)
    save_screenshot(state, viz_config, ROOT_DIR / "generated" / "screenshot_2.jpg")
    assert os.path.exists(os.path.join(ROOT_DIR, "generated", "screenshot_2.jpg"))
