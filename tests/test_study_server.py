import json
from unittest import mock

from fastapi import status
from fastapi.testclient import TestClient
from requests import Response

import cooperative_cuisine.study_server as study_server_module
from cooperative_cuisine.study_server import app


def test_valid_post_requests():
    test_response = Response()
    test_response.status_code = status.HTTP_200_OK
    test_response.encoding = "utf8"
    test_response._content = json.dumps(
        {
            "player_info": {
                "0": {
                    "player_id": "0",
                    "client_id": "ksjdhfkjsdfn",
                    "player_hash": "shdfbmsndfb",
                    "websocket_url": "jhvfshfvbsdnmf",
                }
            },
            "env_id": "123456789",
            "recipe_graphs": [],
            "kitchen_size": (0, 0),
        }
    ).encode()
    with mock.patch.object(
        study_server_module, "request_game_server", return_value=test_response
    ) as mock_call:
        with TestClient(app) as client:
            res = client.post("/start_study/124/1")

    assert res.status_code == status.HTTP_200_OK

    mock_call.assert_called_once()

    with mock.patch.object(
        study_server_module, "request_game_server", return_value=test_response
    ) as mock_call:
        with TestClient(app) as client:
            res = client.post("/get_game_connection/124")

    assert res.status_code == status.HTTP_200_OK
    assert res.json()["player_info"] == {
        "0": {
            "player_id": "0",
            "client_id": "ksjdhfkjsdfn",
            "player_hash": "shdfbmsndfb",
            "websocket_url": "jhvfshfvbsdnmf",
        }
    }

    with mock.patch.object(
        study_server_module, "request_game_server", return_value=test_response
    ) as mock_call:
        with TestClient(app) as client:
            res = client.post("/level_done/124")

    assert res.status_code == status.HTTP_200_OK


def test_invalid_post_requests():
    test_response = ""
    with mock.patch.object(
        study_server_module, "request_game_server", return_value=test_response
    ) as mock_call:
        with TestClient(app) as client:
            res = client.post("/level_done/125")

    assert res.status_code == status.HTTP_409_CONFLICT

    with mock.patch.object(
        study_server_module, "request_game_server", return_value=test_response
    ) as mock_call:
        with TestClient(app) as client:
            res = client.post("/get_game_connection/125")

    assert res.status_code == status.HTTP_409_CONFLICT


def test_game_server_crashed():
    test_response = Response()
    test_response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR

    with mock.patch.object(
        study_server_module, "request_game_server", return_value=test_response
    ) as mock_call:
        with TestClient(app) as client:
            res = client.post("/start_study/124/1")

    assert res.status_code == status.HTTP_500_INTERNAL_SERVER_ERROR


def test_tutorial():
    test_response = Response()
    test_response.status_code = status.HTTP_200_OK
    test_response.encoding = "utf8"
    test_response._content = json.dumps(
        {
            "player_info": {
                "0": {
                    "player_id": "0",
                    "client_id": "ksjdhfkjsdfn",
                    "player_hash": "shdfbmsndfb",
                    "websocket_url": "jhvfshfvbsdnmf",
                }
            },
            "env_id": "123456789",
            "recipe_graphs": [],
            "kitchen_size": (0, 0),
        }
    ).encode()
    with mock.patch.object(
        study_server_module, "request_game_server", return_value=test_response
    ) as mock_call:
        with TestClient(app) as client:
            res = client.post("/connect_to_tutorial/124")

    assert res.status_code == status.HTTP_200_OK

    mock_call.assert_called_once()

    with mock.patch.object(
        study_server_module, "request_game_server", return_value=test_response
    ) as mock_call:
        with TestClient(app) as client:
            res = client.post("/disconnect_from_tutorial/124")

    assert res.status_code == status.HTTP_200_OK


# TOOD test bots
