import numpy as np

from cooperative_cuisine.counters import ServingWindow, Dispenser
from cooperative_cuisine.hooks import Hooks
from cooperative_cuisine.items import Item, Plate, ItemInfo, ItemType
from cooperative_cuisine.utils import create_init_env_time


def test_serving_window():
    class DummyOrderManager:
        def serve_meal(self, item, env_time, player) -> bool:
            return (isinstance(item, str) and item == "Test123") or item.content_list[
                0
            ].name == "TestMeal"

    class DummyPlateDispenser:
        plate_received = False

        def update_plate_out_of_kitchen(self, env_time):
            self.plate_received = True

    plate_dispenser = DummyPlateDispenser()

    serving_window = ServingWindow(
        order_manager=DummyOrderManager(),
        meals={"TestMeal", "TestMeal2"},
        env_time_func=create_init_env_time,
        plate_dispenser=plate_dispenser,
        pos=np.array([1.0, 1.0]),
        hook=Hooks(None),
    )

    serving_window.drop_off(item="Test123")
    assert (
        plate_dispenser.plate_received
    ), "ServingWindow needs to update plate out of kitchen for ordered meal."
    plate_dispenser.plate_received = False
    plate = Plate(transitions={}, clean=True, item_info=None, hook=Hooks(None))
    plate.content_list = [Item(name="TestMeal", item_info=None)]
    assert serving_window.can_drop_off(
        item=plate
    ), "ServingWindow could drop off a known meal."
    assert (
        serving_window.drop_off(item=plate) is None
    ), "ServingWindow drop_off should return None for a served meal."
    assert (
        plate_dispenser.plate_received
    ), "ServingWindow needs to update plate out of kitchen for ordered meal."
    plate_dispenser.plate_received = False

    plate.content_list = [Item(name="TestMeal2", item_info=None)]
    assert serving_window.can_drop_off(
        item=plate
    ), "ServingWindow could drop off a known meal."
    assert (
        serving_window.drop_off(item=plate) == plate
    ), "ServingWindow should return the item for not ordered meals."

    assert (
            serving_window.pick_up() is None
    ), "Player should not be able to pick something from the ServingWindow."


def test_dispenser():
    dispenser = Dispenser(
        dispensing=ItemInfo(
            type=ItemType.Ingredient,
            name="MyIngredient",
            seconds=0,
            needs=["MySecondIngredient"],
            equipment=None,
        ),
        pos=np.array([1.0, 1.0]),
        hook=Hooks(None),
        undo_dispenser_pickup=False,
    )
    assert (
        dispenser.occupied_by.name == "MyIngredient"
    ), "Initialized dispenser should be occupied by dispensing item"
    assert (
            dispenser.pick_up().name == "MyIngredient"
    ), "Picked up item should be the dispensing item"
    assert (
        dispenser.occupied_by is not None
    ), "After pickup a new occupied by should be generated"
    assert (
        dispenser.occupied_by.name == "MyIngredient"
    ), "After pick up a new occupied by item should be generated"

    assert not dispenser.can_drop_off(
        dispenser.pick_up()
    ), "Config undo_dispenser_pickup==False should stop the player to drop off picked up items"

    dispenser.undo_dispenser_pickup = True
    assert dispenser.can_drop_off(
        dispenser.pick_up()
    ), "Config undo_dispenser_pickup==True should allow the player to drop off picked up items"
    assert (
            dispenser.drop_off(dispenser.pick_up()) is None
    ), "Config undo_dispenser_pickup==True should allow the player to drop off picked up items"
    # check combine?
