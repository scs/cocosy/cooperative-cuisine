import pytest

from cooperative_cuisine.hooks import Hooks
from cooperative_cuisine.items import ItemInfo, ItemType
from cooperative_cuisine.utils import create_init_env_time
from cooperative_cuisine.orders import DeterministicOrderGeneration, Order

from datetime import timedelta
from random import Random


def check_orders(init_orders_gt, now, orders):
    assert len(init_orders_gt) == len(orders)
    for o in orders:
        assert o.meal.name in [_o[0] for _o in init_orders_gt]
        assert any([True for _o in init_orders_gt if
                    DeterministicOrderGeneration.parse_timedelta(_o[2]) == o.max_duration and o.meal.name == _o[
                        0] and now == o.start_time])


def create_item_info_from_kwargs(kwargs):
    return [
        ItemInfo(ItemType.Meal, name=orders[0]) for orders in kwargs["timed_orders"]
    ]


@pytest.mark.parametrize("timedelta_in, correct_seconds", [("0:00", 0), ("0:10", 10), ("1:02", 62), ("1:05", 65), ("4:10:03", 4 * (60**2) + 10 * 60 + 3), (2, 2), (3.5, 3.5)])
def test_parse_timedelta(timedelta_in, correct_seconds):
    assert DeterministicOrderGeneration.parse_timedelta(timedelta_in) == timedelta(seconds=correct_seconds)


@pytest.mark.parametrize("kwargs", [
    {"timed_orders": [["TomatoSoup", "0:00", "0:11"], ["OnionSoup", "0:00", "0:12"], ["TomatoSoup", "0:10", "0:09"], ["TomatoSoup", "0:15", "0:01"]]},
    {"timed_orders": [["TomatoSoup", "0:10", "0:11"], ["OnionSoup", "0:00", "0:12"], ["OnionSoup", "5:01:10", "0:09"], ["TomatoSoup", "0:15", "0:01"]]},
    {"timed_orders": [["TomatoSoup", "3:40:10", "0:11"], ["OnionSoup", "0:00", "0:12"], ["OnionSoup", "5:01:10", "0:09"], ["TomatoSoup", "0:00", "0:01"]]},
    {"timed_orders": [["TomatoSoup", "3:40:10", "0:11"], ["OnionSoup", "10:00", "0:12"], ["OnionSoup", "5:01:10", "0:09"], ["TomatoSoup", "10:00", "0:01"]]},
    {"timed_orders": [["TomatoSoup", "3:40:10", "0:11"], ["OnionSoup", "10:00", "0:12"], ["OnionSoup", "5:01:10", "0:09"], ["TomatoSoup", "5:01:10", "0:10"]]},
])
def test_deterministic_orders(kwargs):
    deterministic_order_gen = DeterministicOrderGeneration(Hooks(None), Random(), **{"kwargs": kwargs})
    now = create_init_env_time()
    deterministic_order_gen.available_meals = create_item_info_from_kwargs(kwargs)
    orders = deterministic_order_gen.init_orders(now)

    init_orders_gt = [o for o in kwargs["timed_orders"] if DeterministicOrderGeneration.parse_timedelta(o[1]) == timedelta(seconds=0)]

    check_orders(init_orders_gt, now, orders)

    other_orders_gt = [o for o in kwargs["timed_orders"] if DeterministicOrderGeneration.parse_timedelta(o[1]) != timedelta(seconds=0)]
    other_orders_gt = sorted(other_orders_gt, key=lambda o: DeterministicOrderGeneration.parse_timedelta(o[1]))
    steps = [DeterministicOrderGeneration.parse_timedelta(o[1]) for o in other_orders_gt]
    gathered_orders = []
    if steps:
        last = timedelta(seconds=0)
        for i, step in enumerate(steps):
            gathered_orders.append(other_orders_gt[i])
            if i + 1 == len(steps) or steps[i+1] != step:
                n_orders = deterministic_order_gen.get_orders(passed_time=step-last, now=now+step, new_finished_orders=[], expired_orders=[])
                check_orders(gathered_orders, now + step, n_orders)
                gathered_orders = []
            last = step


@pytest.mark.parametrize("kwargs", [
    {"timed_orders": [["TomatoSoup", "0:00", "0:11"], ["TomatoSoup", "0:01", "0:11"], ["OnionSoup", "0:03", "0:12"], ["TomatoSoup", "0:10", "0:09"], ["TomatoSoup", "0:15", "0:01"]], "never_no_order": True},
    {"timed_orders": [["TomatoSoup", "0:00", "0:11"], ["TomatoSoup", "0:01", "0:11"], ["OnionSoup", "0:03", "0:12"], ["TomatoSoup", "0:10", "0:09"], ["TomatoSoup", "0:15", "0:01"]], "never_no_order": True, "never_no_order_update_all_remaining": True},
])
def test_never_no_order(kwargs):
    deterministic_order_gen = DeterministicOrderGeneration(Hooks(None), Random(), **{"kwargs": kwargs})
    now = create_init_env_time()
    deterministic_order_gen.available_meals = create_item_info_from_kwargs(kwargs)
    orders = deterministic_order_gen.init_orders(now)
    assert len(orders) == 1
    starts = [timedelta(seconds=1), timedelta(seconds=3), timedelta(seconds=10),timedelta(seconds=15)]
    o = deterministic_order_gen.get_orders(passed_time=starts[0], now=now + starts[0], new_finished_orders=[],
                                       expired_orders=[])
    assert len(o) == 1
    o2 = deterministic_order_gen.get_orders(passed_time=timedelta(seconds=1), now=now + timedelta(seconds=2), new_finished_orders=orders + o,
                                           expired_orders=[])
    # only 2 seconds passed, but because no order is active, the second is antedate.
    assert len(o2) == 1
    if "never_no_order_update_all_remaining" in kwargs and kwargs["never_no_order_update_all_remaining"]:
        assert all([q.start == now + starts[i+2] - timedelta(seconds=1) for i, q in enumerate(deterministic_order_gen.current_queue)])
    else:
        assert all([q.start == now + starts[i+2] for i, q in enumerate(deterministic_order_gen.current_queue)])


@pytest.mark.parametrize("kwargs", [
    {"timed_orders": [], "never_no_order": True},
    {"timed_orders": [], "never_no_order": False},
])
def test_no_timed_orders(kwargs):
    deterministic_order_gen = DeterministicOrderGeneration(Hooks(None), Random(), **{"kwargs": kwargs})
    now = create_init_env_time()
    deterministic_order_gen.available_meals = create_item_info_from_kwargs(kwargs)
    orders = deterministic_order_gen.init_orders(now)
    assert len(orders) == 0
    orders = deterministic_order_gen.get_orders(passed_time=timedelta(seconds=10), now=now + timedelta(seconds=10), new_finished_orders=[], expired_orders=[])
    assert len(orders) == 0
    orders = deterministic_order_gen.get_orders(passed_time=timedelta(seconds=15), now=now + timedelta(seconds=15), new_finished_orders=[], expired_orders=[])
    assert len(orders) == 0
