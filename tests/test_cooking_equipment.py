import pytest

from cooperative_cuisine.hooks import Hooks
from cooperative_cuisine.items import ItemInfo, CookingEquipment, Item, ItemType


def test_can_combine_single_other_item():
    """Test the 'can_combine' method with single other item"""

    item_info = ItemInfo(type=ItemType.Meal, name="Soup", seconds=5.0)
    cooking_equipment = CookingEquipment(
        transitions={}, name="Pot", item_info=item_info, hook=Hooks(None)
    )
    other_item = Item("Onion", ItemInfo(type=ItemType.Ingredient, name="Onion"))

    assert cooking_equipment.can_combine(other_item) == False


def test_can_combine_list_of_other_items():
    """Test the 'can_combine' method with list of other items"""

    item_info = ItemInfo(type=ItemType.Meal, name="Soup", seconds=5.0)
    cooking_equipment = CookingEquipment(
        transitions={}, name="Pot", item_info=item_info, hook=Hooks(None)
    )
    other_item = CookingEquipment(
        name="Pan",
        transitions={},
        item_info=ItemInfo(type=ItemType.Equipment, name="Pan"),
        hook=Hooks(None)
    )

    assert cooking_equipment.can_combine(other_item) == False


def test_can_combine_without_other_item():
    """Test the 'can_combine' method without other item"""

    item_info = ItemInfo(type=ItemType.Meal, name="Soup", seconds=5.0)
    cooking_equipment = CookingEquipment(
        transitions={}, name="Pot", item_info=item_info, hook=Hooks(None)
    )

    assert cooking_equipment.can_combine(None) == False


def test_combine():
    """Test the 'combine' method"""

    item_info = ItemInfo(type=ItemType.Meal, name="Soup", seconds=5.0)
    cooking_equipment = CookingEquipment(
        transitions={}, name="Pot", item_info=item_info, hook=Hooks(None)
    )
    other_item = Item("Onion", ItemInfo(type=ItemType.Ingredient, name="Onion"))

    assert cooking_equipment.combine(other_item) is None


def test_progress():
    """Test the 'progress' method"""

    item_info = ItemInfo(type=ItemType.Meal, name="Soup", seconds=5.0)
    cooking_equipment = CookingEquipment(
        transitions={}, name="Pot", item_info=item_info, hook=Hooks(None)
    )
    result = Item(name="TestResult", item_info=None)
    cooking_equipment.active_transition = {
        "seconds": 5.0,
        "result": result,
    }
    from datetime import datetime, timedelta

    cooking_equipment.progress(passed_time=timedelta(seconds=5.0), now=datetime.now())

    assert cooking_equipment.content_list == [result]


def test_reset_content():
    """Test the 'reset_content' method"""

    item_info = ItemInfo(type=ItemType.Meal, name="Soup", seconds=5.0)
    cooking_equipment = CookingEquipment(
        transitions={}, name="Pot", item_info=item_info, hook=Hooks(None)
    )
    cooking_equipment.content_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    cooking_equipment.reset_content()

    assert cooking_equipment.content_list == []


def test_release():
    """Test the 'release' method"""

    item_info = ItemInfo(type=ItemType.Meal, name="Soup", seconds=5.0)
    cooking_equipment = CookingEquipment(
        transitions={}, name="Pot", item_info=item_info, hook=Hooks(None)
    )

    cooking_equipment.content_list = ["Content1", "Content2"]

    assert cooking_equipment.release() == ["Content1", "Content2"]
    assert cooking_equipment.content_list == []


def test_extra_repr_without_content():
    """Test the 'extra_repr' method without content"""

    item_info = ItemInfo(type=ItemType.Meal, name="Soup", seconds=5.0)
    cooking_equipment = CookingEquipment(
        transitions={}, name="Pot", item_info=item_info, hook=Hooks(None)
    )

    assert cooking_equipment.extra_repr == "[], None"


def test_extra_repr_with_content():
    """Test the 'extra_repr' method with content"""

    item_info = ItemInfo(type=ItemType.Meal, name="Soup", seconds=5.0)
    cooking_equipment = CookingEquipment(
        transitions={}, name="Pot", item_info=item_info, hook=Hooks(None)
    )

    item_1 = Item(
        name="Tomato", item_info=ItemInfo(type=ItemType.Ingredient, name="Tomato")
    )
    item_2 = Item(
        name="Potato", item_info=ItemInfo(type=ItemType.Ingredient, name="Potato")
    )
    cooking_equipment.content_list.extend([item_1, item_2])

    assert cooking_equipment.extra_repr == "[Tomato(), Potato()], None"


def test_get_potential_meal_without_content():
    """Test the 'get_potential_meal' method without content"""

    item_info = ItemInfo(type=ItemType.Meal, name="Soup", seconds=5.0)
    cooking_equipment = CookingEquipment(
        transitions={}, name="Pot", item_info=item_info, hook=Hooks(None)
    )

    assert cooking_equipment.get_potential_meal() is None


def test_get_potential_meal_with_content():
    """Test the 'get_potential_meal' method with content"""

    item_info = ItemInfo(type=ItemType.Meal, name="Soup", seconds=5.0)
    cooking_equipment = CookingEquipment(
        transitions={}, name="Pot", item_info=item_info, hook=Hooks(None)
    )

    item_1 = Item(
        name="Tomato", item_info=ItemInfo(type=ItemType.Ingredient, name="Tomato")
    )
    cooking_equipment.content_list.append(item_1)

    assert cooking_equipment.get_potential_meal() == item_1

    item_2 = Item(
        name="TomatoSoup", item_info=ItemInfo(type=ItemType.Meal, name="TomatoSoup")
    )
    cooking_equipment.content_ready = item_2
    assert cooking_equipment.get_potential_meal() == item_2


@pytest.fixture
def cooking_equipment():
    item_info = ItemInfo(type=ItemType.Meal, name="Soup", seconds=5.0)
    return CookingEquipment(transitions={}, name="Pot", item_info=item_info, hook=Hooks(None))


def test_reset(cooking_equipment):
    """Test the 'reset' method"""

    cooking_equipment.active_transition = {"1": 2}
    cooking_equipment.progress_percentage = 1.0
    cooking_equipment.progress_equipment = "Here"

    cooking_equipment.reset()

    assert cooking_equipment.progress_percentage == 0.0
    assert cooking_equipment.active_transition is None
    assert cooking_equipment.progress_equipment is None


# TODO full transition test with combine, progress etc.
