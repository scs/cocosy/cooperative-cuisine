<div align="center">
![Main Pipeline Report](https://gitlab.ub.uni-bielefeld.de/scs/cocosy/cooperative-cuisine/badges/main/pipeline.svg?key_text=Main+Pipeline&key_width=82)
![Main Coverage Report](https://gitlab.ub.uni-bielefeld.de/scs/cocosy/cooperative-cuisine/badges/main/coverage.svg?job=pytest&key_text=Main+Test+Coverage&key_width=114)
![Dev Pipeline Report](https://gitlab.ub.uni-bielefeld.de/scs/cocosy/cooperative-cuisine/badges/dev/pipeline.svg?key_text=Dev+Pipeline&key_width=80)
![Dev Coverage Report](https://gitlab.ub.uni-bielefeld.de/scs/cocosy/cooperative-cuisine/badges/dev/coverage.svg?job=pytest&key_text=Dev+Test+Coverage&key_width=112)
</div>

# Cooperative Cuisine Environment

The overcooked-like cooperative cuisine environment for real-time human cooperative interactions and artificial agents.

For an extensive introduction, have a look at
the [Documentation](https://scs.pages.ub.uni-bielefeld.de/cocosy/cooperative-cuisine).

<div align="center">
<img src="cooperative_cuisine/pygame_2d_vis/images/cooperative_cuisine.png"  width="800">
</div>

## Installation
You have two options to install the environment. Either clone it and install it locally or install it in your
site-packages.
You need a Python 3.10 or newer environment conda environment.

### Local Editable Installation
In your `repo`, `PyCharmProjects` or similar directory with the correct environment active:

```bash
conda install -c conda-forge pygraphviz
git clone https://gitlab.ub.uni-bielefeld.de/scs/cocosy/cooperative-cuisine.git
cd cooperative-cuisine
pip install -e .
```

#### Run

Run it via the command line (in your pyenv/conda environment):

```bash
cooperative-cuisine start -s localhost -sp 8080 -g localhost -gp 8000
```

*The arguments shown are the defaults.*

You can also start the **Game Server**m **Study Server** (Matchmaking),and the **PyGame GUI** individually in different
terminals.

```bash
cooperative-cuisine game-server -g localhost -gp 8000 --manager-ids SECRETKEY1 SECRETKEY2

cooperative-cuisine study-server -s localhost -sp 8080 -g localhost -gp 8000 --manager-ids SECRETKEY1

cooperative-cuisine gui -s localhost -sp 8080 -g localhost -gp 8000
```

You can start also several GUIs. The study server does the matchmaking.

### Library Installation
The correct environment needs to be active:

```bash
pip install cooperative_cuisine@git+https://gitlab.ub.uni-bielefeld.de/scs/cocosy/cooperative-cuisine@main
```

#### Run
You can now use the environment and/or simulator in your python code. Just by importing
it `import cooperative_cuisine`

## Configuration
The environment configuration is currently done with 3 config files + GUI configuration.

### Item Config
The item config defines which ingredients, cooking equipment and meals can exist and how meals and processed ingredients
can be cooked/created.

### Layout Config
You can define the layout of the kitchen via a layout file. The position of counters are based on a grid system, even
when the players do not move grid steps but continuous steps. Each character defines a different type of counter. Which
character is mapped to which counter is defined in the Environment config.

### Environment Config
The environment config defines how a level/environment is defined. Here, the available plates, meals, order and player
configuration is done.

### Study Config
When starting a study, the study config holds all the information about the levels (layouts, dishes, orders). In the argument_parser.py it can be chosen whether the 
orders should in appear in a random ordering or whether their schedule should be pre-defined. 

### PyGame Visualization Config
Here the visualisation for all objects is defined. Reference the images or define a list of base shapes that represent
the counters, ingredients, meals and players.

## Reinforcement Learning
- Vector/Simple representation and small model sizes for single agent RL is done with [sb3](https://stable-baselines3.readthedocs.io/en/master/). You can find additional information in the corresponding folder, [here](cooperative_cuisine/reinforcement_learning/README.md).
- You can find single agent RL with Dreamer and FuN models (based on images) [here](https://gitlab.ub.uni-bielefeld.de/scs/cocosy/rl-with-dreamer-and-fun).


## Troubleshooting

```cannot open shared object file: No such file or directory (search paths /usr/lib/x86_64-linux-gnu/dri:\$${ORIGIN}/dri:/usr/lib/dri, suffix _dri)```
if you have a conda environment:

```bash
conda install -c conda-forge libstdcxx-ng
```

## License
Cooperative Cuisine © 2024 by [Social Cognitive Systems Group](https://scs.techfak.uni-bielefeld.de/) is licensed under [`CC BY-NC-SA 4.0`](https://creativecommons.org/licenses/by-nc-sa/4.0/?ref=chooser-v1) [![License: CC BY-NC-SA 4.0](https://licensebuttons.net/l/by-nc-sa/4.0/80x15.png)](https://creativecommons.org/licenses/by-nc-sa/4.0/)
