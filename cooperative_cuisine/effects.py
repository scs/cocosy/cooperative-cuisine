"""
Until now, only the fire effect is managed here.

Fire spreads after some time to other counter.

"""

from __future__ import annotations

from collections import deque
from datetime import timedelta, datetime
from random import Random
from typing import TYPE_CHECKING, Tuple

from cooperative_cuisine.hooks import Hooks, NEW_FIRE, FIRE_SPREADING, FIRE_EXTINGUISHED
from cooperative_cuisine.items import (
    ItemInfo,
    Item,
    ItemType,
    CookingEquipment,
    Effect,
)
from cooperative_cuisine.utils import get_touching_counters, find_item_on_counters

if TYPE_CHECKING:
    from cooperative_cuisine.counters import Counter


# the effect class is located in the items.py


class EffectManager:
    """The EffectManager class is responsible for managing effects in an environment.

    It provides methods to add effects, set counters, register new active effects, check if an effect is active,
    remove active effects, and progress the manager.
    """

    def __init__(self, hook: Hooks, random: Random) -> None:
        """Constructor for EffectManager.

        Args:
            hook: An instance of the Hooks class representing the hooks in the environment.
            random: An instance of the Random class representing the random number generator.
        """
        self.effects: list[ItemInfo] = []
        """A list of ItemInfo objects representing the effects managed by the manager."""
        self.counters: list[Counter] = []
        """A list of Counter objects representing the counters in the environment."""
        self.hook: Hooks = hook
        """An instance of the Hooks class representing the hooks in the environment."""
        self.new_effects: list[Tuple[Effect, Item | Counter]] = []
        """A list of tuples containing an Effect object and either an Item or Counter object representing the new 
        active effects."""
        self.random: Random = random
        """An instance of the Random class representing the random number generator."""

    def add_effect(self, effect: ItemInfo):
        """Add the effect (item) info that the manager handles.

        Args:
            effect: An object of type ItemInfo representing the effect to be added.

        """
        self.effects.append(effect)

    def set_counters(self, counters: list[Counter]):
        """Sets the counters of the environment.

        Args:
            counters: A list of Counter objects representing the counters.
        """
        self.counters = counters.copy()

    def register_active_effect(self, effect: Effect, target: Item | Counter):
        """Register new active effects on a target item or counter.

        Args:
            effect: An instance of the Effect class representing the active effect to be registered.
            target: An instance of either the Item or Counter class to which the effect will be applied.

        """
        target.active_effects.append(effect)
        self.new_effects.append((effect, target))

    def progress(self, passed_time: timedelta, now: datetime):
        """Iterative progress call on the manager.

        Similar to the `Counter.progress` method.
        How often it is called depends on the `env_step_frequency` or how often `step` is called.

        Args:
            passed_time: The amount of time that has passed since the last call of the method.
            now: The current env time.
        """
        ...

    @staticmethod
    def effect_is_active(effect: ItemInfo, target: Item | Counter) -> bool:
        """Checks if a given effect is active on a specified target.

        Args:
            effect: An instance of ItemInfo representing the effect to check.
            target: An instance of Item or Counter representing the target on which the effect is being checked.

        Returns:
            A boolean value indicating whether the effect is active on the target. True if active, False otherwise.

        """
        return effect.name in [e.name for e in target.active_effects]

    def remove_active_effect(self, effect: Effect, target: Item | Counter, player: str):
        """Removes an active effect from a target item or counter.

        Args:
            effect (Effect): The effect to be removed.
            target (Item | Counter): The target item or counter from which the effect will be removed.

        """
        ...


class FireEffectManager(EffectManager):
    """A class representing a manager for fire effects."""

    def __init__(
        self,
        spreading_duration: list[float],
        fire_burns_ingredients_and_meals: bool,
        **kwargs,
    ):
        super().__init__(**kwargs)
        self.spreading_duration: list[float] = spreading_duration
        """A list of two floats representing the minimum and maximum duration for the fire effect to spread."""
        self.fire_burns_ingredients_and_meals: bool = fire_burns_ingredients_and_meals
        """A boolean indicating whether the fire burns ingredients and meals."""
        self.effect_to_timer: dict[str:datetime] = {}
        """A dictionary mapping effect uuids to their corresponding timers."""
        self.next_finished_timer: datetime = datetime.max
        """A datetime representing the time when the next effect will finish."""
        self.active_effects: list[Tuple[Effect, Item | Counter]] = []
        """A list of tuples representing the active effects and their target items or counters."""

    def progress(self, passed_time: timedelta, now: datetime):
        """Check if new effects occurred and apply them and check if fire spread and then create new fire effects.

        Args:
            passed_time: A timedelta representing the amount of time that has passed since the last progress update.
            now: A datetime representing the current time.

        """
        if self.new_effects:
            # new effects since the last progress call
            for effect, target in self.new_effects:
                # update next fire spreading
                self.effect_to_timer[effect.uuid] = now + timedelta(
                    seconds=self.random.uniform(*self.spreading_duration)
                )
                self.next_finished_timer = min(
                    self.next_finished_timer, self.effect_to_timer[effect.uuid]
                )
                self.hook(NEW_FIRE, target=target)
                self.active_effects.append((effect, target))
            # reset new effects
            self.new_effects = []
        if self.next_finished_timer <= now:
            for effect, target in self.active_effects:
                if self.effect_to_timer[effect.uuid] <= now:
                    if isinstance(target, Item):
                        target = find_item_on_counters(target.uuid, self.counters)
                    if target:
                        touching = get_touching_counters(target, self.counters)
                        for counter in touching:
                            if counter.occupied_by:
                                if isinstance(counter.occupied_by, deque):
                                    # only the top object on a plate stack is burning
                                    self.apply_effect(effect, counter.occupied_by[-1])
                                else:
                                    self.apply_effect(effect, counter.occupied_by)
                            else:
                                self.apply_effect(effect, counter)
                    self.effect_to_timer[effect.uuid] = now + timedelta(
                        seconds=self.random.uniform(*self.spreading_duration)
                    )
            if self.effect_to_timer:
                self.next_finished_timer = min(self.effect_to_timer.values())
            else:
                self.next_finished_timer = datetime.max

    def apply_effect(self, effect: Effect, target: Item | Counter):
        """
        Apply an effect to a target item or counter.

        Args:
            effect: The effect to apply.
            target: The target item or counter to apply the effect to.

        """
        if (
            isinstance(target, Item)
            and target.item_info.type == ItemType.Tool
            and effect.name in target.item_info.needs
        ):
            # Tools that reduce fire can not burn
            return
        if effect.name not in target.active_effects and target.uuid not in [
            t.uuid for _, t in self.active_effects
        ]:
            self.hook(FIRE_SPREADING, target=target)
            if isinstance(target, CookingEquipment):
                if target.content_list:
                    for content in target.content_list:
                        self.burn_content(content)
                    if self.fire_burns_ingredients_and_meals:
                        self.burn_content(target.content_ready)
            elif isinstance(target, Item):
                self.burn_content(target)
            self.register_active_effect(
                Effect(effect.name, item_info=effect.item_info), target
            )

    def burn_content(self, content: Item):
        """Add the prefix "Burnt" to the content name.

        Args:
            content (Item): The content to be burned.
        """
        if self.fire_burns_ingredients_and_meals and content:
            if not content.name.startswith("Burnt"):
                content.name = "Burnt" + content.name

    def remove_active_effect(self, effect: Effect, target: Item | Counter, player: str):
        """Remove the fire effect from a target item or counter.

        Args:
            effect: The effect to be removed.
            target: The item or counter associated with the effect.

        """
        if (effect, target) in self.active_effects:
            self.hook(FIRE_EXTINGUISHED, target=target, player=player)
            self.active_effects.remove((effect, target))
        if effect.uuid in self.effect_to_timer:
            del self.effect_to_timer[effect.uuid]
