"""
# Usage
- Set `CONNECT_WITH_STUDY_SERVER` in gui.py to True.
- Run this script. Copy the manager id that is printed
- Run the game_server.py script with the manager id copied from the terminal
```
python game_server.py --manager-ids COPIED_UUID
```
- Run 2 gui.py scripts in different terminals. For more players change `NUMBER_PLAYER_PER_ENV` and start more guis.

The environment starts when all players connected.
"""
import argparse
import asyncio
import json
import logging
import os
import signal
import subprocess
import uuid
from pathlib import Path
from subprocess import Popen
from typing import Tuple, Any

import requests
import uvicorn
import yaml
from fastapi import FastAPI, HTTPException, Request
from pydantic import BaseModel

from cooperative_cuisine import ROOT_DIR
from cooperative_cuisine.argument_parser import create_study_server_parser
from cooperative_cuisine.environment import EnvironmentConfig
from cooperative_cuisine.game_server import CreateEnvironmentConfig, EnvironmentData
from cooperative_cuisine.server_results import PlayerInfo, CreateEnvResult
from cooperative_cuisine.utils import (
    expand_path,
    deep_update,
    UUID_CUTOFF, load_config_files,
)

log = logging.getLogger(__name__)
"""The logger for this module."""


app = FastAPI()
"""The FastAPI app that runs the study server."""

USE_AAAMBOS_AGENT = False
"""Use the aaambos random agents instead of the simpler python script agents."""


def request_game_server(game_server: str, json_data: dict):
    return requests.post(game_server, json=json_data)


class LevelConfig(BaseModel):
    """Configuration of a level in the study."""

    name: str
    """Name of the level."""
    config_path: str
    """Path to the environment config file."""
    layout_path: str
    """Path to the layout file."""
    item_info_path: str
    """Path to the item info file."""
    config_overwrite: dict[str, Any] | None = None
    """Overwrite parts of the `environment_config`"""
    seed: int
    """Seed for the level."""


class LevelInfo(BaseModel):
    """Information for the clients about a level."""

    name: str
    """Name of the level."""
    last_level: bool
    """If the level is the last in the study."""
    recipe_graphs: list[dict]
    """Graph representations for the recipes in this level."""
    number_players: int
    """Number of players in this level."""
    kitchen_size: tuple[float, float]
    """Size of the created env of this level."""


class StudyConfig(BaseModel):
    """Configuration of the study."""

    levels: list[LevelConfig]
    """List of levels which the study goes through."""
    num_players: int
    """Number of players per study."""
    num_bots: int
    """Number of bots which are added to the study."""


class Study:
    def __init__(self, study_config_path: str | Path, game_url: str, game_port: int):
        """Constructor of Study.

        Args:
            study_config_path: The file path or Path object of the study configuration file.
            game_url: The URL of the game server.
            game_port: The port number of the game server.
        """
        with open(study_config_path, "r") as file:
            study_config_f = file.read()

        self.study_id = uuid.uuid4().hex[:UUID_CUTOFF]
        """Unique ID of the study."""

        self.study_config: StudyConfig = yaml.load(
            str(study_config_f), Loader=yaml.Loader
        )
        """Configuration for the study which layouts, env_configs and item infos are used for the study levels."""
        self.levels: list[dict] = self.study_config["levels"]
        """List of level configs for each of the levels which the study runs through."""
        self.current_level_idx: int = 0
        """Counter of which level is currently run in the config."""
        self.participant_id_to_player_info: dict[str, dict[str, PlayerInfo]] = {}
        """A dictionary which maps participants to player infos."""
        self.num_connected_players: int = 0
        """Number of currently connected players."""

        self.current_running_env: CreateEnvResult | None = None
        """Information about the current running environment."""
        self.participants_done: dict[str, bool] = {}
        """A dictionary which saves which player has sent ready."""
        self.current_config: dict | None = None
        """Save current environment config"""

        self.use_aaambos_agent: bool = USE_AAAMBOS_AGENT
        """Use aaambos-agents or simple python scripts."""

        """Use aaambos-agents or simple python scripts."""
        self.bot_websocket_url: str = f"ws://{game_url}:{game_port}/ws/player/"
        """The websocket url for the bots to use."""
        self.sub_processes: list[Popen] = []
        """Save subprocesses of the bots to be able to kill them afterwards."""

    @property
    def study_done(self) -> bool:
        """Is the study done."""
        return self.current_level_idx >= len(self.levels)

    @property
    def last_level(self) -> bool:
        """Is it the last level that is played?"""
        return self.current_level_idx >= len(self.levels) - 1

    @property
    def is_full(self) -> bool:
        """Can the study start? No new players can join."""
        return (
            len(self.participant_id_to_player_info) == self.study_config["num_players"]
        )

    def can_add_participants(self, num_participants: int) -> bool:
        """Checks whether the number of participants fit in this study.

        Args:
            num_participants: Number of participants wished to be added.

        Returns:
            True of the participants fit in this study, False if not.
        """
        filled = (
            self.num_connected_players + num_participants
            <= self.study_config["num_players"]
        )
        return filled and not self.is_full

    def create_env(self, level: dict) -> EnvironmentData:
        """Creates/starts an environment on the game server,
        given the configuration file paths specified in the level.

        Args:
            level: dict with LevelConfig content which contains the paths to the env config, layout and item info files.

        Returns:
            EnvironmentData which contains information about the newly created environment.

        Raises:
            ValueError if the gameserver returned a conflict, HTTPError with 500 if the game server crashes.
        """
        level = LevelConfig(**level)
        item_info_path = expand_path(level.item_info_path)
        layout_path = expand_path(level.layout_path)
        config_path = expand_path(level.config_path)

        item_info, layout, environment_config, _ = load_config_files(item_info_path, layout_path, config_path)
        self.current_config: EnvironmentConfig = yaml.load(
            environment_config, Loader=yaml.Loader
        )
        if level.config_overwrite is not None:
            deep_update(self.current_config, level.config_overwrite)
            environment_config = yaml.dump(self.current_config)

        seed = level.seed
        creation_json = CreateEnvironmentConfig(
            manager_id=study_manager.server_manager_id,
            number_players=self.study_config["num_players"]
            + self.study_config["num_bots"],
            environment_settings={"all_player_can_pause_game": False},
            item_info_config=item_info,
            environment_config=environment_config,
            layout_config=layout,
            seed=seed,
            env_name=f"study_{self.study_id}_level_{self.current_level_idx}",
        ).model_dump(mode="json")

        env_info = request_game_server(
            study_manager.game_server_url + "/manage/create_env/",
            json_data=creation_json,
        )

        if env_info.status_code == 403:
            raise ValueError(f"Forbidden Request: {env_info.json()['detail']}")
        elif env_info.status_code == 500:
            raise HTTPException(
                status_code=500,
                detail=f"Game server crashed.",
            )
        env_info = env_info.json()

        player_info = env_info["player_info"]
        for idx, (player_id, player_info) in enumerate(player_info.items()):
            if idx >= self.study_config["num_players"]:
                self.create_and_connect_bot(player_id, player_info)
        return env_info

    def start_level(self):
        """Starts an environment based on the current level index."""
        self.current_running_env = self.create_env(self.levels[self.current_level_idx])

    def next_level(self):
        """Stops the last environment, starts the next one and
        remaps the participants to the new player infos.
        """
        request_game_server(
            f"{study_manager.game_server_url}/manage/stop_env/",
            json_data={
                "manager_id": study_manager.server_manager_id,
                "env_id": self.current_running_env["env_id"],
                "reason": "Next level",
            },
        )

        self.current_level_idx += 1
        if not self.study_done:
            self.start_level()
            for (
                participant_id,
                player_info,
            ) in self.participant_id_to_player_info.items():
                new_player_info = {
                    player_name: self.current_running_env["player_info"][player_name]
                    for player_name in player_info.keys()
                }
                self.participant_id_to_player_info[participant_id] = new_player_info

            for key in self.participants_done:
                self.participants_done[key] = False

    def add_participant(self, participant_id: str, number_players: int):
        """Adds a participant to the study, one participant can control multiple players.

        Args:
            participant_id: The participant id for which to register the participant.
            number_players: The number of players which the participant controls.
        """
        player_names = [
            str(self.num_connected_players + i) for i in range(number_players)
        ]
        player_info = {
            player_name: self.current_running_env["player_info"][player_name]
            for player_name in player_names
        }
        self.participant_id_to_player_info[participant_id] = player_info
        self.num_connected_players += number_players

    def participant_finished_level(self, participant_id: str):
        """Signals the server if a player has finished a level.
        If all participants finished the level, the next level is started."""
        self.participants_done[participant_id] = True
        if all(self.participants_done.values()):
            self.next_level()

    def get_connection(
        self, participant_id: str, participant_host: str
    ) -> Tuple[dict[str, PlayerInfo] | None, LevelInfo | None]:
        """Get the assigned connections to the game server for a participant.

        Args:
            participant_id: The participant id which requests the connections.
            participant_host: ip-address of the participant.

        Returns:
            The player info for the game server connections, level name and information if the level is the last
        one and which recipes are possible in the level.

        Raises:
            HTTPException(409) if the player is not found in the dictionary keys which saves the connections.
        """
        if participant_id in self.participant_id_to_player_info.keys():
            player_info = self.participant_id_to_player_info[participant_id]
            current_level = self.levels[self.current_level_idx]
            level_info = LevelInfo(
                name=current_level["name"],
                last_level=self.last_level,
                recipe_graphs=self.current_running_env["recipe_graphs"],
                number_players=len(self.current_running_env["player_info"]),
                kitchen_size=self.current_running_env["kitchen_size"],
            )
            log_path = expand_path(
                self.study_config["study_log_path"],
                env_name=self.current_running_env["env_id"],
            )
            os.makedirs(log_path, exist_ok=True)
            with open(Path(log_path) / "study_log.jsonl", "a") as log_file:
                log_file.write(
                    json.dumps(
                        {
                            "env_id": self.current_running_env["env_id"],
                            "participant_ip": participant_host,
                            "level_info": level_info.dict(),
                            "player_info": player_info,
                        }
                    )
                    + "\n"
                )
            return player_info, level_info
        else:
            raise HTTPException(
                status_code=409,
                detail=f"Participant not registered in this study.",
            )

    def create_and_connect_bot(self, player_id: str, player_info: PlayerInfo):
        """Creates and connects a bot to the current environment.

        Args:
            player_id: player id of the player the bot controls.
            player_info: Connection info for the bot.
        """
        player_hash = player_info["player_hash"]
        ws_address = self.bot_websocket_url + player_info["client_id"]
        print(
            f'--general_plus="agent_websocket:{ws_address};player_hash:{player_hash};agent_id:{player_id}"'
        )
        if self.use_aaambos_agent:
            sub = Popen(
                " ".join(
                    [
                        "exec",
                        "aaambos",
                        "run",
                        "--arch_config",
                        str(ROOT_DIR / "configs" / "agents" / "arch_config.yml"),
                        "--run_config",
                        str(ROOT_DIR / "configs" / "agents" / "run_config.yml"),
                        f'--general_plus="agent_websocket:{ws_address};player_hash:{player_hash};agent_id:{player_id}"',
                        f"--instance={player_hash}",
                    ]
                ),
                shell=True,
            )
        else:
            sub = Popen(
                " ".join(
                    [
                        "python",
                        f'"{ROOT_DIR / "configs" / "agents" / "random_agent.py"}"',
                        f'--uri {self.bot_websocket_url + player_info["client_id"]}',
                        f"--player_hash {player_hash}",
                        f"--player_id {player_id}",
                    ]
                ),
                shell=True,
            )
        self.sub_processes.append(sub)

    def kill_bots(self):
        """Terminates the subprocesses of the bots."""
        for sub in self.sub_processes:
            try:
                if self.use_aaambos_agent:
                    pgrp = os.getpgid(sub.pid)
                    os.killpg(pgrp, signal.SIGINT)
                    subprocess.run(
                        "kill $(ps aux | grep 'aaambos' | awk '{print $2}')", shell=True
                    )
                else:
                    sub.kill()

            except ProcessLookupError:
                pass

        self.sub_processes = []

    def __repr__(self):
        return f"Study({self.current_running_env['env_id']})"


class StudyManager:
    """Class which manages different studies, their creation and connecting participants to them."""

    def __init__(self):
        """Constructor of the StudyManager class."""
        self.game_host: str = "localhost"
        """Host address of the game server where the studies are running their environments."""
        self.game_port: int = 8000
        """Port of the game server where the studies are running their environments."""
        self.game_server_url: str = ""
        """Combined URL of the game server where the studies are running their environments."""
        self.create_game_server_url(use_ssl=False)

        self.server_manager_id: str = ""
        """Manager id of this manager which will be registered in the game server."""
        self.running_studies: list[Study] = []
        """List of currently running studies."""

        self.participant_id_to_study_map: dict[str, Study] = {}
        """Dict which maps participants to studies."""

        self.running_tutorials: dict[str, CreateEnvResult] = {}
        """Dict which saves currently running tutorial envs, as these do not need advanced player management."""

        self.study_config_path = ROOT_DIR / "configs" / "study" / "study_config.yaml"
        """Path to the configuration file for the studies. Default will be overwritten."""




    def create_study(self):
        """Creates a study with the path of the config files and the connection to the game server."""
        study = Study(
            self.study_config_path,
            self.game_host,
            self.game_port,
        )
        study.start_level()
        self.running_studies.append(study)

    def add_participant(self, participant_id: str, number_players: int):
        """Adds participants to a study. Creates a new study if all other
                studies have not enough free player slots
        Args:
            participant_id: ID of the participant which wants to connect to a study.
            number_players: The number of player the participant wants to connect.

        Raises: HTTPException(409) if the participants requests more players than can fit in a study.
        """

        if not self.running_studies or all(
            [not s.can_add_participants(number_players) for s in self.running_studies]
        ):
            self.create_study()

        for study in self.running_studies:
            if study.can_add_participants(number_players):
                study.add_participant(participant_id, number_players)
                self.participant_id_to_study_map[participant_id] = study
                return
        raise HTTPException(status_code=409, detail="Too many participants to add.")

    def player_finished_level(self, participant_id: str):
        """A participant signals the study manager that they finished a level.

        Args:
            participant_id: ID of the participant.

        Raises: HTTPException(409) if this participant is not registered in any study.

        """
        if participant_id in self.participant_id_to_study_map.keys():
            assigned_study = self.participant_id_to_study_map[participant_id]
            assigned_study.participant_finished_level(participant_id)
        else:
            raise HTTPException(status_code=409, detail="Participant not in any study.")

    def get_participant_game_connection(
        self, participant_id: str, participant_host: str
    ) -> Tuple[dict[str, PlayerInfo], LevelInfo]:
        """Get the assigned connections to the game server for a participant.

        Args:
            participant_id: ID of the participant.

        Returns:
            The player info for the game server connections, level name and information if the level is the last one and
        which recipes are possible in the level.

        Raises:
            HTTPException(409) if the player not registered in any study.
        """
        if participant_id in self.running_tutorials.keys():
            tutorial_env = self.running_tutorials[participant_id]
            level_info = LevelInfo(
                name="Tutorial",
                last_level=False,
                recipe_graphs=tutorial_env["recipe_graphs"],
                number_players=1,
                kitchen_size=tutorial_env["kitchen_size"],
            )
            player_info = tutorial_env["player_info"]
            return player_info, level_info

        if participant_id in self.participant_id_to_study_map.keys():
            assigned_study = self.participant_id_to_study_map[participant_id]
            player_info, level_info = assigned_study.get_connection(
                participant_id, participant_host
            )

            return player_info, level_info
        else:
            raise HTTPException(status_code=409, detail="Participant not in any study.")

    def set_game_server_url(self, game_host: str, game_port: int, use_ssl: bool):
        """Set the game server host address, port and combined url. These values are set this way because
        the fastapi requests act on top level of the python script.

        Args:
            use_ssl: A boolean indicating whether to use SSL for the game server URL.
            game_host: The game server host address.
            game_port: The game server port.
        """
        self.game_host = game_host
        self.game_port = game_port
        self.create_game_server_url(use_ssl)

    def create_game_server_url(self, use_ssl: bool):
        """Update `game_server_url` attribute.
        Args:
            use_ssl: A boolean indicating whether to use SSL for the game server URL.

        """
        self.game_server_url = (
            f"http{'s' if use_ssl else ''}://{self.game_host}:{self.game_port}"
        )

    def set_manager_id(self, manager_id: str):
        """Set the manager id of the study server. This value is set this way because
        the fastapi requests act on top level of the python script.

        Args:
            manager_id: Manager ID for this study manager so that it matches in the game server.
        """
        self.server_manager_id = manager_id

    def set_study_config(self, study_config_path: str):
        """Set the study config path of the study server. This value is set this way because
        the fastapi requests act on top level of the python script.

        Args:
            study_config_path: Path to the study config file for the studies.
        """
        # TODO validate study_config?
        self.study_config_path = study_config_path

    @staticmethod
    def start_tutorial(participant_id: str):
        """Start the tutorial by instructing a game server to create a tutorial env.

        Args:
            participant_id: The ID of the participant who wants to start the tutorial.

        """
        environment_config_path = ROOT_DIR / "configs" / "tutorial_env_config.yaml"
        layout_path = ROOT_DIR / "configs" / "layouts" / "tutorial.layout"
        item_info_path = ROOT_DIR / "configs" / "item_info.yaml"
        item_info, layout, environment_config, _ = load_config_files(item_info_path, layout_path, environment_config_path)

        creation_json = CreateEnvironmentConfig(
            manager_id=study_manager.server_manager_id,
            number_players=1,
            environment_settings={"all_player_can_pause_game": False},
            item_info_config=item_info,
            environment_config=environment_config,
            layout_config=layout,
            seed=1234567890,
            env_name="tutorial",
        ).model_dump(mode="json")
        # todo async
        env_info = request_game_server(
            study_manager.game_server_url + "/manage/create_env/",
            json_data=creation_json,
        )
        match env_info.status_code:
            case 200:
                env_info = env_info.json()
                study_manager.running_tutorials[participant_id] = env_info
            case 403:
                raise HTTPException(
                    status_code=403,
                    detail=f"Forbidden Request: {env_info.json()['detail']}",
                )
            case 500:
                raise HTTPException(
                    status_code=500,
                    detail=f"Game server crashed: {env_info.json()['detail']}",
                )

    @staticmethod
    def end_tutorial(participant_id: str):
        """End a tutorial by doing clean up on the game server.

        This static method is used to end a tutorial for a specified participant.
        It retrieves the environment information from the `running_tutorials` dictionary using the participant ID.
        It then sends a request to the game server to stop the environment associated with the tutorial,
        providing the manager ID, environment ID, and reason for ending the tutorial.
        If the request is successful (status code 200), the tutorial is removed from the `running_tutorials` dictionary.
        If the request fails, an HTTPException with status code 503 is raised.

        Args:
            participant_id (str): The ID of the participant whose tutorial is ending.

        Raises:
            HTTPException: If there is an error disconnecting from the tutorial.

        """
        env = study_manager.running_tutorials[participant_id]
        answer = request_game_server(
            f"{study_manager.game_server_url}/manage/stop_env/",
            json_data={
                "manager_id": study_manager.server_manager_id,
                "env_id": env["env_id"],
                "reason": "Finished tutorial",
            },
        )
        if answer.status_code != 200:
            raise HTTPException(
                status_code=503, detail="Could not disconnect from tutorial"
            )
        del study_manager.running_tutorials[participant_id]


study_manager = StudyManager()
"""Study manager instance for the study server."""


@app.post("/start_study/{participant_id}/{number_players}")
async def start_study(participant_id: str, number_players: int):
    """Request to start a study.

    Args:
        participant_id: ID of the requesting participant.
        number_players: Number of player the participant wants to add to a study.

    """
    log.debug(f"ADDING PLAYERS: {number_players}")
    study_manager.add_participant(participant_id, number_players)


@app.post("/level_done/{participant_id}")
async def level_done(participant_id: str):
    """Request to signal that a participant has finished a level.
    For synchronizing level endings and starting a new level.

    Args:
        participant_id: ID of the requesting participant.
    """
    study_manager.player_finished_level(participant_id)


@app.post("/get_game_connection/{participant_id}")
async def get_game_connection(
    participant_id: str,
    request: Request,
) -> dict[str, dict[str, PlayerInfo] | LevelInfo]:
    """Request to get the connection to the game server of a participant.

    Args:
        participant_id: ID of the requesting participant.
        request: Info about the post request, e.g, ip address.

    Returns:
        A dict containing the game server connection information and information about the current level.

    """
    player_info, level_info = study_manager.get_participant_game_connection(
        participant_id, request.client.host if request.client is not None else "Test"
    )
    return {"player_info": player_info, "level_info": level_info}


@app.post("/connect_to_tutorial/{participant_id}")
async def connect_to_tutorial(participant_id: str):
    """Request of a participant to start a tutorial env and connect to it.

    Args:
        participant_id: ID of the requesting participant.
    Raises:
        HTTPException(403) if the game server returns 403
        HTTPException(500) if the game server returns 500
    """
    study_manager.start_tutorial(participant_id)


@app.post("/disconnect_from_tutorial/{participant_id}")
async def disconnect_from_tutorial(participant_id: str):
    """A participant disconnects from a tutorial environment, which is then stopped on the game server.

    Args:
        participant_id: The participant which disconnects from the tutorial.

    Raises: HTTPException(503) if the game server returns some error.
    """
    study_manager.end_tutorial(participant_id)


def main(
    study_host,
    study_port,
    game_host,
    game_port,
    manager_ids,
    study_config_path,
    use_ssl,
):
    """Start a study server."""
    study_manager.set_game_server_url(
        game_host=game_host, game_port=game_port, use_ssl=use_ssl
    )
    study_manager.set_manager_id(manager_id=manager_ids[0])
    study_manager.set_study_config(study_config_path=study_config_path)

    print(
        f"Use {study_manager.server_manager_id=} for game_server_url=http{'s' if use_ssl else ''}://{game_host}:{game_port}"
    )
    loop = asyncio.new_event_loop()
    config = uvicorn.Config(app, host=study_host, port=study_port, loop=loop)
    server = uvicorn.Server(config)
    loop.run_until_complete(server.serve())


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="Cooperative Cuisine Study Server",
        description="Study Server: Match Making, client pre and post managing.",
        epilog="For further information, "
        "see https://scs.pages.ub.uni-bielefeld.de/cocosy/cooperative-cuisine/overcooked_simulator.html",
    )
    create_study_server_parser(parser)
    args = parser.parse_args()
    main(
        args.study_url,
        args.study_port,
        game_host=args.game_url,
        game_port=args.game_port,
        manager_ids=args.manager_ids,
        study_config_path=args.study_config,
        use_ssl=args.ssl,
    )
