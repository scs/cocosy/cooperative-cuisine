"""
FROM https://www.webucator.com/article/python-color-constants-module/

Provide RGB color constants and a colors dictionary with
elements formatted: colors[colorname] = CONSTANT"""
from collections import namedtuple, OrderedDict

Color = namedtuple("RGB", "red, green, blue")
colors = {}  # dict of colors


class RGB(Color):
    def hex_format(self):
        """Returns color in hex format"""
        return "#{:02X}{:02X}{:02X}".format(self.red, self.green, self.blue)


# Color Constants
ALICEBLUE = RGB(240, 248, 255)
"""<div id="rectangle" style="background-color: #f0f8ff">🎨</div>"""
ANTIQUEWHITE = RGB(250, 235, 215)
"""<div id="rectangle" style="background-color: #faebd7">🎨</div>"""
ANTIQUEWHITE1 = RGB(255, 239, 219)
"""<div id="rectangle" style="background-color: #ffefdb">🎨</div>"""
ANTIQUEWHITE2 = RGB(238, 223, 204)
"""<div id="rectangle" style="background-color: #eedfcc">🎨</div>"""
ANTIQUEWHITE3 = RGB(205, 192, 176)
"""<div id="rectangle" style="background-color: #cdc0b0">🎨</div>"""
ANTIQUEWHITE4 = RGB(139, 131, 120)
"""<div id="rectangle" style="background-color: #8b8378">🎨</div>"""
AQUA = RGB(0, 255, 255)
"""<div id="rectangle" style="background-color: #00ffff">🎨</div>"""
AQUAMARINE1 = RGB(127, 255, 212)
"""<div id="rectangle" style="background-color: #7fffd4">🎨</div>"""
AQUAMARINE2 = RGB(118, 238, 198)
"""<div id="rectangle" style="background-color: #76eec6">🎨</div>"""
AQUAMARINE3 = RGB(102, 205, 170)
"""<div id="rectangle" style="background-color: #66cdaa">🎨</div>"""
AQUAMARINE4 = RGB(69, 139, 116)
"""<div id="rectangle" style="background-color: #458b74">🎨</div>"""
AZURE1 = RGB(240, 255, 255)
"""<div id="rectangle" style="background-color: #f0ffff">🎨</div>"""
AZURE2 = RGB(224, 238, 238)
"""<div id="rectangle" style="background-color: #e0eeee">🎨</div>"""
AZURE3 = RGB(193, 205, 205)
"""<div id="rectangle" style="background-color: #c1cdcd">🎨</div>"""
AZURE4 = RGB(131, 139, 139)
"""<div id="rectangle" style="background-color: #838b8b">🎨</div>"""
BANANA = RGB(227, 207, 87)
"""<div id="rectangle" style="background-color: #e3cf57">🎨</div>"""
BEIGE = RGB(245, 245, 220)
"""<div id="rectangle" style="background-color: #f5f5dc">🎨</div>"""
BISQUE1 = RGB(255, 228, 196)
"""<div id="rectangle" style="background-color: #ffe4c4">🎨</div>"""
BISQUE2 = RGB(238, 213, 183)
"""<div id="rectangle" style="background-color: #eed5b7">🎨</div>"""
BISQUE3 = RGB(205, 183, 158)
"""<div id="rectangle" style="background-color: #cdb79e">🎨</div>"""
BISQUE4 = RGB(139, 125, 107)
"""<div id="rectangle" style="background-color: #8b7d6b">🎨</div>"""
BLACK = RGB(0, 0, 0)
"""<div id="rectangle" style="background-color: #000000">🎨</div>"""
BLANCHEDALMOND = RGB(255, 235, 205)
"""<div id="rectangle" style="background-color: #ffebcd">🎨</div>"""
BLUE = RGB(0, 0, 255)
"""<div id="rectangle" style="background-color: #0000ff">🎨</div>"""
BLUE2 = RGB(0, 0, 238)
"""<div id="rectangle" style="background-color: #0000ee">🎨</div>"""
BLUE3 = RGB(0, 0, 205)
"""<div id="rectangle" style="background-color: #0000cd">🎨</div>"""
BLUE4 = RGB(0, 0, 139)
"""<div id="rectangle" style="background-color: #00008b">🎨</div>"""
BLUEVIOLET = RGB(138, 43, 226)
"""<div id="rectangle" style="background-color: #8a2be2">🎨</div>"""
BRICK = RGB(156, 102, 31)
"""<div id="rectangle" style="background-color: #9c661f">🎨</div>"""
BROWN = RGB(165, 42, 42)
"""<div id="rectangle" style="background-color: #a52a2a">🎨</div>"""
BROWN1 = RGB(255, 64, 64)
"""<div id="rectangle" style="background-color: #ff4040">🎨</div>"""
BROWN2 = RGB(238, 59, 59)
"""<div id="rectangle" style="background-color: #ee3b3b">🎨</div>"""
BROWN3 = RGB(205, 51, 51)
"""<div id="rectangle" style="background-color: #cd3333">🎨</div>"""
BROWN4 = RGB(139, 35, 35)
"""<div id="rectangle" style="background-color: #8b2323">🎨</div>"""
BURLYWOOD = RGB(222, 184, 135)
"""<div id="rectangle" style="background-color: #deb887">🎨</div>"""
BURLYWOOD1 = RGB(255, 211, 155)
"""<div id="rectangle" style="background-color: #ffd39b">🎨</div>"""
BURLYWOOD2 = RGB(238, 197, 145)
"""<div id="rectangle" style="background-color: #eec591">🎨</div>"""
BURLYWOOD3 = RGB(205, 170, 125)
"""<div id="rectangle" style="background-color: #cdaa7d">🎨</div>"""
BURLYWOOD4 = RGB(139, 115, 85)
"""<div id="rectangle" style="background-color: #8b7355">🎨</div>"""
BURNTSIENNA = RGB(138, 54, 15)
"""<div id="rectangle" style="background-color: #8a360f">🎨</div>"""
BURNTUMBER = RGB(138, 51, 36)
"""<div id="rectangle" style="background-color: #8a3324">🎨</div>"""
CADETBLUE = RGB(95, 158, 160)
"""<div id="rectangle" style="background-color: #5f9ea0">🎨</div>"""
CADETBLUE1 = RGB(152, 245, 255)
"""<div id="rectangle" style="background-color: #98f5ff">🎨</div>"""
CADETBLUE2 = RGB(142, 229, 238)
"""<div id="rectangle" style="background-color: #8ee5ee">🎨</div>"""
CADETBLUE3 = RGB(122, 197, 205)
"""<div id="rectangle" style="background-color: #7ac5cd">🎨</div>"""
CADETBLUE4 = RGB(83, 134, 139)
"""<div id="rectangle" style="background-color: #53868b">🎨</div>"""
CADMIUMORANGE = RGB(255, 97, 3)
"""<div id="rectangle" style="background-color: #ff6103">🎨</div>"""
CADMIUMYELLOW = RGB(255, 153, 18)
"""<div id="rectangle" style="background-color: #ff9912">🎨</div>"""
CARROT = RGB(237, 145, 33)
"""<div id="rectangle" style="background-color: #ed9121">🎨</div>"""
CHARTREUSE1 = RGB(127, 255, 0)
"""<div id="rectangle" style="background-color: #7fff00">🎨</div>"""
CHARTREUSE2 = RGB(118, 238, 0)
"""<div id="rectangle" style="background-color: #76ee00">🎨</div>"""
CHARTREUSE3 = RGB(102, 205, 0)
"""<div id="rectangle" style="background-color: #66cd00">🎨</div>"""
CHARTREUSE4 = RGB(69, 139, 0)
"""<div id="rectangle" style="background-color: #458b00">🎨</div>"""
CHOCOLATE = RGB(210, 105, 30)
"""<div id="rectangle" style="background-color: #d2691e">🎨</div>"""
CHOCOLATE1 = RGB(255, 127, 36)
"""<div id="rectangle" style="background-color: #ff7f24">🎨</div>"""
CHOCOLATE2 = RGB(238, 118, 33)
"""<div id="rectangle" style="background-color: #ee7621">🎨</div>"""
CHOCOLATE3 = RGB(205, 102, 29)
"""<div id="rectangle" style="background-color: #cd661d">🎨</div>"""
CHOCOLATE4 = RGB(139, 69, 19)
"""<div id="rectangle" style="background-color: #8b4513">🎨</div>"""
COBALT = RGB(61, 89, 171)
"""<div id="rectangle" style="background-color: #3d59ab">🎨</div>"""
COBALTGREEN = RGB(61, 145, 64)
"""<div id="rectangle" style="background-color: #3d9140">🎨</div>"""
COLDGREY = RGB(128, 138, 135)
"""<div id="rectangle" style="background-color: #808a87">🎨</div>"""
CORAL = RGB(255, 127, 80)
"""<div id="rectangle" style="background-color: #ff7f50">🎨</div>"""
CORAL1 = RGB(255, 114, 86)
"""<div id="rectangle" style="background-color: #ff7256">🎨</div>"""
CORAL2 = RGB(238, 106, 80)
"""<div id="rectangle" style="background-color: #ee6a50">🎨</div>"""
CORAL3 = RGB(205, 91, 69)
"""<div id="rectangle" style="background-color: #cd5b45">🎨</div>"""
CORAL4 = RGB(139, 62, 47)
"""<div id="rectangle" style="background-color: #8b3e2f">🎨</div>"""
CORNFLOWERBLUE = RGB(100, 149, 237)
"""<div id="rectangle" style="background-color: #6495ed">🎨</div>"""
CORNSILK1 = RGB(255, 248, 220)
"""<div id="rectangle" style="background-color: #fff8dc">🎨</div>"""
CORNSILK2 = RGB(238, 232, 205)
"""<div id="rectangle" style="background-color: #eee8cd">🎨</div>"""
CORNSILK3 = RGB(205, 200, 177)
"""<div id="rectangle" style="background-color: #cdc8b1">🎨</div>"""
CORNSILK4 = RGB(139, 136, 120)
"""<div id="rectangle" style="background-color: #8b8878">🎨</div>"""
CRIMSON = RGB(220, 20, 60)
"""<div id="rectangle" style="background-color: #dc143c">🎨</div>"""
CYAN2 = RGB(0, 238, 238)
"""<div id="rectangle" style="background-color: #00eeee">🎨</div>"""
CYAN3 = RGB(0, 205, 205)
"""<div id="rectangle" style="background-color: #00cdcd">🎨</div>"""
CYAN4 = RGB(0, 139, 139)
"""<div id="rectangle" style="background-color: #008b8b">🎨</div>"""
DARKGOLDENROD = RGB(184, 134, 11)
"""<div id="rectangle" style="background-color: #b8860b">🎨</div>"""
DARKGOLDENROD1 = RGB(255, 185, 15)
"""<div id="rectangle" style="background-color: #ffb90f">🎨</div>"""
DARKGOLDENROD2 = RGB(238, 173, 14)
"""<div id="rectangle" style="background-color: #eead0e">🎨</div>"""
DARKGOLDENROD3 = RGB(205, 149, 12)
"""<div id="rectangle" style="background-color: #cd950c">🎨</div>"""
DARKGOLDENROD4 = RGB(139, 101, 8)
"""<div id="rectangle" style="background-color: #8b6508">🎨</div>"""
DARKGRAY = RGB(169, 169, 169)
"""<div id="rectangle" style="background-color: #a9a9a9">🎨</div>"""
DARKGREEN = RGB(0, 100, 0)
"""<div id="rectangle" style="background-color: #006400">🎨</div>"""
DARKKHAKI = RGB(189, 183, 107)
"""<div id="rectangle" style="background-color: #bdb76b">🎨</div>"""
DARKOLIVEGREEN = RGB(85, 107, 47)
"""<div id="rectangle" style="background-color: #556b2f">🎨</div>"""
DARKOLIVEGREEN1 = RGB(202, 255, 112)
"""<div id="rectangle" style="background-color: #caff70">🎨</div>"""
DARKOLIVEGREEN2 = RGB(188, 238, 104)
"""<div id="rectangle" style="background-color: #bcee68">🎨</div>"""
DARKOLIVEGREEN3 = RGB(162, 205, 90)
"""<div id="rectangle" style="background-color: #a2cd5a">🎨</div>"""
DARKOLIVEGREEN4 = RGB(110, 139, 61)
"""<div id="rectangle" style="background-color: #6e8b3d">🎨</div>"""
DARKORANGE = RGB(255, 140, 0)
"""<div id="rectangle" style="background-color: #ff8c00">🎨</div>"""
DARKORANGE1 = RGB(255, 127, 0)
"""<div id="rectangle" style="background-color: #ff7f00">🎨</div>"""
DARKORANGE2 = RGB(238, 118, 0)
"""<div id="rectangle" style="background-color: #ee7600">🎨</div>"""
DARKORANGE3 = RGB(205, 102, 0)
"""<div id="rectangle" style="background-color: #cd6600">🎨</div>"""
DARKORANGE4 = RGB(139, 69, 0)
"""<div id="rectangle" style="background-color: #8b4500">🎨</div>"""
DARKORCHID = RGB(153, 50, 204)
"""<div id="rectangle" style="background-color: #9932cc">🎨</div>"""
DARKORCHID1 = RGB(191, 62, 255)
"""<div id="rectangle" style="background-color: #bf3eff">🎨</div>"""
DARKORCHID2 = RGB(178, 58, 238)
"""<div id="rectangle" style="background-color: #b23aee">🎨</div>"""
DARKORCHID3 = RGB(154, 50, 205)
"""<div id="rectangle" style="background-color: #9a32cd">🎨</div>"""
DARKORCHID4 = RGB(104, 34, 139)
"""<div id="rectangle" style="background-color: #68228b">🎨</div>"""
DARKSALMON = RGB(233, 150, 122)
"""<div id="rectangle" style="background-color: #e9967a">🎨</div>"""
DARKSEAGREEN = RGB(143, 188, 143)
"""<div id="rectangle" style="background-color: #8fbc8f">🎨</div>"""
DARKSEAGREEN1 = RGB(193, 255, 193)
"""<div id="rectangle" style="background-color: #c1ffc1">🎨</div>"""
DARKSEAGREEN2 = RGB(180, 238, 180)
"""<div id="rectangle" style="background-color: #b4eeb4">🎨</div>"""
DARKSEAGREEN3 = RGB(155, 205, 155)
"""<div id="rectangle" style="background-color: #9bcd9b">🎨</div>"""
DARKSEAGREEN4 = RGB(105, 139, 105)
"""<div id="rectangle" style="background-color: #698b69">🎨</div>"""
DARKSLATEBLUE = RGB(72, 61, 139)
"""<div id="rectangle" style="background-color: #483d8b">🎨</div>"""
DARKSLATEGRAY = RGB(47, 79, 79)
"""<div id="rectangle" style="background-color: #2f4f4f">🎨</div>"""
DARKSLATEGRAY1 = RGB(151, 255, 255)
"""<div id="rectangle" style="background-color: #97ffff">🎨</div>"""
DARKSLATEGRAY2 = RGB(141, 238, 238)
"""<div id="rectangle" style="background-color: #8deeee">🎨</div>"""
DARKSLATEGRAY3 = RGB(121, 205, 205)
"""<div id="rectangle" style="background-color: #79cdcd">🎨</div>"""
DARKSLATEGRAY4 = RGB(82, 139, 139)
"""<div id="rectangle" style="background-color: #528b8b">🎨</div>"""
DARKTURQUOISE = RGB(0, 206, 209)
"""<div id="rectangle" style="background-color: #00ced1">🎨</div>"""
DARKVIOLET = RGB(148, 0, 211)
"""<div id="rectangle" style="background-color: #9400d3">🎨</div>"""
DEEPPINK1 = RGB(255, 20, 147)
"""<div id="rectangle" style="background-color: #ff1493">🎨</div>"""
DEEPPINK2 = RGB(238, 18, 137)
"""<div id="rectangle" style="background-color: #ee1289">🎨</div>"""
DEEPPINK3 = RGB(205, 16, 118)
"""<div id="rectangle" style="background-color: #cd1076">🎨</div>"""
DEEPPINK4 = RGB(139, 10, 80)
"""<div id="rectangle" style="background-color: #8b0a50">🎨</div>"""
DEEPSKYBLUE1 = RGB(0, 191, 255)
"""<div id="rectangle" style="background-color: #00bfff">🎨</div>"""
DEEPSKYBLUE2 = RGB(0, 178, 238)
"""<div id="rectangle" style="background-color: #00b2ee">🎨</div>"""
DEEPSKYBLUE3 = RGB(0, 154, 205)
"""<div id="rectangle" style="background-color: #009acd">🎨</div>"""
DEEPSKYBLUE4 = RGB(0, 104, 139)
"""<div id="rectangle" style="background-color: #00688b">🎨</div>"""
DIMGRAY = RGB(105, 105, 105)
"""<div id="rectangle" style="background-color: #696969">🎨</div>"""
DODGERBLUE1 = RGB(30, 144, 255)
"""<div id="rectangle" style="background-color: #1e90ff">🎨</div>"""
DODGERBLUE2 = RGB(28, 134, 238)
"""<div id="rectangle" style="background-color: #1c86ee">🎨</div>"""
DODGERBLUE3 = RGB(24, 116, 205)
"""<div id="rectangle" style="background-color: #1874cd">🎨</div>"""
DODGERBLUE4 = RGB(16, 78, 139)
"""<div id="rectangle" style="background-color: #104e8b">🎨</div>"""
EGGSHELL = RGB(252, 230, 201)
"""<div id="rectangle" style="background-color: #fce6c9">🎨</div>"""
EMERALDGREEN = RGB(0, 201, 87)
"""<div id="rectangle" style="background-color: #00c957">🎨</div>"""
FIREBRICK = RGB(178, 34, 34)
"""<div id="rectangle" style="background-color: #b22222">🎨</div>"""
FIREBRICK1 = RGB(255, 48, 48)
"""<div id="rectangle" style="background-color: #ff3030">🎨</div>"""
FIREBRICK2 = RGB(238, 44, 44)
"""<div id="rectangle" style="background-color: #ee2c2c">🎨</div>"""
FIREBRICK3 = RGB(205, 38, 38)
"""<div id="rectangle" style="background-color: #cd2626">🎨</div>"""
FIREBRICK4 = RGB(139, 26, 26)
"""<div id="rectangle" style="background-color: #8b1a1a">🎨</div>"""
FLESH = RGB(255, 125, 64)
"""<div id="rectangle" style="background-color: #ff7d40">🎨</div>"""
FLORALWHITE = RGB(255, 250, 240)
"""<div id="rectangle" style="background-color: #fffaf0">🎨</div>"""
FORESTGREEN = RGB(34, 139, 34)
"""<div id="rectangle" style="background-color: #228b22">🎨</div>"""
GAINSBORO = RGB(220, 220, 220)
"""<div id="rectangle" style="background-color: #dcdcdc">🎨</div>"""
GHOSTWHITE = RGB(248, 248, 255)
"""<div id="rectangle" style="background-color: #f8f8ff">🎨</div>"""
GOLD1 = RGB(255, 215, 0)
"""<div id="rectangle" style="background-color: #ffd700">🎨</div>"""
GOLD2 = RGB(238, 201, 0)
"""<div id="rectangle" style="background-color: #eec900">🎨</div>"""
GOLD3 = RGB(205, 173, 0)
"""<div id="rectangle" style="background-color: #cdad00">🎨</div>"""
GOLD4 = RGB(139, 117, 0)
"""<div id="rectangle" style="background-color: #8b7500">🎨</div>"""
GOLDENROD = RGB(218, 165, 32)
"""<div id="rectangle" style="background-color: #daa520">🎨</div>"""
GOLDENROD1 = RGB(255, 193, 37)
"""<div id="rectangle" style="background-color: #ffc125">🎨</div>"""
GOLDENROD2 = RGB(238, 180, 34)
"""<div id="rectangle" style="background-color: #eeb422">🎨</div>"""
GOLDENROD3 = RGB(205, 155, 29)
"""<div id="rectangle" style="background-color: #cd9b1d">🎨</div>"""
GOLDENROD4 = RGB(139, 105, 20)
"""<div id="rectangle" style="background-color: #8b6914">🎨</div>"""
GRAY = RGB(128, 128, 128)
"""<div id="rectangle" style="background-color: #808080">🎨</div>"""
GRAY1 = RGB(3, 3, 3)
"""<div id="rectangle" style="background-color: #030303">🎨</div>"""
GRAY2 = RGB(5, 5, 5)
"""<div id="rectangle" style="background-color: #050505">🎨</div>"""
GRAY3 = RGB(8, 8, 8)
"""<div id="rectangle" style="background-color: #080808">🎨</div>"""
GRAY4 = RGB(10, 10, 10)
"""<div id="rectangle" style="background-color: #0a0a0a">🎨</div>"""
GRAY5 = RGB(13, 13, 13)
"""<div id="rectangle" style="background-color: #0d0d0d">🎨</div>"""
GRAY6 = RGB(15, 15, 15)
"""<div id="rectangle" style="background-color: #0f0f0f">🎨</div>"""
GRAY7 = RGB(18, 18, 18)
"""<div id="rectangle" style="background-color: #121212">🎨</div>"""
GRAY8 = RGB(20, 20, 20)
"""<div id="rectangle" style="background-color: #141414">🎨</div>"""
GRAY9 = RGB(23, 23, 23)
"""<div id="rectangle" style="background-color: #171717">🎨</div>"""
GRAY10 = RGB(26, 26, 26)
"""<div id="rectangle" style="background-color: #1a1a1a">🎨</div>"""
GRAY11 = RGB(28, 28, 28)
"""<div id="rectangle" style="background-color: #1c1c1c">🎨</div>"""
GRAY12 = RGB(31, 31, 31)
"""<div id="rectangle" style="background-color: #1f1f1f">🎨</div>"""
GRAY13 = RGB(33, 33, 33)
"""<div id="rectangle" style="background-color: #212121">🎨</div>"""
GRAY14 = RGB(36, 36, 36)
"""<div id="rectangle" style="background-color: #242424">🎨</div>"""
GRAY15 = RGB(38, 38, 38)
"""<div id="rectangle" style="background-color: #262626">🎨</div>"""
GRAY16 = RGB(41, 41, 41)
"""<div id="rectangle" style="background-color: #292929">🎨</div>"""
GRAY17 = RGB(43, 43, 43)
"""<div id="rectangle" style="background-color: #2b2b2b">🎨</div>"""
GRAY18 = RGB(46, 46, 46)
"""<div id="rectangle" style="background-color: #2e2e2e">🎨</div>"""
GRAY19 = RGB(48, 48, 48)
"""<div id="rectangle" style="background-color: #303030">🎨</div>"""
GRAY20 = RGB(51, 51, 51)
"""<div id="rectangle" style="background-color: #333333">🎨</div>"""
GRAY21 = RGB(54, 54, 54)
"""<div id="rectangle" style="background-color: #363636">🎨</div>"""
GRAY22 = RGB(56, 56, 56)
"""<div id="rectangle" style="background-color: #383838">🎨</div>"""
GRAY23 = RGB(59, 59, 59)
"""<div id="rectangle" style="background-color: #3b3b3b">🎨</div>"""
GRAY24 = RGB(61, 61, 61)
"""<div id="rectangle" style="background-color: #3d3d3d">🎨</div>"""
GRAY25 = RGB(64, 64, 64)
"""<div id="rectangle" style="background-color: #404040">🎨</div>"""
GRAY26 = RGB(66, 66, 66)
"""<div id="rectangle" style="background-color: #424242">🎨</div>"""
GRAY27 = RGB(69, 69, 69)
"""<div id="rectangle" style="background-color: #454545">🎨</div>"""
GRAY28 = RGB(71, 71, 71)
"""<div id="rectangle" style="background-color: #474747">🎨</div>"""
GRAY29 = RGB(74, 74, 74)
"""<div id="rectangle" style="background-color: #4a4a4a">🎨</div>"""
GRAY30 = RGB(77, 77, 77)
"""<div id="rectangle" style="background-color: #4d4d4d">🎨</div>"""
GRAY31 = RGB(79, 79, 79)
"""<div id="rectangle" style="background-color: #4f4f4f">🎨</div>"""
GRAY32 = RGB(82, 82, 82)
"""<div id="rectangle" style="background-color: #525252">🎨</div>"""
GRAY33 = RGB(84, 84, 84)
"""<div id="rectangle" style="background-color: #545454">🎨</div>"""
GRAY34 = RGB(87, 87, 87)
"""<div id="rectangle" style="background-color: #575757">🎨</div>"""
GRAY35 = RGB(89, 89, 89)
"""<div id="rectangle" style="background-color: #595959">🎨</div>"""
GRAY36 = RGB(92, 92, 92)
"""<div id="rectangle" style="background-color: #5c5c5c">🎨</div>"""
GRAY37 = RGB(94, 94, 94)
"""<div id="rectangle" style="background-color: #5e5e5e">🎨</div>"""
GRAY38 = RGB(97, 97, 97)
"""<div id="rectangle" style="background-color: #616161">🎨</div>"""
GRAY39 = RGB(99, 99, 99)
"""<div id="rectangle" style="background-color: #636363">🎨</div>"""
GRAY4 = RGB(10, 10, 10)
"""<div id="rectangle" style="background-color: #0a0a0a">🎨</div>"""
GRAY40 = RGB(102, 102, 102)
"""<div id="rectangle" style="background-color: #666666">🎨</div>"""
GRAY42 = RGB(107, 107, 107)
"""<div id="rectangle" style="background-color: #6b6b6b">🎨</div>"""
GRAY43 = RGB(110, 110, 110)
"""<div id="rectangle" style="background-color: #6e6e6e">🎨</div>"""
GRAY44 = RGB(112, 112, 112)
"""<div id="rectangle" style="background-color: #707070">🎨</div>"""
GRAY45 = RGB(115, 115, 115)
"""<div id="rectangle" style="background-color: #737373">🎨</div>"""
GRAY46 = RGB(117, 117, 117)
"""<div id="rectangle" style="background-color: #757575">🎨</div>"""
GRAY47 = RGB(120, 120, 120)
"""<div id="rectangle" style="background-color: #787878">🎨</div>"""
GRAY48 = RGB(122, 122, 122)
"""<div id="rectangle" style="background-color: #7a7a7a">🎨</div>"""
GRAY49 = RGB(125, 125, 125)
"""<div id="rectangle" style="background-color: #7d7d7d">🎨</div>"""
GRAY50 = RGB(127, 127, 127)
"""<div id="rectangle" style="background-color: #7f7f7f">🎨</div>"""
GRAY51 = RGB(130, 130, 130)
"""<div id="rectangle" style="background-color: #828282">🎨</div>"""
GRAY52 = RGB(133, 133, 133)
"""<div id="rectangle" style="background-color: #858585">🎨</div>"""
GRAY53 = RGB(135, 135, 135)
"""<div id="rectangle" style="background-color: #878787">🎨</div>"""
GRAY54 = RGB(138, 138, 138)
"""<div id="rectangle" style="background-color: #8a8a8a">🎨</div>"""
GRAY55 = RGB(140, 140, 140)
"""<div id="rectangle" style="background-color: #8c8c8c">🎨</div>"""
GRAY56 = RGB(143, 143, 143)
"""<div id="rectangle" style="background-color: #8f8f8f">🎨</div>"""
GRAY57 = RGB(145, 145, 145)
"""<div id="rectangle" style="background-color: #919191">🎨</div>"""
GRAY58 = RGB(148, 148, 148)
"""<div id="rectangle" style="background-color: #949494">🎨</div>"""
GRAY59 = RGB(150, 150, 150)
"""<div id="rectangle" style="background-color: #969696">🎨</div>"""
GRAY60 = RGB(153, 153, 153)
"""<div id="rectangle" style="background-color: #999999">🎨</div>"""
GRAY61 = RGB(156, 156, 156)
"""<div id="rectangle" style="background-color: #9c9c9c">🎨</div>"""
GRAY62 = RGB(158, 158, 158)
"""<div id="rectangle" style="background-color: #9e9e9e">🎨</div>"""
GRAY63 = RGB(161, 161, 161)
"""<div id="rectangle" style="background-color: #a1a1a1">🎨</div>"""
GRAY64 = RGB(163, 163, 163)
"""<div id="rectangle" style="background-color: #a3a3a3">🎨</div>"""
GRAY65 = RGB(166, 166, 166)
"""<div id="rectangle" style="background-color: #a6a6a6">🎨</div>"""
GRAY66 = RGB(168, 168, 168)
"""<div id="rectangle" style="background-color: #a8a8a8">🎨</div>"""
GRAY67 = RGB(171, 171, 171)
"""<div id="rectangle" style="background-color: #ababab">🎨</div>"""
GRAY68 = RGB(173, 173, 173)
"""<div id="rectangle" style="background-color: #adadad">🎨</div>"""
GRAY69 = RGB(176, 176, 176)
"""<div id="rectangle" style="background-color: #b0b0b0">🎨</div>"""
GRAY70 = RGB(179, 179, 179)
"""<div id="rectangle" style="background-color: #b3b3b3">🎨</div>"""
GRAY71 = RGB(181, 181, 181)
"""<div id="rectangle" style="background-color: #b5b5b5">🎨</div>"""
GRAY72 = RGB(184, 184, 184)
"""<div id="rectangle" style="background-color: #b8b8b8">🎨</div>"""
GRAY73 = RGB(186, 186, 186)
"""<div id="rectangle" style="background-color: #bababa">🎨</div>"""
GRAY74 = RGB(189, 189, 189)
"""<div id="rectangle" style="background-color: #bdbdbd">🎨</div>"""
GRAY75 = RGB(191, 191, 191)
"""<div id="rectangle" style="background-color: #bfbfbf">🎨</div>"""
GRAY76 = RGB(194, 194, 194)
"""<div id="rectangle" style="background-color: #c2c2c2">🎨</div>"""
GRAY77 = RGB(196, 196, 196)
"""<div id="rectangle" style="background-color: #c4c4c4">🎨</div>"""
GRAY78 = RGB(199, 199, 199)
"""<div id="rectangle" style="background-color: #c7c7c7">🎨</div>"""
GRAY79 = RGB(201, 201, 201)
"""<div id="rectangle" style="background-color: #c9c9c9">🎨</div>"""
GRAY80 = RGB(204, 204, 204)
"""<div id="rectangle" style="background-color: #cccccc">🎨</div>"""
GRAY81 = RGB(207, 207, 207)
"""<div id="rectangle" style="background-color: #cfcfcf">🎨</div>"""
GRAY82 = RGB(209, 209, 209)
"""<div id="rectangle" style="background-color: #d1d1d1">🎨</div>"""
GRAY83 = RGB(212, 212, 212)
"""<div id="rectangle" style="background-color: #d4d4d4">🎨</div>"""
GRAY84 = RGB(214, 214, 214)
"""<div id="rectangle" style="background-color: #d6d6d6">🎨</div>"""
GRAY85 = RGB(217, 217, 217)
"""<div id="rectangle" style="background-color: #d9d9d9">🎨</div>"""
GRAY86 = RGB(219, 219, 219)
"""<div id="rectangle" style="background-color: #dbdbdb">🎨</div>"""
GRAY87 = RGB(222, 222, 222)
"""<div id="rectangle" style="background-color: #dedede">🎨</div>"""
GRAY88 = RGB(224, 224, 224)
"""<div id="rectangle" style="background-color: #e0e0e0">🎨</div>"""
GRAY89 = RGB(227, 227, 227)
"""<div id="rectangle" style="background-color: #e3e3e3">🎨</div>"""
GRAY90 = RGB(229, 229, 229)
"""<div id="rectangle" style="background-color: #e5e5e5">🎨</div>"""
GRAY91 = RGB(232, 232, 232)
"""<div id="rectangle" style="background-color: #e8e8e8">🎨</div>"""
GRAY92 = RGB(235, 235, 235)
"""<div id="rectangle" style="background-color: #ebebeb">🎨</div>"""
GRAY93 = RGB(237, 237, 237)
"""<div id="rectangle" style="background-color: #ededed">🎨</div>"""
GRAY94 = RGB(240, 240, 240)
"""<div id="rectangle" style="background-color: #f0f0f0">🎨</div>"""
GRAY95 = RGB(242, 242, 242)
"""<div id="rectangle" style="background-color: #f2f2f2">🎨</div>"""
GRAY97 = RGB(247, 247, 247)
"""<div id="rectangle" style="background-color: #f7f7f7">🎨</div>"""
GRAY98 = RGB(250, 250, 250)
"""<div id="rectangle" style="background-color: #fafafa">🎨</div>"""
GRAY99 = RGB(252, 252, 252)
"""<div id="rectangle" style="background-color: #fcfcfc">🎨</div>"""
GREEN = RGB(0, 128, 0)
"""<div id="rectangle" style="background-color: #008000">🎨</div>"""
GREEN1 = RGB(0, 255, 0)
"""<div id="rectangle" style="background-color: #00ff00">🎨</div>"""
GREEN2 = RGB(0, 238, 0)
"""<div id="rectangle" style="background-color: #00ee00">🎨</div>"""
GREEN3 = RGB(0, 205, 0)
"""<div id="rectangle" style="background-color: #00cd00">🎨</div>"""
GREEN4 = RGB(0, 139, 0)
"""<div id="rectangle" style="background-color: #008b00">🎨</div>"""
GREENYELLOW = RGB(173, 255, 47)
"""<div id="rectangle" style="background-color: #adff2f">🎨</div>"""
HONEYDEW1 = RGB(240, 255, 240)
"""<div id="rectangle" style="background-color: #f0fff0">🎨</div>"""
HONEYDEW2 = RGB(224, 238, 224)
"""<div id="rectangle" style="background-color: #e0eee0">🎨</div>"""
HONEYDEW3 = RGB(193, 205, 193)
"""<div id="rectangle" style="background-color: #c1cdc1">🎨</div>"""
HONEYDEW4 = RGB(131, 139, 131)
"""<div id="rectangle" style="background-color: #838b83">🎨</div>"""
HOTPINK = RGB(255, 105, 180)
"""<div id="rectangle" style="background-color: #ff69b4">🎨</div>"""
HOTPINK1 = RGB(255, 110, 180)
"""<div id="rectangle" style="background-color: #ff6eb4">🎨</div>"""
HOTPINK2 = RGB(238, 106, 167)
"""<div id="rectangle" style="background-color: #ee6aa7">🎨</div>"""
HOTPINK3 = RGB(205, 96, 144)
"""<div id="rectangle" style="background-color: #cd6090">🎨</div>"""
HOTPINK4 = RGB(139, 58, 98)
"""<div id="rectangle" style="background-color: #8b3a62">🎨</div>"""
INDIANRED = RGB(205, 92, 92)
"""<div id="rectangle" style="background-color: #cd5c5c">🎨</div>"""
INDIANRED1 = RGB(255, 106, 106)
"""<div id="rectangle" style="background-color: #ff6a6a">🎨</div>"""
INDIANRED2 = RGB(238, 99, 99)
"""<div id="rectangle" style="background-color: #ee6363">🎨</div>"""
INDIANRED3 = RGB(205, 85, 85)
"""<div id="rectangle" style="background-color: #cd5555">🎨</div>"""
INDIANRED4 = RGB(139, 58, 58)
"""<div id="rectangle" style="background-color: #8b3a3a">🎨</div>"""
INDIGO = RGB(75, 0, 130)
"""<div id="rectangle" style="background-color: #4b0082">🎨</div>"""
IVORY1 = RGB(255, 255, 240)
"""<div id="rectangle" style="background-color: #fffff0">🎨</div>"""
IVORY2 = RGB(238, 238, 224)
"""<div id="rectangle" style="background-color: #eeeee0">🎨</div>"""
IVORY3 = RGB(205, 205, 193)
"""<div id="rectangle" style="background-color: #cdcdc1">🎨</div>"""
IVORY4 = RGB(139, 139, 131)
"""<div id="rectangle" style="background-color: #8b8b83">🎨</div>"""
IVORYBLACK = RGB(41, 36, 33)
"""<div id="rectangle" style="background-color: #292421">🎨</div>"""
KHAKI = RGB(240, 230, 140)
"""<div id="rectangle" style="background-color: #f0e68c">🎨</div>"""
KHAKI1 = RGB(255, 246, 143)
"""<div id="rectangle" style="background-color: #fff68f">🎨</div>"""
KHAKI2 = RGB(238, 230, 133)
"""<div id="rectangle" style="background-color: #eee685">🎨</div>"""
KHAKI3 = RGB(205, 198, 115)
"""<div id="rectangle" style="background-color: #cdc673">🎨</div>"""
KHAKI4 = RGB(139, 134, 78)
"""<div id="rectangle" style="background-color: #8b864e">🎨</div>"""
LAVENDER = RGB(230, 230, 250)
"""<div id="rectangle" style="background-color: #e6e6fa">🎨</div>"""
LAVENDERBLUSH1 = RGB(255, 240, 245)
"""<div id="rectangle" style="background-color: #fff0f5">🎨</div>"""
LAVENDERBLUSH2 = RGB(238, 224, 229)
"""<div id="rectangle" style="background-color: #eee0e5">🎨</div>"""
LAVENDERBLUSH3 = RGB(205, 193, 197)
"""<div id="rectangle" style="background-color: #cdc1c5">🎨</div>"""
LAVENDERBLUSH4 = RGB(139, 131, 134)
"""<div id="rectangle" style="background-color: #8b8386">🎨</div>"""
LAWNGREEN = RGB(124, 252, 0)
"""<div id="rectangle" style="background-color: #7cfc00">🎨</div>"""
LEMONCHIFFON1 = RGB(255, 250, 205)
"""<div id="rectangle" style="background-color: #fffacd">🎨</div>"""
LEMONCHIFFON2 = RGB(238, 233, 191)
"""<div id="rectangle" style="background-color: #eee9bf">🎨</div>"""
LEMONCHIFFON3 = RGB(205, 201, 165)
"""<div id="rectangle" style="background-color: #cdc9a5">🎨</div>"""
LEMONCHIFFON4 = RGB(139, 137, 112)
"""<div id="rectangle" style="background-color: #8b8970">🎨</div>"""
LIGHTBLUE = RGB(173, 216, 230)
"""<div id="rectangle" style="background-color: #add8e6">🎨</div>"""
LIGHTBLUE1 = RGB(191, 239, 255)
"""<div id="rectangle" style="background-color: #bfefff">🎨</div>"""
LIGHTBLUE2 = RGB(178, 223, 238)
"""<div id="rectangle" style="background-color: #b2dfee">🎨</div>"""
LIGHTBLUE3 = RGB(154, 192, 205)
"""<div id="rectangle" style="background-color: #9ac0cd">🎨</div>"""
LIGHTBLUE4 = RGB(104, 131, 139)
"""<div id="rectangle" style="background-color: #68838b">🎨</div>"""
LIGHTCORAL = RGB(240, 128, 128)
"""<div id="rectangle" style="background-color: #f08080">🎨</div>"""
LIGHTCYAN1 = RGB(224, 255, 255)
"""<div id="rectangle" style="background-color: #e0ffff">🎨</div>"""
LIGHTCYAN2 = RGB(209, 238, 238)
"""<div id="rectangle" style="background-color: #d1eeee">🎨</div>"""
LIGHTCYAN3 = RGB(180, 205, 205)
"""<div id="rectangle" style="background-color: #b4cdcd">🎨</div>"""
LIGHTCYAN4 = RGB(122, 139, 139)
"""<div id="rectangle" style="background-color: #7a8b8b">🎨</div>"""
LIGHTGOLDENROD1 = RGB(255, 236, 139)
"""<div id="rectangle" style="background-color: #ffec8b">🎨</div>"""
LIGHTGOLDENROD2 = RGB(238, 220, 130)
"""<div id="rectangle" style="background-color: #eedc82">🎨</div>"""
LIGHTGOLDENROD3 = RGB(205, 190, 112)
"""<div id="rectangle" style="background-color: #cdbe70">🎨</div>"""
LIGHTGOLDENROD4 = RGB(139, 129, 76)
"""<div id="rectangle" style="background-color: #8b814c">🎨</div>"""
LIGHTGOLDENRODYELLOW = RGB(250, 250, 210)
"""<div id="rectangle" style="background-color: #fafad2">🎨</div>"""
LIGHTGREY = RGB(211, 211, 211)
"""<div id="rectangle" style="background-color: #d3d3d3">🎨</div>"""
LIGHTPINK = RGB(255, 182, 193)
"""<div id="rectangle" style="background-color: #ffb6c1">🎨</div>"""
LIGHTPINK1 = RGB(255, 174, 185)
"""<div id="rectangle" style="background-color: #ffaeb9">🎨</div>"""
LIGHTPINK2 = RGB(238, 162, 173)
"""<div id="rectangle" style="background-color: #eea2ad">🎨</div>"""
LIGHTPINK3 = RGB(205, 140, 149)
"""<div id="rectangle" style="background-color: #cd8c95">🎨</div>"""
LIGHTPINK4 = RGB(139, 95, 101)
"""<div id="rectangle" style="background-color: #8b5f65">🎨</div>"""
LIGHTSALMON1 = RGB(255, 160, 122)
"""<div id="rectangle" style="background-color: #ffa07a">🎨</div>"""
LIGHTSALMON2 = RGB(238, 149, 114)
"""<div id="rectangle" style="background-color: #ee9572">🎨</div>"""
LIGHTSALMON3 = RGB(205, 129, 98)
"""<div id="rectangle" style="background-color: #cd8162">🎨</div>"""
LIGHTSALMON4 = RGB(139, 87, 66)
"""<div id="rectangle" style="background-color: #8b5742">🎨</div>"""
LIGHTSEAGREEN = RGB(32, 178, 170)
"""<div id="rectangle" style="background-color: #20b2aa">🎨</div>"""
LIGHTSKYBLUE = RGB(135, 206, 250)
"""<div id="rectangle" style="background-color: #87cefa">🎨</div>"""
LIGHTSKYBLUE1 = RGB(176, 226, 255)
"""<div id="rectangle" style="background-color: #b0e2ff">🎨</div>"""
LIGHTSKYBLUE2 = RGB(164, 211, 238)
"""<div id="rectangle" style="background-color: #a4d3ee">🎨</div>"""
LIGHTSKYBLUE3 = RGB(141, 182, 205)
"""<div id="rectangle" style="background-color: #8db6cd">🎨</div>"""
LIGHTSKYBLUE4 = RGB(96, 123, 139)
"""<div id="rectangle" style="background-color: #607b8b">🎨</div>"""
LIGHTSLATEBLUE = RGB(132, 112, 255)
"""<div id="rectangle" style="background-color: #8470ff">🎨</div>"""
LIGHTSLATEGRAY = RGB(119, 136, 153)
"""<div id="rectangle" style="background-color: #778899">🎨</div>"""
LIGHTSTEELBLUE = RGB(176, 196, 222)
"""<div id="rectangle" style="background-color: #b0c4de">🎨</div>"""
LIGHTSTEELBLUE1 = RGB(202, 225, 255)
"""<div id="rectangle" style="background-color: #cae1ff">🎨</div>"""
LIGHTSTEELBLUE2 = RGB(188, 210, 238)
"""<div id="rectangle" style="background-color: #bcd2ee">🎨</div>"""
LIGHTSTEELBLUE3 = RGB(162, 181, 205)
"""<div id="rectangle" style="background-color: #a2b5cd">🎨</div>"""
LIGHTSTEELBLUE4 = RGB(110, 123, 139)
"""<div id="rectangle" style="background-color: #6e7b8b">🎨</div>"""
LIGHTYELLOW1 = RGB(255, 255, 224)
"""<div id="rectangle" style="background-color: #ffffe0">🎨</div>"""
LIGHTYELLOW2 = RGB(238, 238, 209)
"""<div id="rectangle" style="background-color: #eeeed1">🎨</div>"""
LIGHTYELLOW3 = RGB(205, 205, 180)
"""<div id="rectangle" style="background-color: #cdcdb4">🎨</div>"""
LIGHTYELLOW4 = RGB(139, 139, 122)
"""<div id="rectangle" style="background-color: #8b8b7a">🎨</div>"""
LIMEGREEN = RGB(50, 205, 50)
"""<div id="rectangle" style="background-color: #32cd32">🎨</div>"""
LINEN = RGB(250, 240, 230)
"""<div id="rectangle" style="background-color: #faf0e6">🎨</div>"""
MAGENTA = RGB(255, 0, 255)
"""<div id="rectangle" style="background-color: #ff00ff">🎨</div>"""
MAGENTA2 = RGB(238, 0, 238)
"""<div id="rectangle" style="background-color: #ee00ee">🎨</div>"""
MAGENTA3 = RGB(205, 0, 205)
"""<div id="rectangle" style="background-color: #cd00cd">🎨</div>"""
MAGENTA4 = RGB(139, 0, 139)
"""<div id="rectangle" style="background-color: #8b008b">🎨</div>"""
MANGANESEBLUE = RGB(3, 168, 158)
"""<div id="rectangle" style="background-color: #03a89e">🎨</div>"""
MAROON = RGB(128, 0, 0)
"""<div id="rectangle" style="background-color: #800000">🎨</div>"""
MAROON1 = RGB(255, 52, 179)
"""<div id="rectangle" style="background-color: #ff34b3">🎨</div>"""
MAROON2 = RGB(238, 48, 167)
"""<div id="rectangle" style="background-color: #ee30a7">🎨</div>"""
MAROON3 = RGB(205, 41, 144)
"""<div id="rectangle" style="background-color: #cd2990">🎨</div>"""
MAROON4 = RGB(139, 28, 98)
"""<div id="rectangle" style="background-color: #8b1c62">🎨</div>"""
MEDIUMORCHID = RGB(186, 85, 211)
"""<div id="rectangle" style="background-color: #ba55d3">🎨</div>"""
MEDIUMORCHID1 = RGB(224, 102, 255)
"""<div id="rectangle" style="background-color: #e066ff">🎨</div>"""
MEDIUMORCHID2 = RGB(209, 95, 238)
"""<div id="rectangle" style="background-color: #d15fee">🎨</div>"""
MEDIUMORCHID3 = RGB(180, 82, 205)
"""<div id="rectangle" style="background-color: #b452cd">🎨</div>"""
MEDIUMORCHID4 = RGB(122, 55, 139)
"""<div id="rectangle" style="background-color: #7a378b">🎨</div>"""
MEDIUMPURPLE = RGB(147, 112, 219)
"""<div id="rectangle" style="background-color: #9370db">🎨</div>"""
MEDIUMPURPLE1 = RGB(171, 130, 255)
"""<div id="rectangle" style="background-color: #ab82ff">🎨</div>"""
MEDIUMPURPLE2 = RGB(159, 121, 238)
"""<div id="rectangle" style="background-color: #9f79ee">🎨</div>"""
MEDIUMPURPLE3 = RGB(137, 104, 205)
"""<div id="rectangle" style="background-color: #8968cd">🎨</div>"""
MEDIUMPURPLE4 = RGB(93, 71, 139)
"""<div id="rectangle" style="background-color: #5d478b">🎨</div>"""
MEDIUMSEAGREEN = RGB(60, 179, 113)
"""<div id="rectangle" style="background-color: #3cb371">🎨</div>"""
MEDIUMSLATEBLUE = RGB(123, 104, 238)
"""<div id="rectangle" style="background-color: #7b68ee">🎨</div>"""
MEDIUMSPRINGGREEN = RGB(0, 250, 154)
"""<div id="rectangle" style="background-color: #00fa9a">🎨</div>"""
MEDIUMTURQUOISE = RGB(72, 209, 204)
"""<div id="rectangle" style="background-color: #48d1cc">🎨</div>"""
MEDIUMVIOLETRED = RGB(199, 21, 133)
"""<div id="rectangle" style="background-color: #c71585">🎨</div>"""
MELON = RGB(227, 168, 105)
"""<div id="rectangle" style="background-color: #e3a869">🎨</div>"""
MIDNIGHTBLUE = RGB(25, 25, 112)
"""<div id="rectangle" style="background-color: #191970">🎨</div>"""
MINT = RGB(189, 252, 201)
"""<div id="rectangle" style="background-color: #bdfcc9">🎨</div>"""
MINTCREAM = RGB(245, 255, 250)
"""<div id="rectangle" style="background-color: #f5fffa">🎨</div>"""
MISTYROSE1 = RGB(255, 228, 225)
"""<div id="rectangle" style="background-color: #ffe4e1">🎨</div>"""
MISTYROSE2 = RGB(238, 213, 210)
"""<div id="rectangle" style="background-color: #eed5d2">🎨</div>"""
MISTYROSE3 = RGB(205, 183, 181)
"""<div id="rectangle" style="background-color: #cdb7b5">🎨</div>"""
MISTYROSE4 = RGB(139, 125, 123)
"""<div id="rectangle" style="background-color: #8b7d7b">🎨</div>"""
MOCCASIN = RGB(255, 228, 181)
"""<div id="rectangle" style="background-color: #ffe4b5">🎨</div>"""
NAVAJOWHITE1 = RGB(255, 222, 173)
"""<div id="rectangle" style="background-color: #ffdead">🎨</div>"""
NAVAJOWHITE2 = RGB(238, 207, 161)
"""<div id="rectangle" style="background-color: #eecfa1">🎨</div>"""
NAVAJOWHITE3 = RGB(205, 179, 139)
"""<div id="rectangle" style="background-color: #cdb38b">🎨</div>"""
NAVAJOWHITE4 = RGB(139, 121, 94)
"""<div id="rectangle" style="background-color: #8b795e">🎨</div>"""
NAVY = RGB(0, 0, 128)
"""<div id="rectangle" style="background-color: #000080">🎨</div>"""
OLDLACE = RGB(253, 245, 230)
"""<div id="rectangle" style="background-color: #fdf5e6">🎨</div>"""
OLIVE = RGB(128, 128, 0)
"""<div id="rectangle" style="background-color: #808000">🎨</div>"""
OLIVEDRAB = RGB(107, 142, 35)
"""<div id="rectangle" style="background-color: #6b8e23">🎨</div>"""
OLIVEDRAB1 = RGB(192, 255, 62)
"""<div id="rectangle" style="background-color: #c0ff3e">🎨</div>"""
OLIVEDRAB2 = RGB(179, 238, 58)
"""<div id="rectangle" style="background-color: #b3ee3a">🎨</div>"""
OLIVEDRAB3 = RGB(154, 205, 50)
"""<div id="rectangle" style="background-color: #9acd32">🎨</div>"""
OLIVEDRAB4 = RGB(105, 139, 34)
"""<div id="rectangle" style="background-color: #698b22">🎨</div>"""
ORANGE = RGB(255, 128, 0)
"""<div id="rectangle" style="background-color: #ff8000">🎨</div>"""
ORANGE1 = RGB(255, 165, 0)
"""<div id="rectangle" style="background-color: #ffa500">🎨</div>"""
ORANGE2 = RGB(238, 154, 0)
"""<div id="rectangle" style="background-color: #ee9a00">🎨</div>"""
ORANGE3 = RGB(205, 133, 0)
"""<div id="rectangle" style="background-color: #cd8500">🎨</div>"""
ORANGE4 = RGB(139, 90, 0)
"""<div id="rectangle" style="background-color: #8b5a00">🎨</div>"""
ORANGERED1 = RGB(255, 69, 0)
"""<div id="rectangle" style="background-color: #ff4500">🎨</div>"""
ORANGERED2 = RGB(238, 64, 0)
"""<div id="rectangle" style="background-color: #ee4000">🎨</div>"""
ORANGERED3 = RGB(205, 55, 0)
"""<div id="rectangle" style="background-color: #cd3700">🎨</div>"""
ORANGERED4 = RGB(139, 37, 0)
"""<div id="rectangle" style="background-color: #8b2500">🎨</div>"""
ORCHID = RGB(218, 112, 214)
"""<div id="rectangle" style="background-color: #da70d6">🎨</div>"""
ORCHID1 = RGB(255, 131, 250)
"""<div id="rectangle" style="background-color: #ff83fa">🎨</div>"""
ORCHID2 = RGB(238, 122, 233)
"""<div id="rectangle" style="background-color: #ee7ae9">🎨</div>"""
ORCHID3 = RGB(205, 105, 201)
"""<div id="rectangle" style="background-color: #cd69c9">🎨</div>"""
ORCHID4 = RGB(139, 71, 137)
"""<div id="rectangle" style="background-color: #8b4789">🎨</div>"""
PALEGOLDENROD = RGB(238, 232, 170)
"""<div id="rectangle" style="background-color: #eee8aa">🎨</div>"""
PALEGREEN = RGB(152, 251, 152)
"""<div id="rectangle" style="background-color: #98fb98">🎨</div>"""
PALEGREEN1 = RGB(154, 255, 154)
"""<div id="rectangle" style="background-color: #9aff9a">🎨</div>"""
PALEGREEN2 = RGB(144, 238, 144)
"""<div id="rectangle" style="background-color: #90ee90">🎨</div>"""
PALEGREEN3 = RGB(124, 205, 124)
"""<div id="rectangle" style="background-color: #7ccd7c">🎨</div>"""
PALEGREEN4 = RGB(84, 139, 84)
"""<div id="rectangle" style="background-color: #548b54">🎨</div>"""
PALETURQUOISE1 = RGB(187, 255, 255)
"""<div id="rectangle" style="background-color: #bbffff">🎨</div>"""
PALETURQUOISE2 = RGB(174, 238, 238)
"""<div id="rectangle" style="background-color: #aeeeee">🎨</div>"""
PALETURQUOISE3 = RGB(150, 205, 205)
"""<div id="rectangle" style="background-color: #96cdcd">🎨</div>"""
PALETURQUOISE4 = RGB(102, 139, 139)
"""<div id="rectangle" style="background-color: #668b8b">🎨</div>"""
PALEVIOLETRED = RGB(219, 112, 147)
"""<div id="rectangle" style="background-color: #db7093">🎨</div>"""
PALEVIOLETRED1 = RGB(255, 130, 171)
"""<div id="rectangle" style="background-color: #ff82ab">🎨</div>"""
PALEVIOLETRED2 = RGB(238, 121, 159)
"""<div id="rectangle" style="background-color: #ee799f">🎨</div>"""
PALEVIOLETRED3 = RGB(205, 104, 137)
"""<div id="rectangle" style="background-color: #cd6889">🎨</div>"""
PALEVIOLETRED4 = RGB(139, 71, 93)
"""<div id="rectangle" style="background-color: #8b475d">🎨</div>"""
PAPAYAWHIP = RGB(255, 239, 213)
"""<div id="rectangle" style="background-color: #ffefd5">🎨</div>"""
PEACHPUFF1 = RGB(255, 218, 185)
"""<div id="rectangle" style="background-color: #ffdab9">🎨</div>"""
PEACHPUFF2 = RGB(238, 203, 173)
"""<div id="rectangle" style="background-color: #eecbad">🎨</div>"""
PEACHPUFF3 = RGB(205, 175, 149)
"""<div id="rectangle" style="background-color: #cdaf95">🎨</div>"""
PEACHPUFF4 = RGB(139, 119, 101)
"""<div id="rectangle" style="background-color: #8b7765">🎨</div>"""
PEACOCK = RGB(51, 161, 201)
"""<div id="rectangle" style="background-color: #33a1c9">🎨</div>"""
PINK = RGB(255, 192, 203)
"""<div id="rectangle" style="background-color: #ffc0cb">🎨</div>"""
PINK1 = RGB(255, 181, 197)
"""<div id="rectangle" style="background-color: #ffb5c5">🎨</div>"""
PINK2 = RGB(238, 169, 184)
"""<div id="rectangle" style="background-color: #eea9b8">🎨</div>"""
PINK3 = RGB(205, 145, 158)
"""<div id="rectangle" style="background-color: #cd919e">🎨</div>"""
PINK4 = RGB(139, 99, 108)
"""<div id="rectangle" style="background-color: #8b636c">🎨</div>"""
PLUM = RGB(221, 160, 221)
"""<div id="rectangle" style="background-color: #dda0dd">🎨</div>"""
PLUM1 = RGB(255, 187, 255)
"""<div id="rectangle" style="background-color: #ffbbff">🎨</div>"""
PLUM2 = RGB(238, 174, 238)
"""<div id="rectangle" style="background-color: #eeaeee">🎨</div>"""
PLUM3 = RGB(205, 150, 205)
"""<div id="rectangle" style="background-color: #cd96cd">🎨</div>"""
PLUM4 = RGB(139, 102, 139)
"""<div id="rectangle" style="background-color: #8b668b">🎨</div>"""
POWDERBLUE = RGB(176, 224, 230)
"""<div id="rectangle" style="background-color: #b0e0e6">🎨</div>"""
PURPLE = RGB(128, 0, 128)
"""<div id="rectangle" style="background-color: #800080">🎨</div>"""
PURPLE1 = RGB(155, 48, 255)
"""<div id="rectangle" style="background-color: #9b30ff">🎨</div>"""
PURPLE2 = RGB(145, 44, 238)
"""<div id="rectangle" style="background-color: #912cee">🎨</div>"""
PURPLE3 = RGB(125, 38, 205)
"""<div id="rectangle" style="background-color: #7d26cd">🎨</div>"""
PURPLE4 = RGB(85, 26, 139)
"""<div id="rectangle" style="background-color: #551a8b">🎨</div>"""
RASPBERRY = RGB(135, 38, 87)
"""<div id="rectangle" style="background-color: #872657">🎨</div>"""
RAWSIENNA = RGB(199, 97, 20)
"""<div id="rectangle" style="background-color: #c76114">🎨</div>"""
RED1 = RGB(255, 0, 0)
"""<div id="rectangle" style="background-color: #ff0000">🎨</div>"""
RED2 = RGB(238, 0, 0)
"""<div id="rectangle" style="background-color: #ee0000">🎨</div>"""
RED3 = RGB(205, 0, 0)
"""<div id="rectangle" style="background-color: #cd0000">🎨</div>"""
RED4 = RGB(139, 0, 0)
"""<div id="rectangle" style="background-color: #8b0000">🎨</div>"""
ROSYBROWN = RGB(188, 143, 143)
"""<div id="rectangle" style="background-color: #bc8f8f">🎨</div>"""
ROSYBROWN1 = RGB(255, 193, 193)
"""<div id="rectangle" style="background-color: #ffc1c1">🎨</div>"""
ROSYBROWN2 = RGB(238, 180, 180)
"""<div id="rectangle" style="background-color: #eeb4b4">🎨</div>"""
ROSYBROWN3 = RGB(205, 155, 155)
"""<div id="rectangle" style="background-color: #cd9b9b">🎨</div>"""
ROSYBROWN4 = RGB(139, 105, 105)
"""<div id="rectangle" style="background-color: #8b6969">🎨</div>"""
ROYALBLUE = RGB(65, 105, 225)
"""<div id="rectangle" style="background-color: #4169e1">🎨</div>"""
ROYALBLUE1 = RGB(72, 118, 255)
"""<div id="rectangle" style="background-color: #4876ff">🎨</div>"""
ROYALBLUE2 = RGB(67, 110, 238)
"""<div id="rectangle" style="background-color: #436eee">🎨</div>"""
ROYALBLUE3 = RGB(58, 95, 205)
"""<div id="rectangle" style="background-color: #3a5fcd">🎨</div>"""
ROYALBLUE4 = RGB(39, 64, 139)
"""<div id="rectangle" style="background-color: #27408b">🎨</div>"""
SALMON = RGB(250, 128, 114)
"""<div id="rectangle" style="background-color: #fa8072">🎨</div>"""
SALMON1 = RGB(255, 140, 105)
"""<div id="rectangle" style="background-color: #ff8c69">🎨</div>"""
SALMON2 = RGB(238, 130, 98)
"""<div id="rectangle" style="background-color: #ee8262">🎨</div>"""
SALMON3 = RGB(205, 112, 84)
"""<div id="rectangle" style="background-color: #cd7054">🎨</div>"""
SALMON4 = RGB(139, 76, 57)
"""<div id="rectangle" style="background-color: #8b4c39">🎨</div>"""
SANDYBROWN = RGB(244, 164, 96)
"""<div id="rectangle" style="background-color: #f4a460">🎨</div>"""
SAPGREEN = RGB(48, 128, 20)
"""<div id="rectangle" style="background-color: #308014">🎨</div>"""
SEAGREEN1 = RGB(84, 255, 159)
"""<div id="rectangle" style="background-color: #54ff9f">🎨</div>"""
SEAGREEN2 = RGB(78, 238, 148)
"""<div id="rectangle" style="background-color: #4eee94">🎨</div>"""
SEAGREEN3 = RGB(67, 205, 128)
"""<div id="rectangle" style="background-color: #43cd80">🎨</div>"""
SEAGREEN4 = RGB(46, 139, 87)
"""<div id="rectangle" style="background-color: #2e8b57">🎨</div>"""
SEASHELL1 = RGB(255, 245, 238)
"""<div id="rectangle" style="background-color: #fff5ee">🎨</div>"""
SEASHELL2 = RGB(238, 229, 222)
"""<div id="rectangle" style="background-color: #eee5de">🎨</div>"""
SEASHELL3 = RGB(205, 197, 191)
"""<div id="rectangle" style="background-color: #cdc5bf">🎨</div>"""
SEASHELL4 = RGB(139, 134, 130)
"""<div id="rectangle" style="background-color: #8b8682">🎨</div>"""
SEPIA = RGB(94, 38, 18)
"""<div id="rectangle" style="background-color: #5e2612">🎨</div>"""
SGIBEET = RGB(142, 56, 142)
"""<div id="rectangle" style="background-color: #8e388e">🎨</div>"""
SGIBRIGHTGRAY = RGB(197, 193, 170)
"""<div id="rectangle" style="background-color: #c5c1aa">🎨</div>"""
SGICHARTREUSE = RGB(113, 198, 113)
"""<div id="rectangle" style="background-color: #71c671">🎨</div>"""
SGIDARKGRAY = RGB(85, 85, 85)
"""<div id="rectangle" style="background-color: #555555">🎨</div>"""
SGIGRAY12 = RGB(30, 30, 30)
"""<div id="rectangle" style="background-color: #1e1e1e">🎨</div>"""
SGIGRAY16 = RGB(40, 40, 40)
"""<div id="rectangle" style="background-color: #282828">🎨</div>"""
SGIGRAY32 = RGB(81, 81, 81)
"""<div id="rectangle" style="background-color: #515151">🎨</div>"""
SGIGRAY36 = RGB(91, 91, 91)
"""<div id="rectangle" style="background-color: #5b5b5b">🎨</div>"""
SGIGRAY52 = RGB(132, 132, 132)
"""<div id="rectangle" style="background-color: #848484">🎨</div>"""
SGIGRAY56 = RGB(142, 142, 142)
"""<div id="rectangle" style="background-color: #8e8e8e">🎨</div>"""
SGIGRAY72 = RGB(183, 183, 183)
"""<div id="rectangle" style="background-color: #b7b7b7">🎨</div>"""
SGIGRAY76 = RGB(193, 193, 193)
"""<div id="rectangle" style="background-color: #c1c1c1">🎨</div>"""
SGIGRAY92 = RGB(234, 234, 234)
"""<div id="rectangle" style="background-color: #eaeaea">🎨</div>"""
SGIGRAY96 = RGB(244, 244, 244)
"""<div id="rectangle" style="background-color: #f4f4f4">🎨</div>"""
SGILIGHTBLUE = RGB(125, 158, 192)
"""<div id="rectangle" style="background-color: #7d9ec0">🎨</div>"""
SGILIGHTGRAY = RGB(170, 170, 170)
"""<div id="rectangle" style="background-color: #aaaaaa">🎨</div>"""
SGIOLIVEDRAB = RGB(142, 142, 56)
"""<div id="rectangle" style="background-color: #8e8e38">🎨</div>"""
SGISALMON = RGB(198, 113, 113)
"""<div id="rectangle" style="background-color: #c67171">🎨</div>"""
SGISLATEBLUE = RGB(113, 113, 198)
"""<div id="rectangle" style="background-color: #7171c6">🎨</div>"""
SGITEAL = RGB(56, 142, 142)
"""<div id="rectangle" style="background-color: #388e8e">🎨</div>"""
SIENNA = RGB(160, 82, 45)
"""<div id="rectangle" style="background-color: #a0522d">🎨</div>"""
SIENNA1 = RGB(255, 130, 71)
"""<div id="rectangle" style="background-color: #ff8247">🎨</div>"""
SIENNA2 = RGB(238, 121, 66)
"""<div id="rectangle" style="background-color: #ee7942">🎨</div>"""
SIENNA3 = RGB(205, 104, 57)
"""<div id="rectangle" style="background-color: #cd6839">🎨</div>"""
SIENNA4 = RGB(139, 71, 38)
"""<div id="rectangle" style="background-color: #8b4726">🎨</div>"""
SILVER = RGB(192, 192, 192)
"""<div id="rectangle" style="background-color: #c0c0c0">🎨</div>"""
SKYBLUE = RGB(135, 206, 235)
"""<div id="rectangle" style="background-color: #87ceeb">🎨</div>"""
SKYBLUE1 = RGB(135, 206, 255)
"""<div id="rectangle" style="background-color: #87ceff">🎨</div>"""
SKYBLUE2 = RGB(126, 192, 238)
"""<div id="rectangle" style="background-color: #7ec0ee">🎨</div>"""
SKYBLUE3 = RGB(108, 166, 205)
"""<div id="rectangle" style="background-color: #6ca6cd">🎨</div>"""
SKYBLUE4 = RGB(74, 112, 139)
"""<div id="rectangle" style="background-color: #4a708b">🎨</div>"""
SLATEBLUE = RGB(106, 90, 205)
"""<div id="rectangle" style="background-color: #6a5acd">🎨</div>"""
SLATEBLUE1 = RGB(131, 111, 255)
"""<div id="rectangle" style="background-color: #836fff">🎨</div>"""
SLATEBLUE2 = RGB(122, 103, 238)
"""<div id="rectangle" style="background-color: #7a67ee">🎨</div>"""
SLATEBLUE3 = RGB(105, 89, 205)
"""<div id="rectangle" style="background-color: #6959cd">🎨</div>"""
SLATEBLUE4 = RGB(71, 60, 139)
"""<div id="rectangle" style="background-color: #473c8b">🎨</div>"""
SLATEGRAY = RGB(112, 128, 144)
"""<div id="rectangle" style="background-color: #708090">🎨</div>"""
SLATEGRAY1 = RGB(198, 226, 255)
"""<div id="rectangle" style="background-color: #c6e2ff">🎨</div>"""
SLATEGRAY2 = RGB(185, 211, 238)
"""<div id="rectangle" style="background-color: #b9d3ee">🎨</div>"""
SLATEGRAY3 = RGB(159, 182, 205)
"""<div id="rectangle" style="background-color: #9fb6cd">🎨</div>"""
SLATEGRAY4 = RGB(108, 123, 139)
"""<div id="rectangle" style="background-color: #6c7b8b">🎨</div>"""
SNOW1 = RGB(255, 250, 250)
"""<div id="rectangle" style="background-color: #fffafa">🎨</div>"""
SNOW2 = RGB(238, 233, 233)
"""<div id="rectangle" style="background-color: #eee9e9">🎨</div>"""
SNOW3 = RGB(205, 201, 201)
"""<div id="rectangle" style="background-color: #cdc9c9">🎨</div>"""
SNOW4 = RGB(139, 137, 137)
"""<div id="rectangle" style="background-color: #8b8989">🎨</div>"""
SPRINGGREEN = RGB(0, 255, 127)
"""<div id="rectangle" style="background-color: #00ff7f">🎨</div>"""
SPRINGGREEN1 = RGB(0, 238, 118)
"""<div id="rectangle" style="background-color: #00ee76">🎨</div>"""
SPRINGGREEN2 = RGB(0, 205, 102)
"""<div id="rectangle" style="background-color: #00cd66">🎨</div>"""
SPRINGGREEN3 = RGB(0, 139, 69)
"""<div id="rectangle" style="background-color: #008b45">🎨</div>"""
STEELBLUE = RGB(70, 130, 180)
"""<div id="rectangle" style="background-color: #4682b4">🎨</div>"""
STEELBLUE1 = RGB(99, 184, 255)
"""<div id="rectangle" style="background-color: #63b8ff">🎨</div>"""
STEELBLUE2 = RGB(92, 172, 238)
"""<div id="rectangle" style="background-color: #5cacee">🎨</div>"""
STEELBLUE3 = RGB(79, 148, 205)
"""<div id="rectangle" style="background-color: #4f94cd">🎨</div>"""
STEELBLUE4 = RGB(54, 100, 139)
"""<div id="rectangle" style="background-color: #36648b">🎨</div>"""
TAN = RGB(210, 180, 140)
"""<div id="rectangle" style="background-color: #d2b48c">🎨</div>"""
TAN1 = RGB(255, 165, 79)
"""<div id="rectangle" style="background-color: #ffa54f">🎨</div>"""
TAN2 = RGB(238, 154, 73)
"""<div id="rectangle" style="background-color: #ee9a49">🎨</div>"""
TAN3 = RGB(205, 133, 63)
"""<div id="rectangle" style="background-color: #cd853f">🎨</div>"""
TAN4 = RGB(139, 90, 43)
"""<div id="rectangle" style="background-color: #8b5a2b">🎨</div>"""
TEAL = RGB(0, 128, 128)
"""<div id="rectangle" style="background-color: #008080">🎨</div>"""
THISTLE = RGB(216, 191, 216)
"""<div id="rectangle" style="background-color: #d8bfd8">🎨</div>"""
THISTLE1 = RGB(255, 225, 255)
"""<div id="rectangle" style="background-color: #ffe1ff">🎨</div>"""
THISTLE2 = RGB(238, 210, 238)
"""<div id="rectangle" style="background-color: #eed2ee">🎨</div>"""
THISTLE3 = RGB(205, 181, 205)
"""<div id="rectangle" style="background-color: #cdb5cd">🎨</div>"""
THISTLE4 = RGB(139, 123, 139)
"""<div id="rectangle" style="background-color: #8b7b8b">🎨</div>"""
TOMATO1 = RGB(255, 99, 71)
"""<div id="rectangle" style="background-color: #ff6347">🎨</div>"""
TOMATO2 = RGB(238, 92, 66)
"""<div id="rectangle" style="background-color: #ee5c42">🎨</div>"""
TOMATO3 = RGB(205, 79, 57)
"""<div id="rectangle" style="background-color: #cd4f39">🎨</div>"""
TOMATO4 = RGB(139, 54, 38)
"""<div id="rectangle" style="background-color: #8b3626">🎨</div>"""
TURQUOISE = RGB(64, 224, 208)
"""<div id="rectangle" style="background-color: #40e0d0">🎨</div>"""
TURQUOISE1 = RGB(0, 245, 255)
"""<div id="rectangle" style="background-color: #00f5ff">🎨</div>"""
TURQUOISE2 = RGB(0, 229, 238)
"""<div id="rectangle" style="background-color: #00e5ee">🎨</div>"""
TURQUOISE3 = RGB(0, 197, 205)
"""<div id="rectangle" style="background-color: #00c5cd">🎨</div>"""
TURQUOISE4 = RGB(0, 134, 139)
"""<div id="rectangle" style="background-color: #00868b">🎨</div>"""
TURQUOISEBLUE = RGB(0, 199, 140)
"""<div id="rectangle" style="background-color: #00c78c">🎨</div>"""
VIOLET = RGB(238, 130, 238)
"""<div id="rectangle" style="background-color: #ee82ee">🎨</div>"""
VIOLETRED = RGB(208, 32, 144)
"""<div id="rectangle" style="background-color: #d02090">🎨</div>"""
VIOLETRED1 = RGB(255, 62, 150)
"""<div id="rectangle" style="background-color: #ff3e96">🎨</div>"""
VIOLETRED2 = RGB(238, 58, 140)
"""<div id="rectangle" style="background-color: #ee3a8c">🎨</div>"""
VIOLETRED3 = RGB(205, 50, 120)
"""<div id="rectangle" style="background-color: #cd3278">🎨</div>"""
VIOLETRED4 = RGB(139, 34, 82)
"""<div id="rectangle" style="background-color: #8b2252">🎨</div>"""
WARMGREY = RGB(128, 128, 105)
"""<div id="rectangle" style="background-color: #808069">🎨</div>"""
WHEAT = RGB(245, 222, 179)
"""<div id="rectangle" style="background-color: #f5deb3">🎨</div>"""
WHEAT1 = RGB(255, 231, 186)
"""<div id="rectangle" style="background-color: #ffe7ba">🎨</div>"""
WHEAT2 = RGB(238, 216, 174)
"""<div id="rectangle" style="background-color: #eed8ae">🎨</div>"""
WHEAT3 = RGB(205, 186, 150)
"""<div id="rectangle" style="background-color: #cdba96">🎨</div>"""
WHEAT4 = RGB(139, 126, 102)
"""<div id="rectangle" style="background-color: #8b7e66">🎨</div>"""
WHITE = RGB(255, 255, 255)
"""<div id="rectangle" style="background-color: #ffffff">🎨</div>"""
WHITESMOKE = RGB(245, 245, 245)
"""<div id="rectangle" style="background-color: #f5f5f5">🎨</div>"""
YELLOW1 = RGB(255, 255, 0)
"""<div id="rectangle" style="background-color: #ffff00">🎨</div>"""
YELLOW2 = RGB(238, 238, 0)
"""<div id="rectangle" style="background-color: #eeee00">🎨</div>"""
YELLOW3 = RGB(205, 205, 0)
"""<div id="rectangle" style="background-color: #cdcd00">🎨</div>"""
YELLOW4 = RGB(139, 139, 0)
"""<div id="rectangle" style="background-color: #8b8b00">🎨</div>"""

# Add colors to colors dictionary
colors["aliceblue"] = ALICEBLUE
colors["antiquewhite"] = ANTIQUEWHITE
colors["antiquewhite1"] = ANTIQUEWHITE1
colors["antiquewhite2"] = ANTIQUEWHITE2
colors["antiquewhite3"] = ANTIQUEWHITE3
colors["antiquewhite4"] = ANTIQUEWHITE4
colors["aqua"] = AQUA
colors["aquamarine1"] = AQUAMARINE1
colors["aquamarine2"] = AQUAMARINE2
colors["aquamarine3"] = AQUAMARINE3
colors["aquamarine4"] = AQUAMARINE4
colors["azure1"] = AZURE1
colors["azure2"] = AZURE2
colors["azure3"] = AZURE3
colors["azure4"] = AZURE4
colors["banana"] = BANANA
colors["beige"] = BEIGE
colors["bisque1"] = BISQUE1
colors["bisque2"] = BISQUE2
colors["bisque3"] = BISQUE3
colors["bisque4"] = BISQUE4
colors["black"] = BLACK
colors["blanchedalmond"] = BLANCHEDALMOND
colors["blue"] = BLUE
colors["blue2"] = BLUE2
colors["blue3"] = BLUE3
colors["blue4"] = BLUE4
colors["blueviolet"] = BLUEVIOLET
colors["brick"] = BRICK
colors["brown"] = BROWN
colors["brown1"] = BROWN1
colors["brown2"] = BROWN2
colors["brown3"] = BROWN3
colors["brown4"] = BROWN4
colors["burlywood"] = BURLYWOOD
colors["burlywood1"] = BURLYWOOD1
colors["burlywood2"] = BURLYWOOD2
colors["burlywood3"] = BURLYWOOD3
colors["burlywood4"] = BURLYWOOD4
colors["burntsienna"] = BURNTSIENNA
colors["burntumber"] = BURNTUMBER
colors["cadetblue"] = CADETBLUE
colors["cadetblue1"] = CADETBLUE1
colors["cadetblue2"] = CADETBLUE2
colors["cadetblue3"] = CADETBLUE3
colors["cadetblue4"] = CADETBLUE4
colors["cadmiumorange"] = CADMIUMORANGE
colors["cadmiumyellow"] = CADMIUMYELLOW
colors["carrot"] = CARROT
colors["chartreuse1"] = CHARTREUSE1
colors["chartreuse2"] = CHARTREUSE2
colors["chartreuse3"] = CHARTREUSE3
colors["chartreuse4"] = CHARTREUSE4
colors["chocolate"] = CHOCOLATE
colors["chocolate1"] = CHOCOLATE1
colors["chocolate2"] = CHOCOLATE2
colors["chocolate3"] = CHOCOLATE3
colors["chocolate4"] = CHOCOLATE4
colors["cobalt"] = COBALT
colors["cobaltgreen"] = COBALTGREEN
colors["coldgrey"] = COLDGREY
colors["coral"] = CORAL
colors["coral1"] = CORAL1
colors["coral2"] = CORAL2
colors["coral3"] = CORAL3
colors["coral4"] = CORAL4
colors["cornflowerblue"] = CORNFLOWERBLUE
colors["cornsilk1"] = CORNSILK1
colors["cornsilk2"] = CORNSILK2
colors["cornsilk3"] = CORNSILK3
colors["cornsilk4"] = CORNSILK4
colors["crimson"] = CRIMSON
colors["cyan2"] = CYAN2
colors["cyan3"] = CYAN3
colors["cyan4"] = CYAN4
colors["darkgoldenrod"] = DARKGOLDENROD
colors["darkgoldenrod1"] = DARKGOLDENROD1
colors["darkgoldenrod2"] = DARKGOLDENROD2
colors["darkgoldenrod3"] = DARKGOLDENROD3
colors["darkgoldenrod4"] = DARKGOLDENROD4
colors["darkgray"] = DARKGRAY
colors["darkgreen"] = DARKGREEN
colors["darkkhaki"] = DARKKHAKI
colors["darkolivegreen"] = DARKOLIVEGREEN
colors["darkolivegreen1"] = DARKOLIVEGREEN1
colors["darkolivegreen2"] = DARKOLIVEGREEN2
colors["darkolivegreen3"] = DARKOLIVEGREEN3
colors["darkolivegreen4"] = DARKOLIVEGREEN4
colors["darkorange"] = DARKORANGE
colors["darkorange1"] = DARKORANGE1
colors["darkorange2"] = DARKORANGE2
colors["darkorange3"] = DARKORANGE3
colors["darkorange4"] = DARKORANGE4
colors["darkorchid"] = DARKORCHID
colors["darkorchid1"] = DARKORCHID1
colors["darkorchid2"] = DARKORCHID2
colors["darkorchid3"] = DARKORCHID3
colors["darkorchid4"] = DARKORCHID4
colors["darksalmon"] = DARKSALMON
colors["darkseagreen"] = DARKSEAGREEN
colors["darkseagreen1"] = DARKSEAGREEN1
colors["darkseagreen2"] = DARKSEAGREEN2
colors["darkseagreen3"] = DARKSEAGREEN3
colors["darkseagreen4"] = DARKSEAGREEN4
colors["darkslateblue"] = DARKSLATEBLUE
colors["darkslategray"] = DARKSLATEGRAY
colors["darkslategray1"] = DARKSLATEGRAY1
colors["darkslategray2"] = DARKSLATEGRAY2
colors["darkslategray3"] = DARKSLATEGRAY3
colors["darkslategray4"] = DARKSLATEGRAY4
colors["darkturquoise"] = DARKTURQUOISE
colors["darkviolet"] = DARKVIOLET
colors["deeppink1"] = DEEPPINK1
colors["deeppink2"] = DEEPPINK2
colors["deeppink3"] = DEEPPINK3
colors["deeppink4"] = DEEPPINK4
colors["deepskyblue1"] = DEEPSKYBLUE1
colors["deepskyblue2"] = DEEPSKYBLUE2
colors["deepskyblue3"] = DEEPSKYBLUE3
colors["deepskyblue4"] = DEEPSKYBLUE4
colors["dimgray"] = DIMGRAY
colors["dimgray"] = DIMGRAY
colors["dodgerblue1"] = DODGERBLUE1
colors["dodgerblue2"] = DODGERBLUE2
colors["dodgerblue3"] = DODGERBLUE3
colors["dodgerblue4"] = DODGERBLUE4
colors["eggshell"] = EGGSHELL
colors["emeraldgreen"] = EMERALDGREEN
colors["firebrick"] = FIREBRICK
colors["firebrick1"] = FIREBRICK1
colors["firebrick2"] = FIREBRICK2
colors["firebrick3"] = FIREBRICK3
colors["firebrick4"] = FIREBRICK4
colors["flesh"] = FLESH
colors["floralwhite"] = FLORALWHITE
colors["forestgreen"] = FORESTGREEN
colors["gainsboro"] = GAINSBORO
colors["ghostwhite"] = GHOSTWHITE
colors["gold1"] = GOLD1
colors["gold2"] = GOLD2
colors["gold3"] = GOLD3
colors["gold4"] = GOLD4
colors["goldenrod"] = GOLDENROD
colors["goldenrod1"] = GOLDENROD1
colors["goldenrod2"] = GOLDENROD2
colors["goldenrod3"] = GOLDENROD3
colors["goldenrod4"] = GOLDENROD4
colors["gray"] = GRAY
colors["gray1"] = GRAY1
colors["gray10"] = GRAY10
colors["gray11"] = GRAY11
colors["gray12"] = GRAY12
colors["gray13"] = GRAY13
colors["gray14"] = GRAY14
colors["gray15"] = GRAY15
colors["gray16"] = GRAY16
colors["gray17"] = GRAY17
colors["gray18"] = GRAY18
colors["gray19"] = GRAY19
colors["gray2"] = GRAY2
colors["gray20"] = GRAY20
colors["gray21"] = GRAY21
colors["gray22"] = GRAY22
colors["gray23"] = GRAY23
colors["gray24"] = GRAY24
colors["gray25"] = GRAY25
colors["gray26"] = GRAY26
colors["gray27"] = GRAY27
colors["gray28"] = GRAY28
colors["gray29"] = GRAY29
colors["gray3"] = GRAY3
colors["gray30"] = GRAY30
colors["gray31"] = GRAY31
colors["gray32"] = GRAY32
colors["gray33"] = GRAY33
colors["gray34"] = GRAY34
colors["gray35"] = GRAY35
colors["gray36"] = GRAY36
colors["gray37"] = GRAY37
colors["gray38"] = GRAY38
colors["gray39"] = GRAY39
colors["gray4"] = GRAY4
colors["gray40"] = GRAY40
colors["gray42"] = GRAY42
colors["gray43"] = GRAY43
colors["gray44"] = GRAY44
colors["gray45"] = GRAY45
colors["gray46"] = GRAY46
colors["gray47"] = GRAY47
colors["gray48"] = GRAY48
colors["gray49"] = GRAY49
colors["gray5"] = GRAY5
colors["gray50"] = GRAY50
colors["gray51"] = GRAY51
colors["gray52"] = GRAY52
colors["gray53"] = GRAY53
colors["gray54"] = GRAY54
colors["gray55"] = GRAY55
colors["gray56"] = GRAY56
colors["gray57"] = GRAY57
colors["gray58"] = GRAY58
colors["gray59"] = GRAY59
colors["gray6"] = GRAY6
colors["gray60"] = GRAY60
colors["gray61"] = GRAY61
colors["gray62"] = GRAY62
colors["gray63"] = GRAY63
colors["gray64"] = GRAY64
colors["gray65"] = GRAY65
colors["gray66"] = GRAY66
colors["gray67"] = GRAY67
colors["gray68"] = GRAY68
colors["gray69"] = GRAY69
colors["gray7"] = GRAY7
colors["gray70"] = GRAY70
colors["gray71"] = GRAY71
colors["gray72"] = GRAY72
colors["gray73"] = GRAY73
colors["gray74"] = GRAY74
colors["gray75"] = GRAY75
colors["gray76"] = GRAY76
colors["gray77"] = GRAY77
colors["gray78"] = GRAY78
colors["gray79"] = GRAY79
colors["gray8"] = GRAY8
colors["gray80"] = GRAY80
colors["gray81"] = GRAY81
colors["gray82"] = GRAY82
colors["gray83"] = GRAY83
colors["gray84"] = GRAY84
colors["gray85"] = GRAY85
colors["gray86"] = GRAY86
colors["gray87"] = GRAY87
colors["gray88"] = GRAY88
colors["gray89"] = GRAY89
colors["gray9"] = GRAY9
colors["gray90"] = GRAY90
colors["gray91"] = GRAY91
colors["gray92"] = GRAY92
colors["gray93"] = GRAY93
colors["gray94"] = GRAY94
colors["gray95"] = GRAY95
colors["gray97"] = GRAY97
colors["gray98"] = GRAY98
colors["gray99"] = GRAY99
colors["green"] = GREEN
colors["green1"] = GREEN1
colors["green2"] = GREEN2
colors["green3"] = GREEN3
colors["green4"] = GREEN4
colors["greenyellow"] = GREENYELLOW
colors["honeydew1"] = HONEYDEW1
colors["honeydew2"] = HONEYDEW2
colors["honeydew3"] = HONEYDEW3
colors["honeydew4"] = HONEYDEW4
colors["hotpink"] = HOTPINK
colors["hotpink1"] = HOTPINK1
colors["hotpink2"] = HOTPINK2
colors["hotpink3"] = HOTPINK3
colors["hotpink4"] = HOTPINK4
colors["indianred"] = INDIANRED
colors["indianred"] = INDIANRED
colors["indianred1"] = INDIANRED1
colors["indianred2"] = INDIANRED2
colors["indianred3"] = INDIANRED3
colors["indianred4"] = INDIANRED4
colors["indigo"] = INDIGO
colors["ivory1"] = IVORY1
colors["ivory2"] = IVORY2
colors["ivory3"] = IVORY3
colors["ivory4"] = IVORY4
colors["ivoryblack"] = IVORYBLACK
colors["khaki"] = KHAKI
colors["khaki1"] = KHAKI1
colors["khaki2"] = KHAKI2
colors["khaki3"] = KHAKI3
colors["khaki4"] = KHAKI4
colors["lavender"] = LAVENDER
colors["lavenderblush1"] = LAVENDERBLUSH1
colors["lavenderblush2"] = LAVENDERBLUSH2
colors["lavenderblush3"] = LAVENDERBLUSH3
colors["lavenderblush4"] = LAVENDERBLUSH4
colors["lawngreen"] = LAWNGREEN
colors["lemonchiffon1"] = LEMONCHIFFON1
colors["lemonchiffon2"] = LEMONCHIFFON2
colors["lemonchiffon3"] = LEMONCHIFFON3
colors["lemonchiffon4"] = LEMONCHIFFON4
colors["lightblue"] = LIGHTBLUE
colors["lightblue1"] = LIGHTBLUE1
colors["lightblue2"] = LIGHTBLUE2
colors["lightblue3"] = LIGHTBLUE3
colors["lightblue4"] = LIGHTBLUE4
colors["lightcoral"] = LIGHTCORAL
colors["lightcyan1"] = LIGHTCYAN1
colors["lightcyan2"] = LIGHTCYAN2
colors["lightcyan3"] = LIGHTCYAN3
colors["lightcyan4"] = LIGHTCYAN4
colors["lightgoldenrod1"] = LIGHTGOLDENROD1
colors["lightgoldenrod2"] = LIGHTGOLDENROD2
colors["lightgoldenrod3"] = LIGHTGOLDENROD3
colors["lightgoldenrod4"] = LIGHTGOLDENROD4
colors["lightgoldenrodyellow"] = LIGHTGOLDENRODYELLOW
colors["lightgrey"] = LIGHTGREY
colors["lightpink"] = LIGHTPINK
colors["lightpink1"] = LIGHTPINK1
colors["lightpink2"] = LIGHTPINK2
colors["lightpink3"] = LIGHTPINK3
colors["lightpink4"] = LIGHTPINK4
colors["lightsalmon1"] = LIGHTSALMON1
colors["lightsalmon2"] = LIGHTSALMON2
colors["lightsalmon3"] = LIGHTSALMON3
colors["lightsalmon4"] = LIGHTSALMON4
colors["lightseagreen"] = LIGHTSEAGREEN
colors["lightskyblue"] = LIGHTSKYBLUE
colors["lightskyblue1"] = LIGHTSKYBLUE1
colors["lightskyblue2"] = LIGHTSKYBLUE2
colors["lightskyblue3"] = LIGHTSKYBLUE3
colors["lightskyblue4"] = LIGHTSKYBLUE4
colors["lightslateblue"] = LIGHTSLATEBLUE
colors["lightslategray"] = LIGHTSLATEGRAY
colors["lightsteelblue"] = LIGHTSTEELBLUE
colors["lightsteelblue1"] = LIGHTSTEELBLUE1
colors["lightsteelblue2"] = LIGHTSTEELBLUE2
colors["lightsteelblue3"] = LIGHTSTEELBLUE3
colors["lightsteelblue4"] = LIGHTSTEELBLUE4
colors["lightyellow1"] = LIGHTYELLOW1
colors["lightyellow2"] = LIGHTYELLOW2
colors["lightyellow3"] = LIGHTYELLOW3
colors["lightyellow4"] = LIGHTYELLOW4
colors["limegreen"] = LIMEGREEN
colors["linen"] = LINEN
colors["magenta"] = MAGENTA
colors["magenta2"] = MAGENTA2
colors["magenta3"] = MAGENTA3
colors["magenta4"] = MAGENTA4
colors["manganeseblue"] = MANGANESEBLUE
colors["maroon"] = MAROON
colors["maroon1"] = MAROON1
colors["maroon2"] = MAROON2
colors["maroon3"] = MAROON3
colors["maroon4"] = MAROON4
colors["mediumorchid"] = MEDIUMORCHID
colors["mediumorchid1"] = MEDIUMORCHID1
colors["mediumorchid2"] = MEDIUMORCHID2
colors["mediumorchid3"] = MEDIUMORCHID3
colors["mediumorchid4"] = MEDIUMORCHID4
colors["mediumpurple"] = MEDIUMPURPLE
colors["mediumpurple1"] = MEDIUMPURPLE1
colors["mediumpurple2"] = MEDIUMPURPLE2
colors["mediumpurple3"] = MEDIUMPURPLE3
colors["mediumpurple4"] = MEDIUMPURPLE4
colors["mediumseagreen"] = MEDIUMSEAGREEN
colors["mediumslateblue"] = MEDIUMSLATEBLUE
colors["mediumspringgreen"] = MEDIUMSPRINGGREEN
colors["mediumturquoise"] = MEDIUMTURQUOISE
colors["mediumvioletred"] = MEDIUMVIOLETRED
colors["melon"] = MELON
colors["midnightblue"] = MIDNIGHTBLUE
colors["mint"] = MINT
colors["mintcream"] = MINTCREAM
colors["mistyrose1"] = MISTYROSE1
colors["mistyrose2"] = MISTYROSE2
colors["mistyrose3"] = MISTYROSE3
colors["mistyrose4"] = MISTYROSE4
colors["moccasin"] = MOCCASIN
colors["navajowhite1"] = NAVAJOWHITE1
colors["navajowhite2"] = NAVAJOWHITE2
colors["navajowhite3"] = NAVAJOWHITE3
colors["navajowhite4"] = NAVAJOWHITE4
colors["navy"] = NAVY
colors["oldlace"] = OLDLACE
colors["olive"] = OLIVE
colors["olivedrab"] = OLIVEDRAB
colors["olivedrab1"] = OLIVEDRAB1
colors["olivedrab2"] = OLIVEDRAB2
colors["olivedrab3"] = OLIVEDRAB3
colors["olivedrab4"] = OLIVEDRAB4
colors["orange"] = ORANGE
colors["orange1"] = ORANGE1
colors["orange2"] = ORANGE2
colors["orange3"] = ORANGE3
colors["orange4"] = ORANGE4
colors["orangered1"] = ORANGERED1
colors["orangered2"] = ORANGERED2
colors["orangered3"] = ORANGERED3
colors["orangered4"] = ORANGERED4
colors["orchid"] = ORCHID
colors["orchid1"] = ORCHID1
colors["orchid2"] = ORCHID2
colors["orchid3"] = ORCHID3
colors["orchid4"] = ORCHID4
colors["palegoldenrod"] = PALEGOLDENROD
colors["palegreen"] = PALEGREEN
colors["palegreen1"] = PALEGREEN1
colors["palegreen2"] = PALEGREEN2
colors["palegreen3"] = PALEGREEN3
colors["palegreen4"] = PALEGREEN4
colors["paleturquoise1"] = PALETURQUOISE1
colors["paleturquoise2"] = PALETURQUOISE2
colors["paleturquoise3"] = PALETURQUOISE3
colors["paleturquoise4"] = PALETURQUOISE4
colors["palevioletred"] = PALEVIOLETRED
colors["palevioletred1"] = PALEVIOLETRED1
colors["palevioletred2"] = PALEVIOLETRED2
colors["palevioletred3"] = PALEVIOLETRED3
colors["palevioletred4"] = PALEVIOLETRED4
colors["papayawhip"] = PAPAYAWHIP
colors["peachpuff1"] = PEACHPUFF1
colors["peachpuff2"] = PEACHPUFF2
colors["peachpuff3"] = PEACHPUFF3
colors["peachpuff4"] = PEACHPUFF4
colors["peacock"] = PEACOCK
colors["pink"] = PINK
colors["pink1"] = PINK1
colors["pink2"] = PINK2
colors["pink3"] = PINK3
colors["pink4"] = PINK4
colors["plum"] = PLUM
colors["plum1"] = PLUM1
colors["plum2"] = PLUM2
colors["plum3"] = PLUM3
colors["plum4"] = PLUM4
colors["powderblue"] = POWDERBLUE
colors["purple"] = PURPLE
colors["purple1"] = PURPLE1
colors["purple2"] = PURPLE2
colors["purple3"] = PURPLE3
colors["purple4"] = PURPLE4
colors["raspberry"] = RASPBERRY
colors["rawsienna"] = RAWSIENNA
colors["red"] = RED1
colors["red1"] = RED1
colors["red2"] = RED2
colors["red3"] = RED3
colors["red4"] = RED4
colors["rosybrown"] = ROSYBROWN
colors["rosybrown1"] = ROSYBROWN1
colors["rosybrown2"] = ROSYBROWN2
colors["rosybrown3"] = ROSYBROWN3
colors["rosybrown4"] = ROSYBROWN4
colors["royalblue"] = ROYALBLUE
colors["royalblue1"] = ROYALBLUE1
colors["royalblue2"] = ROYALBLUE2
colors["royalblue3"] = ROYALBLUE3
colors["royalblue4"] = ROYALBLUE4
colors["salmon"] = SALMON
colors["salmon1"] = SALMON1
colors["salmon2"] = SALMON2
colors["salmon3"] = SALMON3
colors["salmon4"] = SALMON4
colors["sandybrown"] = SANDYBROWN
colors["sapgreen"] = SAPGREEN
colors["seagreen1"] = SEAGREEN1
colors["seagreen2"] = SEAGREEN2
colors["seagreen3"] = SEAGREEN3
colors["seagreen4"] = SEAGREEN4
colors["seashell1"] = SEASHELL1
colors["seashell2"] = SEASHELL2
colors["seashell3"] = SEASHELL3
colors["seashell4"] = SEASHELL4
colors["sepia"] = SEPIA
colors["sgibeet"] = SGIBEET
colors["sgibrightgray"] = SGIBRIGHTGRAY
colors["sgichartreuse"] = SGICHARTREUSE
colors["sgidarkgray"] = SGIDARKGRAY
colors["sgigray12"] = SGIGRAY12
colors["sgigray16"] = SGIGRAY16
colors["sgigray32"] = SGIGRAY32
colors["sgigray36"] = SGIGRAY36
colors["sgigray52"] = SGIGRAY52
colors["sgigray56"] = SGIGRAY56
colors["sgigray72"] = SGIGRAY72
colors["sgigray76"] = SGIGRAY76
colors["sgigray92"] = SGIGRAY92
colors["sgigray96"] = SGIGRAY96
colors["sgilightblue"] = SGILIGHTBLUE
colors["sgilightgray"] = SGILIGHTGRAY
colors["sgiolivedrab"] = SGIOLIVEDRAB
colors["sgisalmon"] = SGISALMON
colors["sgislateblue"] = SGISLATEBLUE
colors["sgiteal"] = SGITEAL
colors["sienna"] = SIENNA
colors["sienna1"] = SIENNA1
colors["sienna2"] = SIENNA2
colors["sienna3"] = SIENNA3
colors["sienna4"] = SIENNA4
colors["silver"] = SILVER
colors["skyblue"] = SKYBLUE
colors["skyblue1"] = SKYBLUE1
colors["skyblue2"] = SKYBLUE2
colors["skyblue3"] = SKYBLUE3
colors["skyblue4"] = SKYBLUE4
colors["slateblue"] = SLATEBLUE
colors["slateblue1"] = SLATEBLUE1
colors["slateblue2"] = SLATEBLUE2
colors["slateblue3"] = SLATEBLUE3
colors["slateblue4"] = SLATEBLUE4
colors["slategray"] = SLATEGRAY
colors["slategray1"] = SLATEGRAY1
colors["slategray2"] = SLATEGRAY2
colors["slategray3"] = SLATEGRAY3
colors["slategray4"] = SLATEGRAY4
colors["snow1"] = SNOW1
colors["snow2"] = SNOW2
colors["snow3"] = SNOW3
colors["snow4"] = SNOW4
colors["springgreen"] = SPRINGGREEN
colors["springgreen1"] = SPRINGGREEN1
colors["springgreen2"] = SPRINGGREEN2
colors["springgreen3"] = SPRINGGREEN3
colors["steelblue"] = STEELBLUE
colors["steelblue1"] = STEELBLUE1
colors["steelblue2"] = STEELBLUE2
colors["steelblue3"] = STEELBLUE3
colors["steelblue4"] = STEELBLUE4
colors["tan"] = TAN
colors["tan1"] = TAN1
colors["tan2"] = TAN2
colors["tan3"] = TAN3
colors["tan4"] = TAN4
colors["teal"] = TEAL
colors["thistle"] = THISTLE
colors["thistle1"] = THISTLE1
colors["thistle2"] = THISTLE2
colors["thistle3"] = THISTLE3
colors["thistle4"] = THISTLE4
colors["tomato1"] = TOMATO1
colors["tomato2"] = TOMATO2
colors["tomato3"] = TOMATO3
colors["tomato4"] = TOMATO4
colors["turquoise"] = TURQUOISE
colors["turquoise1"] = TURQUOISE1
colors["turquoise2"] = TURQUOISE2
colors["turquoise3"] = TURQUOISE3
colors["turquoise4"] = TURQUOISE4
colors["turquoiseblue"] = TURQUOISEBLUE
colors["violet"] = VIOLET
colors["violetred"] = VIOLETRED
colors["violetred1"] = VIOLETRED1
colors["violetred2"] = VIOLETRED2
colors["violetred3"] = VIOLETRED3
colors["violetred4"] = VIOLETRED4
colors["warmgrey"] = WARMGREY
colors["wheat"] = WHEAT
colors["wheat1"] = WHEAT1
colors["wheat2"] = WHEAT2
colors["wheat3"] = WHEAT3
colors["wheat4"] = WHEAT4
colors["white"] = WHITE
colors["whitesmoke"] = WHITESMOKE
colors["whitesmoke"] = WHITESMOKE
colors["yellow1"] = YELLOW1
colors["yellow2"] = YELLOW2
colors["yellow3"] = YELLOW3
colors["yellow4"] = YELLOW4
colors = OrderedDict(sorted(colors.items(), key=lambda t: t[0]))
