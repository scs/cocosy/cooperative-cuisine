"""
Requires opencv-python.

Install it via `pip install opencv-python`.
"""

import json
from datetime import timedelta
import time
from pathlib import Path

import cv2
import numpy as np

from cooperative_cuisine import ROOT_DIR
from cooperative_cuisine.action import ActionType, Action
from cooperative_cuisine.environment import Environment
from cooperative_cuisine.pygame_2d_vis.drawing import Visualizer, CacheFlags
from cooperative_cuisine.utils import load_config_files


def run_benchmark(environment_config, layout, item_info, visualization_config, grid_size, iterations, cache_flags, do_image_resize=False, image_size=(400, 400),
                  image_interpolation=cv2.INTER_NEAREST, show_image=False, reduced_background=False, verbose=True) -> float:
    env = Environment(
        env_config=environment_config,
        layout_config=layout,
        item_info=item_info,
        as_files=False,
    )
    env.add_player("0")
    env.env_time_end = env.start_time + timedelta(seconds=iterations / 60)

    visualizer: Visualizer = Visualizer(config=visualization_config)
    visualizer.create_player_colors(n=1)
    visualizer.set_grid_size(grid_size=grid_size)
    visualizer.reduced_background = reduced_background

    print("\n") if verbose else ...
    print(f"{cache_flags = }, {iterations = }, {grid_size = }, {do_image_resize = } {image_size if do_image_resize else '' }, {reduced_background = }") if verbose else ...
    durations = []
    duration = 1 / 60
    for i in range(iterations):
        action = Action(
            "0",
            ActionType.MOVEMENT,
            np.array([np.sin(i / 12), np.cos(i / 12)]),
            duration,
        )
        env.perform_action(action)
        env.step(
            timedelta(seconds=duration)
        )

        json_dict = env.get_state(player_id="0")

        time_start = time.perf_counter()
        visualizer.cache_flags = cache_flags
        img = visualizer.get_state_image(state=json_dict)
        if do_image_resize:
            img = cv2.resize(img, image_size, interpolation=image_interpolation)
        durations.append(time.perf_counter() - time_start)

        if show_image:
            cv2.imshow("env", img[:, :, ::-1])
            cv2.waitKey(1)

    mean = np.mean(durations)
    print(f"Mean duration: {mean:.2e}s ({mean:.6f}s)") if verbose else ...
    return mean

if __name__ == "__main__":
    environment_config_path = ROOT_DIR / "configs" / "environment_config.yaml"
    # layout_path: Path = ROOT_DIR / "configs" / "layouts" / "basic.layout"
    layout_path: Path = ROOT_DIR / "reinforcement_learning" / "rl.layout"
    item_info_path = ROOT_DIR / "configs" / "item_info.yaml"
    visualization_config_path = ROOT_DIR / "pygame_2d_vis" / "visualization.yaml"

    item_info, layout, env_config, viz_config = load_config_files(item_info_path, layout_path, environment_config_path, visualization_config_path)
    iterations = 1_000

    # bigger grid_size and resize image.
    means = {}
    all_cache_counters = CacheFlags.BACKGROUND | CacheFlags.COUNTERS
    for grid_size, do_image_resize, image_size in [(48, True, (64,64)), (12.8, False, None)]:
        for reduced_background in [True, False]:
            for cache_flags in list(CacheFlags) + [all_cache_counters, CacheFlags.NONE]:
                mean = run_benchmark(
                    env_config,
                    layout,
                    item_info,
                    viz_config,
                    grid_size,
                    iterations,
                    cache_flags,
                    do_image_resize=do_image_resize,
                    image_size=image_size,
                    show_image=False,
                    reduced_background=reduced_background,
                    verbose=True,
                )
                means[(grid_size, do_image_resize, image_size, reduced_background, cache_flags)] = mean

    minimal = min(means, key=means.get)
    print("\n########################################  Results  ########################################\n")
    print(f"Fastest: do_image_resize={minimal[1]},reduced_background={minimal[3]},cache_flags={minimal[4]} with {means[minimal]}s per image generation")
