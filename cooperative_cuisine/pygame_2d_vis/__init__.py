"""
2D visualization of the Cooperative Cuisine.

You will play the levels accordingly to the study config:
- You can play the Cooperative Cuisine. You can quit the application in the top right or end the level in the bottom right
- The orders are pictured in the top, the current score in the bottom left and the remaining time in the bottom
- The final screen after ending a level shows the score

.. include:: images/images_md_for_docs.md

The keys for the control of the players are:

### Player 1:
- Movement: `W`, `A`, `S`, `D`,
- Pickup/Drop off: `E`
- Interact: `F`
- Swap Players (if configured): `SPACE`

### Player 2:
- Movement: `⬆`, `⬅`, `⬇`, `➡` (arrow keys)
- Pickup/Drop off: `I`
- Interact: `O`
- Swap Players (if configured): `P`

There is also a video game controller support. Just connect it to your PC (USB?) and it should work.
"""
