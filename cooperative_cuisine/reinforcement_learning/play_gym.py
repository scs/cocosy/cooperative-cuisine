import hydra
import stable_baselines3.common.env_checker
from gymnasium.utils.play import play
from omegaconf import DictConfig

from gym_env import EnvGymWrapper, SimpleActionSpace


@hydra.main(version_base="1.3", config_path="config", config_name="rl_config")
def main(cfg: DictConfig):
    """
    Enables steering the agent in the environment used for rl.
    """
    env = EnvGymWrapper(cfg)
    env.render_mode = "rgb_array"
    play(env, keys_to_action={"a": 2, "d": 3, "w": 0, "s": 1, " ": 4, "k": 5}, noop=8)

if __name__ == "__main__":
    main()
