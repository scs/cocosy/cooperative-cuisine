import numpy as np

from cooperative_cuisine.reinforcement_learning.gym_env import StateToObservationConverter


class AdvancedStateConverterArray(StateToObservationConverter):
    """
    Converts an environment state to an Encoding where each counter/item has its unique value
    """

    def __init__(self):
        self.onehot = False
        self.grid_height: int | None = None
        self.grid_width: int | None = None
        self.counter_list = [
            "Empty",
            "Counter",
            "PlateDispenser",
            "TomatoDispenser",
            "OnionDispenser",
            "ServingWindow",
            "PlateReturn",
            "Trashcan",
            "Stove",
            "CuttingBoard",
            "LettuceDispenser",
        ]
        self.player = "0"
        self.item_list = ["None"]

    def setup(self, state, item_info, player="0"):
        """
        Constructor setting basic variables as attributes.

        """
        self.player = player
        self.grid_width, self.grid_height = int(state["kitchen"]["width"]), int(
            state["kitchen"]["height"])
        for key in item_info.keys():
            if item_info[key]["type"] == "Equipment" and "equipment" not in item_info[key].keys():
                continue
            else:
                self.item_list.append(key)

    def convert_state_to_observation(self, state, player=None) -> np.ndarray:

        """
        Convert the environment into an onehot encoding
        Args:
            state: The environment object used
            player: The player for which the state should be converted. If it is none, then the same player as in the
            setup is chosen.


        Returns: An encoding for the environment state

        """
        if player is not None:
            self.player = player
        grid = [[[] for y in range(self.grid_height)] for x in range(self.grid_width)]
        grid_idxs = [(x, y) for x in range(self.grid_width) for y in range(self.grid_height)]

        for counter_key in state["counters"]:
            grid_idx = np.floor(counter_key["pos"]).astype(int)
            counter_item_oh_idx = self.vectorize_item(
                counter_key["occupied_by"], self.item_list
            )
            grid[grid_idx[0]][grid_idx[1]] = counter_item_oh_idx
            counter_oh_idx = self.vectorize_counter(
                counter_key["type"], self.counter_list
            )
            grid_idxs.remove((int(grid_idx[0]), int(grid_idx[1])))
            grid[grid_idx[0]][grid_idx[1]] = np.append(grid[grid_idx[0]][grid_idx[1]], counter_oh_idx)
        player_pos = np.array(state["players"][int(self.player)]["pos"]).astype(int)
        player_dir = np.array(state["players"][int(self.player)]["facing_direction"]).astype(int)
        player_item_idx = self.vectorize_item(
            state["players"][int(self.player)]["holding"], self.item_list
        )
        grid_idxs.remove((int(player_pos[0]), int(player_pos[1])))
        if player_dir[0] == -1:
            # PlayerLeft
            player_val = -1 / 4
        elif player_dir[0] == 1:
            # PlayerRight
            player_val = -2 / 4
        else:
            if player_dir[1] == -1:
                # PlayerUp
                player_val = -3 / 4

            else:
                # PlayerDown
                player_val = -1

        grid[player_pos[0]][player_pos[1]] = player_item_idx
        grid[player_pos[0]][player_pos[1]] = np.append(grid[player_pos[0]][player_pos[1]], player_val)
        for free_idx in grid_idxs:
            grid[free_idx[0]][free_idx[1]] = self.vectorize_item(None, self.item_list)
            grid[free_idx[0]][free_idx[1]] = np.append(grid[free_idx[0]][free_idx[1]], self.counter_list.index("Empty"))
        return np.array(grid).flatten().flatten()

    def vectorize_item(self, item, item_list):

        encoding = []
        if type(item) is list:
            if len(item) == 0:
                item = {"type": "None", "category": "None"}
            else:
                item = item[-1]
        if item is None:
            item = {"type": "None", "category": "None"}
        assert item["type"] in item_list, f"Unknown item {item['type']}."
        encoding.append([self.item_list.index(item["type"])])
        if item["type"] != "None":
            encoding.append([item["progress_percentage"]])
        else:
            encoding.append([0])
        containing_items = []
        if item["category"] == 'ItemCookingEquipment':
            for index in range(len(item["content_list"])):
                assert item['content_list'][index][
                           'type'] in item_list, f"Unknown item {item['content_list'][index]['type']}."
                containing_items.append([self.item_list.index(item["content_list"][index]['type'])])
        while len(containing_items) < 4:
            containing_items.append([0])
        containing_items.sort(reverse=True)
        for item in containing_items:
            encoding.append(item)
        return np.array(encoding).flatten()

    @staticmethod
    def vectorize_counter(counter_name, counter_list):

        assert counter_name in counter_list, f"Unknown Counter {counter_name}"
        counter_oh_idx = counter_list.index("Empty")
        if counter_name in counter_list:
            counter_oh_idx = counter_list.index(counter_name)
        return counter_oh_idx
