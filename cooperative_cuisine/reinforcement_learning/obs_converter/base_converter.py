from collections import deque

import numpy as np

from cooperative_cuisine.counters import CookingCounter, Counter, Dispenser
from cooperative_cuisine.items import CookingEquipment
from cooperative_cuisine.reinforcement_learning.gym_env import StateToObservationConverter


class BaseStateConverter(StateToObservationConverter):
    """
    Converts an environment state to an Encoding where each counter/item has its unique value
    """

    def __init__(self):
        self.onehot = False
        self.grid_height: int | None = None
        self.grid_width: int | None = None
        self.counter_list = [
            "Empty",
            "Counter",
            "PlateDispenser",
            "TomatoDispenser",
            "OnionDispenser",
            "ServingWindow",
            "PlateReturn",
            "Trashcan",
            "Stove",
            "CuttingBoard",
            "LettuceDispenser",
        ]

        self.item_list = [
            "None",
            "Pot",
            "PotOne_Tomato",
            "PotTwo_Tomato",
            "PotThree_Tomato",
            "PotDone_Tomato",
            "PotOne_Onion",
            "PotTwo_Onion",
            "PotThree_Onion",
            "PotDone_Onion",
            "Tomato",
            "Onion",
            "ChoppedTomato",
            "Plate",
            "PlateTomatoSoup",
            "PlateOnionSoup",
            "PlateSalad",
            "Lettuce",
            "PlateChoppedTomato",
            "PlateChoppedLettuce",
            "ChoppedLettuce",
            "ChoppedOnion",
        ]
        self.player = "0"

    def setup(self, state, item_info, player="0"):
        self.player = player
        """
        Constructor setting basic variables as attributes.

        """
        self.grid_width, self.grid_height = int(state["kitchen"]["width"]), int(
            state["kitchen"]["height"])

    def convert_state_to_observation(self, state, player=None) -> np.ndarray:

        """
        Convert the environment into an onehot encoding
        Args:
            state: The environment object used
            player: The player for which the state should be converted. If it is none, then the same player as in the
                        setup is chosen.
        Returns: An encoding for the environment state that is not onehot

        """
        if player is not None:
            self.player = player
        counters = np.zeros(
            (
                self.grid_width,
                self.grid_height,
            ),
            dtype=int,
        )
        grid_idxs = [(x, y) for x in range(self.grid_width) for y in range(self.grid_height)]
        counter_items = np.zeros((self.grid_width, self.grid_height), dtype=int)

        for counter_key in state["counters"]:
            grid_idx = np.floor(counter_key["pos"]).astype(int)

            counter_oh_idx = self.vectorize_counter(
                counter_key["type"], self.counter_list
            )
            counters[grid_idx[0], grid_idx[1]] = counter_oh_idx
            grid_idxs.remove((int(grid_idx[0]), int(grid_idx[1])))

            counter_item_oh_idx = self.vectorize_item(
                counter_key["occupied_by"], self.item_list
            )
            counter_items[grid_idx[0], grid_idx[1]] = (
                counter_item_oh_idx
            )

        for free_idx in grid_idxs:
            counters[free_idx[0], free_idx[1]] = self.counter_list.index("Empty")

        player_pos = np.array(state["players"][int(self.player)]["pos"]).astype(int)
        player_dir = np.array(state["players"][int(self.player)]["facing_direction"]).astype(int)
        player_data = np.concatenate((player_pos, player_dir), axis=0)

        player_item_idx = self.vectorize_item(
            state["players"][int(self.player)]["holding"], self.item_list
        )
        player_item = [player_item_idx]

        final = np.concatenate(
            (
                counters.flatten(),
                counter_items.flatten(),
                player_data.flatten(),
                player_item,
            ),
            axis=0,
        )
        return final

    def vectorize_item(self, item, item_list):
        if item is None:
            item = {"type": "None", "category": "None"}
        if type(item) is list:
            item = item[-1]
        item_name = item["type"]
        if item["category"] == 'ItemCookingEquipment':
            if item["type"] == "Pot":
                if len(item["content_list"]) > 0:
                    if item["content_list"][0]['type'] == "TomatoSoup":
                        item_name = "PotDone_Tomato"
                    if item["content_list"][0]['type'] == "OnionSoup":
                        item_name = "PotDone_Onion"
                    elif len(item["content_list"]) == 1:
                        if item["content_list"][0]['type'] in ["Tomato", "ChoppedTomato"]:
                            item_name = "PotOne_Tomato"
                        if item["content_list"][0]['type'] in ["Onion", "ChoppedOnion"]:
                            item_name = "PotOne_Onion"
                    elif len(item["content_list"]) == 2:
                        if item["content_list"][0]['type'] in ["Tomato", "ChoppedTomato"]:
                            item_name = "PotTwo_Tomato"
                        if item["content_list"][0]['type'] in ["Onion", "ChoppedOnion"]:
                            item_name = "PotTwo_Onion"
                    elif len(item["content_list"]) == 3:
                        if item["content_list"][0]['type'] in ["Tomato", "ChoppedTomato"]:
                            item_name = "PotThree_Tomato"
                        if item["content_list"][0]['type'] in ["Onion", "ChoppedOnion"]:
                            item_name = "PotThree_Onion"
            if item["type"] == "Plate":
                content_list = [it["type"] for it in item["content_list"]]
                match content_list:
                    case ["TomatoSoup"]:
                        item_name = "PlateTomatoSoup"
                    case ["OnionSoup"]:
                        item_name = "PlateOnionSoup"
                    case ["ChoppedTomato"]:
                        item_name = "PlateChoppedTomato"
                    case ["ChoppedLettuce"]:
                        item_name = "PlateChoppedLettuce"
                    case []:
                        item_name = "Plate"
                    case ["ChoppedLettuce", "ChoppedTomato"]:
                        item_name = "PlateSalad"
                    case other:
                        assert False, f"Should not happen. {item}"
        assert item_name in item_list, f"Unknown item {item_name}."
        item_idx = item_list.index(item_name)
        return item_idx

    @staticmethod
    def vectorize_counter(counter_name, counter_list):
        assert counter_name in counter_list, f"Unknown Counter {counter_name}"
        counter_oh_idx = counter_list.index("Empty")
        if counter_name in counter_list:
            counter_oh_idx = counter_list.index(counter_name)
        return counter_oh_idx
