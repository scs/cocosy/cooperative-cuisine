import argparse
from pathlib import Path, PurePath
from cooperative_cuisine import ROOT_DIR


def convert_overcookd_ai_layouts():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--file", dest="inputfile", help="Input file path", required=True
    )
    args = parser.parse_args()
    filepath = PurePath(args.inputfile)
    print(filepath)
    convertion_dict = {
        " ": "_",
        "X": "#",
        "O": "N",
        "T": "T",
        "P": "U",
        "D": "P",
        "S": "$",
        "1": "A",
        "2": "A"
    }

    savepath = Path(ROOT_DIR) / "configs" / "layouts" / "overcooked-ai" / filepath.name
    with open(args.inputfile, "r") as f:
        layoutfile = f.read()
        f.close()
    layout = eval(layoutfile)
    lines = layout["grid"].split("\n")
    additional_info = []
    for key in layout:
        if key != "grid":
            additional_info.append(
                '; {}: {}'.format(key, str(layout[key]).replace("'", "").replace("None", "null")))

        with open(savepath, "w+") as f:
            for line in lines:
                line = line.lstrip()
                for char in line:
                    f.write(convertion_dict[char])
                f.write("\n")
            for info in additional_info:
                f.write(info)
                f.write("\n")
        f.close()


if __name__ == "__main__":
    convert_overcookd_ai_layouts()
