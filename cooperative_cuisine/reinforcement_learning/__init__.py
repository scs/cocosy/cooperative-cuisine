
"""
## Reinforcement Learning Module Overview

The reinforcement learning module consists of several key functions designed to:

- **Train the agent**
- **Test the agent**
- **Utilize the environment**

### Configurations

All hyperparameters related to the reinforcement learning agent and the environment are configurable via specific configuration files. These configurations are managed with **Hydra**, allowing for easy selection of specific config items or entire config files directly from the command line.

### Layouts

Several layouts are predefined in the `cooperative_cuisine/configs/layouts` directory. The layout path can be selected within the corresponding config file.

Additionally, **Overcooked-AI** layouts can be transformed into the cooperative cuisine format using the `convert_overcooked_ai_layout.py` script. To use this script:

1. Specify the path of the Overcooked-AI layout file as a command-line argument.
2. The script will generate the corresponding layout file and save it in the `configs/layouts/overcooked-ai` directory.

"""