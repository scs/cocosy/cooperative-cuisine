import uuid
from argparse import ArgumentParser, FileType

from cooperative_cuisine import ROOT_DIR

DEFAULT_SERVER_URL = "localhost"
"""Default server URL of game and server study."""

DEFAULT_SERVER_PORT = 8080
"""Default study server port."""

DEFAULT_GAME_PORT = 8000
"""Default game server port."""


def study_server_arguments(
    parser: ArgumentParser,
    default_study_port=DEFAULT_SERVER_PORT,
    default_server_url=DEFAULT_SERVER_URL,
):
    """Add arguments for study server ip and port.

    Args:
        parser: the parser to add the arguments.
        default_study_port: the default port of the study server `DEFAULT_STUDY_PORT` -> 8080
        default_server_url: the default ip/url/host of the study server `DEFAULT_SERVER_URL` -> `localhost`
    """
    parser.add_argument(
        "-s",
        "--study-url",
        "--study-host",
        type=str,
        default=default_server_url,
        help=f"Overcooked Study Server host url.",
    )
    parser.add_argument(
        "-sp",
        "--study-port",
        type=int,
        default=default_study_port,
        help=f"Port number for the Study Server",
    )


def game_server_arguments(
    parser: ArgumentParser,
    default_game_port=DEFAULT_GAME_PORT,
    default_server_url=DEFAULT_SERVER_URL,
):
    """Add arguments for game server ip and port.

    Args:
        parser: the parser to add the arguments.
        default_game_port: the default port of the game server `DEFAULT_GAME_PORT` -> 8000
        default_server_url: the default ip/url/host of the game server `DEFAULT_SERVER_URL` -> `localhost`
    """
    parser.add_argument(
        "-g",
        "--game-url",
        "--game-host",
        type=str,
        default=default_server_url,
        help=f"Overcooked Game Server url.",
    )
    parser.add_argument(
        "-gp",
        "--game-port",
        type=int,
        default=default_game_port,
        help=f"Port number for the Game Server",
    )


def disable_websocket_logging_arguments(parser):
    """Disables the logging of WebSocket arguments in the provided parser.

    Args:
        parser: The argument parser object (argparse.ArgumentParser) to which the
            "--enable-websocket-logging" argument will be added.
    """
    parser.add_argument(
        "--enable-websocket-logging" "", action="store_true", default=True
    )


def add_list_of_manager_ids_arguments(parser):
    """This function adds the manager ids argument to the given argument parser.

    Args:
        parser: An ArgumentParser object used to parse command line arguments.
    """
    parser.add_argument(
        "-m",
        "--manager-ids",
        nargs="+",
        type=str,
        default=[uuid.uuid4().hex],
        help="List of manager IDs that can create environments.",
    )


def add_study_arguments(parser):
    """This function adds the study configuration argument to the given argument parser.

    Args:
        parser (argparse.ArgumentParser): The argument parser object.


    Example:
        ```python
        import argparse
        parser = argparse.ArgumentParser()
        add_study_arguments(parser)
        ```
    """
    parser.add_argument(
        "--study-config",
        type=str,
        default=ROOT_DIR / "configs" / "study" / "study_config.yaml",
        # default=ROOT_DIR / "configs" / "study" / "study_config_deterministic_order.yaml",
        help="The config of the study.",
    )


def add_gui_arguments(parser):
    """Adds the gui debug argument to the given argument parser.
        If set, additional debug / admin elements are shown.

    Args:
        parser (argparse.ArgumentParser): The argument parser object.


    Example:
        ```python
        import argparse
        parser = argparse.ArgumentParser()
        add_gui_arguments(parser)
        ```
    """
    parser.add_argument(
        "--do-study",
        help="Start GUI in Fullscreen and do not show configuration and quit buttons.",
        action="store_true",
    )
    parser.add_argument(
        "--debug",
        help="Debug GUI. No need for a study server. Select layouts in GUI. You need to specify a manager id for the game server.",
        action="store_true",
    )


def ssl_argument(parser: ArgumentParser):
    """Add the ssl argument to a parser.

    Args:
        parser: the parser to add the argument.
    """
    parser.add_argument(
        "--ssl",
        action="store_true",
        help="Use SSL certificate. Connect to https and wss.",
    )


def visualization_config_argument(parser: ArgumentParser):
    """Add the visualization config argument to a parser.

    Args:
        parser: the parser to add the argument.
    """
    parser.add_argument(
        "-v",
        "--visualization-config",
        type=FileType("r", encoding="UTF-8"),
        default=ROOT_DIR / "pygame_2d_vis" / "visualization.yaml",
    )


def output_file_argument(parser: ArgumentParser, default_file: str):
    """Add the output file argument to a parser.

    Args:
        parser: the parser to add the argument.
        default_file: the default file name (relative location)
    """
    parser.add_argument(
        "-o",
        "--output-file",
        type=str,
        default=default_file,
    )


def create_replay_parser(parser: ArgumentParser):
    """Add individually arguments for parsers that use the replay function of cooperative cuisine.

    Args:
        parser (ArgumentParser): The argument parser object.
    """
    parser.add_argument("-j", "--json_state", help="Json states file path", type=str)
    visualization_config_argument(parser)
    parser.add_argument(
        "-o",
        "--output",
        type=str,
        default="<json_state_name>",
    )
    parser.add_argument(
        "-p",
        "--player-id",
        type=str,
        default=None,
        help="Render view for specific player",
    )
    parser.add_argument(
        "-g",
        "--grid-size",
        type=int,
        default=40,
        help="Number pixel for one cell in the grid.",
    )
    parser.add_argument(
        "-a",
        "--action-recording",
        type=str,
        default=None,
        help="The path to the action recording",
    )
    parser.add_argument(
        "-e",
        "--env-configs",
        type=str,
        default=None,
        help="The path to the environment config logs",
    )
    parser.add_argument(
        "-s",
        "--step-duration",
        type=float,
        default=1 / 200,
        help="Step duration in seconds between environment steps.",
    )
    parser.add_argument(
        "-f",
        "--fps",
        type=int,
        default=24,
        help="Frames per second to render images from the environment",
    )
    parser.add_argument(
        "-d", "--display", action="store_true", help="Show generated images."
    )
    parser.add_argument(
        "-n",
        "--number-player",
        type=int,
        default=1,
        help="Number of player to visualize. Should be the same as played the game.",
    )
    parser.add_argument(
        "-b",
        "--break-when-no-action",
        action="store_true",
        help="Stop rendering when no more actions are available.",
    )
    parser.add_argument(
        "--video",
        "--video-source",
        type=str,
        help="Create a video from a folder full of images.",
    )


def create_screenshot_parser(parser: ArgumentParser):
    """Add cli arguments for running only the screenshot generation.

    Args:
        parser (ArgumentParser): The argument parser object.
    """
    parser.add_argument(
        "-s",
        "--state",
        type=FileType("r", encoding="UTF-8"),
        default=ROOT_DIR / "pygame_2d_vis" / "sample_state.json",
    )
    visualization_config_argument(parser)
    output_file_argument(parser, ROOT_DIR / "generated" / "screenshot.jpg")


def create_game_server_parser(parser: ArgumentParser):
    """Add cli arguments for running only a game server. For the game-server subcommand.

    Args:
        parser (ArgumentParser): The argument parser object.
    """
    game_server_arguments(parser)
    disable_websocket_logging_arguments(parser)
    add_list_of_manager_ids_arguments(parser)
    ssl_argument(parser)


def create_study_server_parser(parser: ArgumentParser):
    """Add cli arguments for running only a study server. For the study-server subcommand.

    Args:
        parser (ArgumentParser): The argument parser object.
    """
    study_server_arguments(parser)
    # TODO study server can handle several game server
    game_server_arguments(parser)
    add_list_of_manager_ids_arguments(parser=parser)
    add_study_arguments(parser=parser)
    ssl_argument(parser)


def create_gui_parser(parser: ArgumentParser):
    """Add cli arguments for running only a gui instance. For the gui / client subcommand.

    Args:
        parser (ArgumentParser): The argument parser object.
    """
    study_server_arguments(parser)
    disable_websocket_logging_arguments(parser)
    add_list_of_manager_ids_arguments(parser)
    add_gui_arguments(parser)
    ssl_argument(parser)
    game_server_arguments(parser)


def create_normal_parser(parser: ArgumentParser):
    """Add cli arguments for running the servers and one gui instance. For the start subcommand.

    Args:
        parser (ArgumentParser): The argument parser object.
    """
    game_server_arguments(parser)
    study_server_arguments(parser)
    disable_websocket_logging_arguments(parser)
    add_list_of_manager_ids_arguments(parser)
    add_gui_arguments(parser)
    add_study_arguments(parser)
    ssl_argument(parser)


def create_server_parser(parser: ArgumentParser):
    """Add cli arguments for running the servers (game and study but no gui). For the server subcommand.

    Args:
        parser (ArgumentParser): The argument parser object.
    """
    game_server_arguments(parser)
    study_server_arguments(parser)
    disable_websocket_logging_arguments(parser)
    ssl_argument(parser)
    add_list_of_manager_ids_arguments(parser)
    add_study_arguments(parser)
