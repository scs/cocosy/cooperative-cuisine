"""
The actions a player can perform.

The `Action` class is used to represent the incoming action data. There are three types of actions:
- `movement`: Move the player for a specified duration into a direction
- `pick_up_drop`: Based on the situation, pick up or drop off an item from a counter to the players hand or the other way around.
- `interact`: Interact with a counter to increase the progress, e.g., the CuttingBoard to chop ingredients or the Sink to clean plates.

The `action_data` depends on the `action_type`:
- `movement`: a 2d list/array in which direction to move. (Movement vector: complete 360° are allowed, e.g. [0.435889894, 0.9]).
- `pick_up_drop`: None,
- `interact`: `InterActionData` either "keydown" or "keyup" (start and stop the interaction).

The duration part is only needed for the `movement` action. For real-time interactions/games: 1/fps.
"""

from __future__ import annotations

import dataclasses
from enum import Enum

from numpy import typing as npt


class ActionType(Enum):
    """The 3 different types of valid actions. They can be extended via the `Action.action_data` attribute."""

    MOVEMENT = "movement"
    """move the agent."""
    PICK_UP_DROP = "pick_up_drop"
    """interaction type 1, e.g., for pickup or drop off."""
    INTERACT = "interact"
    """interaction type 2, e.g., for progressing. Start and stop interaction via `keydown` and `keyup` actions."""


class InterActionData(Enum):
    """The data for the interaction action: `ActionType.MOVEMENT`."""

    START = "keydown"
    "start an interaction."
    STOP = "keyup"
    "stop an interaction without moving away."


@dataclasses.dataclass
class Action:
    """Action class, specifies player, action type and action itself."""

    player: str
    """Id of the player."""
    action_type: ActionType
    """Type of the action to perform. Defines what action data is valid."""
    action_data: npt.NDArray[float] | list[float] | InterActionData | None
    """Data for the action, e.g., movement vector or start and stop interaction."""
    duration: float | int = 0
    """Duration of the action (relevant for movement)"""

    def __repr__(self):
        return f"Action({self.player},{self.action_type.value},{self.action_data},{self.duration})"

    def __post_init__(self):
        if isinstance(self.action_type, str):
            self.action_type = ActionType(self.action_type)
        if isinstance(self.action_data, str):
            self.action_data = InterActionData(self.action_data)
