"""Type hint classes for the returned data for the post calls."""
from __future__ import annotations

from typing import TYPE_CHECKING

from typing_extensions import TypedDict, Literal

if TYPE_CHECKING:
    from cooperative_cuisine.game_server import PlayerRequestType


class PlayerInfo(TypedDict):
    """Information about a player in an environment."""

    client_id: str
    """ID of the client that controls the player. Used for the websocket url."""
    player_hash: str
    """Hash of the player, for validation."""
    player_id: str
    """ID of the player."""
    websocket_url: str
    """The url for the websocket to connect to."""


class CreateEnvResult(TypedDict):
    """Result of a game server request to create an environment."""

    env_id: str
    """ID of the environment."""
    player_info: dict[str, PlayerInfo]
    """Information about the players."""
    recipe_graphs: list[dict]
    """Graph representation of the recipes possible in this env."""
    kitchen_size: tuple[float, float]
    """Size of the kitchen layout of this env."""


class PlayerRequestResult(TypedDict):
    """Returned message for a processed websocket message."""

    request_type: PlayerRequestType | None
    """The request type of the processed message."""
    status: Literal[200] | Literal[400]
    """The status if the message could be processed correctly."""
    msg: str
    """Returned info message to the client."""
    player_hash: str | None
    """The hash of the player."""
