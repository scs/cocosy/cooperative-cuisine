import argparse
import time
from multiprocessing import Process

from cooperative_cuisine.argument_parser import (
    create_screenshot_parser,
    create_study_server_parser,
    create_game_server_parser,
    create_normal_parser,
    create_gui_parser,
    create_server_parser,
)
from cooperative_cuisine.pygame_2d_vis.video_replay import create_replay_parser


def start_game_server(cli_args):
    from cooperative_cuisine.game_server import main

    main(cli_args.game_url, cli_args.game_port, cli_args.manager_ids)


def start_study_server(cli_args):
    from cooperative_cuisine.study_server import main

    main(
        cli_args.study_url,
        cli_args.study_port,
        game_host=cli_args.game_url,
        game_port=cli_args.game_port,
        manager_ids=cli_args.manager_ids,
        study_config_path=cli_args.study_config,
        use_ssl=cli_args.ssl,
    )


def start_pygame_gui(cli_args):
    from cooperative_cuisine.pygame_2d_vis.gui import main

    main(
        cli_args.study_url,
        cli_args.study_port,
        cli_args.game_url,
        cli_args.game_port,
        cli_args.manager_ids,
        debug=cli_args.debug,
        do_study=cli_args.do_study,
        use_ssl=cli_args.ssl,
    )


def start_server_and_gui(cli_args, no_gui=False):
    study_server = None
    game_server = None
    pygame_gui = None
    try:
        if no_gui or cli_args.do_study or not cli_args.debug:
            print("Start study server:")
            study_server = Process(target=start_study_server, args=(cli_args,))
            study_server.start()
            time.sleep(0.5)

        print("Start game engine:")
        game_server = Process(target=start_game_server, args=(cli_args,))
        game_server.start()
        time.sleep(0.5)
        if no_gui:
            while True:
                time.sleep(1)
        else:
            print("Start PyGame GUI 1:")
            pygame_gui = Process(target=start_pygame_gui, args=(cli_args,))
            pygame_gui.start()

            while pygame_gui.is_alive():
                time.sleep(1)

    except KeyboardInterrupt:
        print("Received Keyboard interrupt")
    finally:
        if study_server is not None and study_server.is_alive():
            print("Terminate study server")
            study_server.terminate()
        if game_server is not None and game_server.is_alive():
            print("Terminate game server")
            game_server.terminate()
        if pygame_gui is not None and pygame_gui.is_alive():
            print("Terminate pygame gui")
            pygame_gui.terminate()
        time.sleep(0.1)


def main(cli_args=None):
    parser = argparse.ArgumentParser(
        prog="Cooperative Cuisine",
        description="CLI control of cooperative cuisine. Start server, guis, execute utility code, etc. Use sub "
        "commands to configure the execution. No commands or arguments results in starting a study "
        "server, game server and a gui.",
        epilog="For further information, see https://scs.pages.ub.uni-bielefeld.de/cocosy/cooperative-cuisine/overcooked_simulator.html",
    )

    subparsers = parser.add_subparsers(
        help="Available CLI of Cooperative Cuisine", dest="command"
    )

    normal_parser = subparsers.add_parser(
        "start",
        help="Start a game server, study server and a PyGame 2D Visualization.",
    )
    create_normal_parser(normal_parser)

    study_server_parser = subparsers.add_parser(
        "study-server", help="Start a study server.", aliases=["study"]
    )
    create_study_server_parser(study_server_parser)

    game_server_parser = subparsers.add_parser(
        "game-server", help="Start a game server.", aliases=["game"]
    )
    create_game_server_parser(game_server_parser)

    gui_parser = subparsers.add_parser(
        "gui", help="Start a PyGame 2D Visualization (a client).", aliases=["client"]
    )
    create_gui_parser(gui_parser)

    screenshot_parser = subparsers.add_parser(
        "screenshot", help="Create a screenshot from a json state.", aliases=["image"]
    )
    create_screenshot_parser(screenshot_parser)

    replay_parser = subparsers.add_parser(
        "replay",
        help="Create replay from json states or recordings.",
        aliases=["video", "video-replay"],
    )
    create_replay_parser(replay_parser)

    server_parser = subparsers.add_parser(
        "server",
        help="Start game and study server.",
        aliases=["game-and-study", "study-and-game"],
    )
    create_server_parser(server_parser)

    cli_args = parser.parse_args()

    if cli_args.command:
        match cli_args.command:
            case "screenshot" | "image":
                from cooperative_cuisine.pygame_2d_vis.drawing import main

                main(cli_args)
            case "study-server" | "study":
                start_study_server(cli_args)
            case "game-server" | "game":
                start_game_server(cli_args)
            case "replay" | "video" | "video-replay":
                from cooperative_cuisine.pygame_2d_vis.video_replay import main

                main(cli_args)
            case "start":
                start_server_and_gui(cli_args)
            case "gui" | "client":
                start_pygame_gui(cli_args)
            case "server" | "game-and-study" | "study-and-game":
                start_server_and_gui(cli_args, no_gui=True)
            case _:
                parser.print_help()
    else:
        default_parser = argparse.ArgumentParser()
        create_normal_parser(default_parser)
        start_server_and_gui(default_parser.parse_args())


if __name__ == "__main__":
    main()
