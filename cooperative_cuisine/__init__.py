"""

This is the documentation of Cooperative Cuisine.

# About the package

The package contains an environment for cooperation between players/agents. A PyGameGUI visualizes the game to
human or virtual agents in 2D. A 3D web-enabled version (for example for online studies, currently under development)
can be found [here](https://gitlab.ub.uni-bielefeld.de/scs/cocosy/godot-overcooked-3d-visualization).

# Background / Literature
The overcooked/cooking domain is a well established cooperation domain/task. There exists
environments designed for reinforcement learning agents as well as the game and adaptations of the game for human
players in a more "real-time"-like environment. They all mostly differ in the visual and graphics dimension. 2D
versions like [overcooked-ai](https://github.com/HumanCompatibleAI/overcooked_ai), [gym-cooking](
https://github.com/rosewang2008/gym-cooking) are most well-known in the community. Besides, the general
adaptations of the original overcooked game. Cooperative Cuisine, we want to bring both worlds together: the
reinforcement learning and real-time playable environment with an appealing visualisation. Enable the potential of
developing artificial agents that play with humans like a "real", cooperative, human partner.

# Installation

You need a Python **3.10** or newer conda environment.
```bash
conda install -c conda-forge pygraphviz
pip install cooperative_cuisine@git+https://gitlab.ub.uni-bielefeld.de/scs/cocosy/cooperative-cuisine@main
```
Or clone it and install it as an editable library which allows you to use all the scripts directly.
```bash
conda install -c conda-forge pygraphviz
git clone https://gitlab.ub.uni-bielefeld.de/scs/cocosy/cooperative-cuisine.git
cd cooperative-cuisine
pip install -e .
```

# Usage / Examples
 Our Cooperative Cuisine is designed for real time interaction but also for reinforcement
learning (gymnasium environment) with time independent step function calls. It focuses on configurability, extensibility and appealing visualization
options.

## Human Player
Run it via the command line (in your pyenv/conda environment):

```bash
cooperative-cuisine start  -s localhost -sp 8080 -g localhost -gp 8000
```

*The arguments shown are the defaults.*

You can also start the **Game Server**, **Study Server** (Matchmaking),and the **PyGame GUI** individually in different terminals.

```bash
cooperative-cuisine game-server -g localhost -gp 8000 --manager-ids SECRETKEY1 SECRETKEY2

cooperative-cuisine study-server -s localhost -sp 8080 -g localhost -gp 8000 --manager-ids SECRETKEY1

cooperative-cuisine gui -s localhost -sp 8080
```

For more CLI arguments and a description of possible arguments, run `cooperative-cuisine -h`

## Connect with agent and receive game state
Or you start a game server, create an environment and connect each player/agent via a websocket connection.

To start a game server see above. Your own manager needs to create an environment.
```python
import requests

from cooperative_cuisine import ROOT_DIR
from cooperative_cuisine.game_server import CreateEnvironmentConfig
from cooperative_cuisine.server_results import CreateEnvResult


with open(ROOT_DIR / "configs" / "item_info.yaml", "r") as file:
    item_info = file.read()
with open(ROOT_DIR / "configs" / "layouts" / "basic.layout", "r") as file:
    layout = file.read()
with open(ROOT_DIR / "configs" / "environment_config.yaml", "r") as file:
    environment_config = file.read()

create_env = CreateEnvironmentConfig(
    manager_id="SECRETKEY1",
    number_players=2,
    environment_settings={"all_player_can_pause_game": False},
    item_info_config=item_info,
    environment_config=environment_config,
    layout_config=layout,
    seed=123456789,
).model_dump(mode="json")

post_result = requests.post("http://localhost:8000/manage/create_env", json=create_env)
if post_result.status_code == 403:
    raise ValueError(f"Forbidden Request: {post_result.json()['detail']}")
env_info: CreateEnvResult = post_result.json()
```

Connect each player via a websocket (threaded or async).
```python
import json
import dataclasses
from websockets.sync.client import connect

from cooperative_cuisine.action import Action, ActionType, InterActionData
from cooperative_cuisine.utils import custom_asdict_factory


p1_websocket = connect("ws://localhost:8000/ws/player/" + env_info["player_info"]["0"]["client_id"])

# set player "0" as ready
p1_websocket.send(json.dumps({"type": "ready", "player_hash": env_info["player_info"]["0"]["player_hash"]}))
assert json.loads(p1_websocket.recv())["status"] == 200, "not accepted player"


# get the state for player "0", call it on every frame/step
p1_websocket.send(json.dumps({"type": "get_state", "player_hash": env_info["player_info"]["0"]["player_hash"]}))
state = json.loads(p1_websocket.recv())

# send an action for player "0"
# --- movement ---
action = Action(
    player="0",
    action_type=ActionType.MOVEMENT,
    action_data=[0.0, 1.0],  # direction (here straight up)
    duration=0.5  # seconds
)
# --- pickup/drop off ---
action = Action(
    player="0",
    action_type=ActionType.PICK_UP_DROP,
    action_data=None,
)
# --- interact ---
action = Action(
    player="0",
    action_type=ActionType.INTERACT,
    action_data=InterActionData.START  # InterActionData.STOP when to stop the interaction
)

p1_websocket.send(json.dumps({
    "type": "action",
    "player_hash": env_info["player_info"]["0"]["player_hash"],
    "action": dataclasses.asdict(
        action, dict_factory=custom_asdict_factory
    ),
}))
p1_websocket.recv()

```

Stop the environment if you want the game to end before the time is up.
```python
requests.post(
    "http://localhost:8000/manage/stop_env",
    json={
        "manager_id": "SECRETKEY1",
        "env_id": env_info["env_id"],
        "reason": "closed environment",
    },
)
```

## Direct integration into your code
You can use the `cooperative_cuisine.environment.Environment` class
and call the `step`, `get_json_state`, and `perform_action` methods directly.

```python
import json
from datetime import timedelta

from cooperative_cuisine import ROOT_DIR
from cooperative_cuisine.action import Action
from cooperative_cuisine.environment import Environment

env = Environment(
    env_config=ROOT_DIR / "configs" / "environment_config.yaml",
    layout_config=ROOT_DIR / "configs" / "layouts" / "basic.layout",
    item_info=ROOT_DIR / "configs" / "item_info.yaml"
)

env.add_player("0")
env.add_player("1")

while True:
    # adapt this to real time if needed with time.sleep etc.
    env.step(timedelta(seconds=0.1))

    player_0_state = json.loads(env.get_json_state("0"))
    if player_0_state["ended"]:
        break

    action = Action(...)  # Please refer to the above code but remember to use np.array instead of list for the movement direction vector
    env.perform_action(action)
```

# JSON State
The JSON schema for the state of the environment for a player can be generated by running the `state_representation.py`
```bash
python state_representation.py
```
Should look like - to have an interactive view, have a look at [jsonschemaviewer](https://navneethg.github.io/jsonschemaviewer/):
```
{"$defs": {"CookingEquipmentState": {"description": "Format of the state representation of cooking equipment.", "properties": {"id": {"title": "Id", "type": "string"}, "category": {"anyOf": [{"const": "Item"}, {"const": "ItemCookingEquipment"}], "title": "Category"}, "type": {"title": "Type", "type": "string"}, "progress_percentage": {"anyOf": [{"type": "number"}, {"type": "integer"}], "title": "Progress Percentage"}, "inverse_progress": {"title": "Inverse Progress", "type": "boolean"}, "active_effects": {"items": {"$ref": "#/$defs/EffectState"}, "title": "Active Effects", "type": "array"}, "content_list": {"items": {"$ref": "#/$defs/ItemState"}, "title": "Content List", "type": "array"}, "content_ready": {"anyOf": [{"$ref": "#/$defs/ItemState"}, {"type": "null"}]}}, "required": ["id", "category", "type", "progress_percentage", "inverse_progress", "active_effects", "content_list", "content_ready"], "title": "CookingEquipmentState", "type": "object"}, "CounterState": {"description": "Format of the state representation of a counter.", "properties": {"id": {"title": "Id", "type": "string"}, "category": {"const": "Counter", "title": "Category"}, "type": {"title": "Type", "type": "string"}, "pos": {"items": {"type": "number"}, "title": "Pos", "type": "array"}, "orientation": {"items": {"type": "number"}, "title": "Orientation", "type": "array"}, "occupied_by": {"anyOf": [{"items": {"anyOf": [{"$ref": "#/$defs/ItemState"}, {"$ref": "#/$defs/CookingEquipmentState"}]}, "type": "array"}, {"$ref": "#/$defs/ItemState"}, {"$ref": "#/$defs/CookingEquipmentState"}, {"type": "null"}], "title": "Occupied By"}, "active_effects": {"items": {"$ref": "#/$defs/EffectState"}, "title": "Active Effects", "type": "array"}}, "required": ["id", "category", "type", "pos", "orientation", "occupied_by", "active_effects"], "title": "CounterState", "type": "object"}, "EffectState": {"description": "Format of the state representation of an effect (fire).", "properties": {"id": {"title": "Id", "type": "string"}, "type": {"title": "Type", "type": "string"}, "progress_percentage": {"anyOf": [{"type": "number"}, {"type": "integer"}], "title": "Progress Percentage"}, "inverse_progress": {"title": "Inverse Progress", "type": "boolean"}}, "required": ["id", "type", "progress_percentage", "inverse_progress"], "title": "EffectState", "type": "object"}, "ItemState": {"description": "Format of the state representation of an item.", "properties": {"id": {"title": "Id", "type": "string"}, "category": {"anyOf": [{"const": "Item"}, {"const": "ItemCookingEquipment"}], "title": "Category"}, "type": {"title": "Type", "type": "string"}, "progress_percentage": {"anyOf": [{"type": "number"}, {"type": "integer"}], "title": "Progress Percentage"}, "inverse_progress": {"title": "Inverse Progress", "type": "boolean"}, "active_effects": {"items": {"$ref": "#/$defs/EffectState"}, "title": "Active Effects", "type": "array"}}, "required": ["id", "category", "type", "progress_percentage", "inverse_progress", "active_effects"], "title": "ItemState", "type": "object"}, "KitchenInfo": {"description": "Format of the state representation of basic information of the kitchen.", "properties": {"width": {"title": "Width", "type": "number"}, "height": {"title": "Height", "type": "number"}}, "required": ["width", "height"], "title": "KitchenInfo", "type": "object"}, "OrderState": {"description": "Format of the state representation of an order.", "properties": {"id": {"title": "Id", "type": "string"}, "category": {"const": "Order", "title": "Category"}, "meal": {"title": "Meal", "type": "string"}, "start_time": {"anyOf": [{"format": "date-time", "type": "string"}, {"type": "string"}], "title": "Start Time"}, "max_duration": {"title": "Max Duration", "type": "number"}, "score": {"anyOf": [{"type": "number"}, {"type": "integer"}], "title": "Score"}}, "required": ["id", "category", "meal", "start_time", "max_duration", "score"], "title": "OrderState", "type": "object"}, "PlayerState": {"description": "Format of the state representation of a player.", "properties": {"id": {"title": "Id", "type": "string"}, "pos": {"items": {"type": "number"}, "title": "Pos", "type": "array"}, "facing_direction": {"items": {"type": "number"}, "title": "Facing Direction", "type": "array"}, "holding": {"anyOf": [{"$ref": "#/$defs/ItemState"}, {"$ref": "#/$defs/CookingEquipmentState"}, {"type": "null"}], "title": "Holding"}, "current_nearest_counter_pos": {"anyOf": [{"items": {"type": "number"}, "type": "array"}, {"type": "null"}], "title": "Current Nearest Counter Pos"}, "current_nearest_counter_id": {"anyOf": [{"type": "string"}, {"type": "null"}], "title": "Current Nearest Counter Id"}}, "required": ["id", "pos", "facing_direction", "holding", "current_nearest_counter_pos", "current_nearest_counter_id"], "title": "PlayerState", "type": "object"}, "ViewRestriction": {"description": "Format of the state representation of a view restriction from the players perspectives.\nCurrently, as a view cone, like a flashlight in the dark.", "properties": {"direction": {"items": {"type": "number"}, "title": "Direction", "type": "array"}, "position": {"items": {"type": "number"}, "title": "Position", "type": "array"}, "angle": {"title": "Angle", "type": "integer"}, "counter_mask": {"anyOf": [{"items": {"type": "boolean"}, "type": "array"}, {"type": "null"}], "title": "Counter Mask"}, "range": {"anyOf": [{"type": "number"}, {"type": "null"}], "title": "Range"}}, "required": ["direction", "position", "angle", "counter_mask", "range"], "title": "ViewRestriction", "type": "object"}}, "description": "The format of the returned state representation.", "properties": {"players": {"items": {"$ref": "#/$defs/PlayerState"}, "title": "Players", "type": "array"}, "counters": {"items": {"$ref": "#/$defs/CounterState"}, "title": "Counters", "type": "array"}, "kitchen": {"$ref": "#/$defs/KitchenInfo"}, "score": {"anyOf": [{"type": "number"}, {"type": "integer"}], "title": "Score"}, "orders": {"items": {"$ref": "#/$defs/OrderState"}, "title": "Orders", "type": "array"}, "ended": {"title": "Ended", "type": "boolean"}, "env_time": {"format": "date-time", "title": "Env Time", "type": "string"}, "remaining_time": {"title": "Remaining Time", "type": "number"}, "view_restrictions": {"anyOf": [{"items": {"$ref": "#/$defs/ViewRestriction"}, "type": "array"}, {"type": "null"}], "title": "View Restrictions"}, "served_meals": {"items": {"maxItems": 2, "minItems": 2, "prefixItems": [{"type": "string"}, {"type": "string"}], "type": "array"}, "title": "Served Meals", "type": "array"}, "info_msg": {"items": {"maxItems": 2, "minItems": 2, "prefixItems": [{"type": "string"}, {"type": "string"}], "type": "array"}, "title": "Info Msg", "type": "array"}}, "required": ["players", "counters", "kitchen", "score", "orders", "ended", "env_time", "remaining_time", "view_restrictions", "served_meals", "info_msg"], "title": "StateRepresentation", "type": "object"}
```


The BaseModel and TypedDicts can be found in `cooperative_cuisine.state_representation`. The
`cooperative_cuisine.state_representation.StateRepresentation` represents the json state that the `get_json_state`
returns.

## Generate images from JSON states
You might have stored some json states and now you want to visualize them via the
pygame-2d visualization. You can do that by running the `drawing.py` script and referencing a json file.
```bash
cooperative-cuisine screenshot --state my_state.json
```
- You can specify a different visualization config with `-v` or `--visualization_config`.
- You can specify the name of the output file with `-o` or `--output_file`. The default is `screenshot.jpg`.

## Generate images/videos from recordings
You can record json states or only the actions and the environment config via hooks and the recording class.

If you want to generate images or complete videos, see `cooperative_cuisine.pygame_2d_vis.video_replay`.

# Configuration

The environment configuration is currently done with 3 config files + GUI configuration.

## Item Config

The item config defines which ingredients, cooking equipment and meals can exist and how meals and processed ingredients
can be cooked/created.

For example

    CuttingBoard:
      type: Equipment

    Stove:
      type: Equipment

    Pot:
      type: Equipment
      equipment: Stove

    Tomato:
      type: Ingredient

    ChoppedTomato:
      type: Ingredient
      needs: [ Tomato ]
      seconds: 4.0
      equipment: CuttingBoard

    TomatoSoup:
      type: Meal
      needs: [ ChoppedTomato, ChoppedTomato, ChoppedTomato ]
      seconds: 6.0
      equipment: Pot


## Layout Config

You can define the layout of the kitchen via a layout file. The position of counters are based on a grid system, even
when the players do not move grid steps but continuous steps. Each character defines a different type of counter. Which
character is mapped to which counter is defined in the Environment config.

For example

```
#QU#FO#TNLB#
@__________M
|__________K
$__________I
#__A____A__D
C__________E
#__________G
C__________#
##PS+#X##S+#
```

## Environment Config

The environment config details how a level/environment is defined. Here, the available plates, meals, order and player
configuration is done.

For example

```yaml
plates:
  clean_plates: 1
  dirty_plates: 2
  plate_delay: [ 5, 10 ]
  # range of seconds until the dirty plate arrives.

game:
  time_limit_seconds: 300
  undo_dispenser_pickup: true
  validate_recipes: true

layout_chars:
  _: Free
  hash: Counter
  A: Agent
  P: PlateDispenser
  C: CuttingBoard
  X: Trashcan
  W: ServingWindow
  S: Sink
  +: SinkAddon
  U: Pot  # with Stove
  T: Tomato

orders:  # how to create orders
  meals:
    all: true
  ...

player_config:
  radius: 0.4
  speed_units_per_seconds: 6
  interaction_range: 1.6
  restricted_view: False
  view_angle: 70
  view_range: 4  # in grid units, can be "null"

effect_manager:  # fire effect
  ...

hook_callbacks:  # scores, recording, msgs, etc.
  ...
```

## PyGame Visualization Config

Here the visualisation for all objects is defined. Reference the images or define a list of base shapes that represent
the counters, ingredients, meals and players. Cooperative Cuisine comes with images for the defined meals.
You can extend it easily. Just have a look at the default [`visualisation.yml`](https://gitlab.ub.uni-bielefeld.de/scs/cocosy/cooperative-cuisine/-/blob/main/cooperative_cuisine/pygame_2d_vis/visualization.yaml?ref_type=heads).
Further, here, you can configure the size of the visualization like screen width, button sizes, fps, colors, etc.

## Study Config

You can setup a study with a study config.
It defines which levels the player will play after they connect to the study server.
Further, you define how many players play together within an environment.

```yaml
levels:
  - config_path: STUDY_DIR/level1/level1_config.yaml
    layout_path: LAYOUTS_DIR/overcooked-1/1-1-far-apart.layout
    item_info_path: STUDY_DIR/level1/level1_item_info.yaml
    name: "Level 1-1: Far Apart"

  - config_path: CONFIGS_DIR/environment_config.yaml
    layout_path: LAYOUTS_DIR/basic.layout
    item_info_path: CONFIGS_DIR/item_info.yaml
    name: "Basic"

  - config_path: STUDY_DIR/level2/level2_config.yaml
    layout_path: LAYOUTS_DIR/overcooked-1/1-4-bottleneck.layout
    item_info_path: STUDY_DIR/level2/level2_item_info.yaml
    name: "Level 1-4: Bottleneck"

num_players: 1
num_bots: 0
```
`STUDY_DIR`, `LAYOUTS_DIR`, `CONFIG_DIR`, etc. will be replaced accordingly to their names.

# Citation

# Structure of the Documentation
The API documentation follows the file and content structure in the repo.
On the left you can find the navigation panel that brings you to the implementation of
- the **action**s the player can perform,
- the **argument parser** definitions for the cli,
- the **counter factory** converts the characters in the layout file to counter instances,
- the **counters**, including the kitchen utility objects like dispenser, cooking counter (stove, deep fryer, oven),
  sink, etc.,
- the **effect manager**, how the fire effect is defined,
- the **environment**, handles the incoming actions and provides the state,
- the **study server**, the match making server,
- the **game server**, which can manage several running environments and can communicates via FastAPI post requests and
websockets,
- the **hooks**, you can adapt and extend recording, scoring, msgs to players, etc. via hooks,
- the **info msgs** to players, based on hook events, short text msgs can be generated,
- the game **items**, the holdable ingredients, cooking equipment, composed ingredients, and meals,
- the calculation of player **movement**,
- the **orders**, how to sample incoming orders and their attributes,
- the **player**/agent, that interacts in the environment,
- the **pygame 2d visualization**, GUI, drawing, and video generation,
- the **recording**, via hooks, actions, environment configs, states, etc. can be recorded in files,
- the **scores**, via hooks, events can affect the scores,
- type hints are defined in **state representation** for the json state and **server results** for the data returned by
the game server in post requests.
- **util**ity code,
- the config **validation** and graph generation.

# License
Cooperative Cuisine © 2024 by [Social Cognitive Systems Group](https://scs.techfak.uni-bielefeld.de/) is licensed under [`CC BY-NC-SA 4.0`](https://creativecommons.org/licenses/by-nc-sa/4.0/?ref=chooser-v1) [![License: CC BY-NC-SA 4.0](https://licensebuttons.net/l/by-nc-sa/4.0/80x15.png)](https://creativecommons.org/licenses/by-nc-sa/4.0/)
"""
import os
from pathlib import Path

ROOT_DIR = Path(os.path.dirname(os.path.abspath(__file__)))  # This is your Project Root
"""A path variable to get access to the layouts coming with the package. For example,
```python 
from cooperative_cuisine import ROOT_DIR

environment_config_path = ROOT_DIR / "configs" / "environment_config.yaml"
```
"""
