# Release Notes

[How to keep the changelog](https://keepachangelog.com/en/1.1.0/):

- `Added` for new features.
- `Changed` for changes in existing functionality.
- `Deprecated` for soon-to-be removed features.
- `Removed` for now removed features.
- `Fixed` for any bug fixes.
- `Security` in case of vulnerabilities.

## [Unreleased]

### Added

### Changed

### Deprecated

### Removed

### Fixed

### Security


##  [1.2.0] (2024-11-28)

### Added
- Single-Agent Reinforcement Learning with stable-baselines3 (vector-based obs)
- Link to Dreamer and FuN RL (with images, single-agent)
- Thumbnails of layouts in pygame 2d debug modus 
- Sequence Diagrams for understanding internal execution flow.
- 2D image generation speed ups. Cache background and basic counters, do other color dimension stacking.
- Reduced background drawing. Only there where no counters stand (if you need that for moving counters you need to change it in the visualization.yaml)
- Visualizer method for getting an image directly via the required image size (only squared images).
- Added benchmark script for benchmarking the drawing time of the 2d visualization.
- Utility function for loading/reading the config files all at once.
- Get image size now returns an already transposed array (opencv style - not pygame style)
- Deterministic Order Generation, opt-in. Instead of random you can define a list of orders that will be generated

### Changed
- Available orders are sorted now (always the same order for the recipe graphs)
- Default config for fire and burning: now slower burning and spreading
- Second continue button on different position. Preventing unintended double clicks

### Deprecated

### Removed

### Fixed
- Recipe graph caching fixes
- Include extra package data files via direct pip install cooperative-cuisine@git...
- Threshold for controller input axis together instead of independent thresholds

### Security


## [1.1.3] (2024-05-07)

### Added
- Added the hook PICK_UP_ON_COOKING_EQUIPMENT for finer distinction of actions
- final license CC BY-NC-SA 4.0

### Changed
- Added more infos to many hooks to better track the difference of before and after the hook.
- more hook changes

### Deprecated

### Removed

### Fixed
- Fixed config values of serving not ordered meals

### Security

## [1.1.2] (2024-04-12)

### Added

- Recipe graphs contain the names of the interactive counters and equipment
- Random agent also gets the recipe graph information.
- Hook SCORE_CHANGED when the score is increased.

### Changed

- `CooperativeCuisine` occurrences to `Cooperative Cuisine`
- Changed `overcooked-simulator` slug in urls to `cooperative-cuisine`
- Adjusted study settings.

### Deprecated

### Removed

### Fixed

- Random Order generation does not depend on other sampling of random numbers.

### Security

## [1.1.1] (2024-04-03)

### Added

- More Docstrings

### Changed

- No keyboard tutorial. Only the controller is described. In the future, add config option which to show.
- Study Doc is now a .jsonl file.
- Other default processing time.

### Deprecated

### Removed

### Fixed

### Security

## [1.1.0] (2024-03-27)

Updates and fixes resulted from v1. New cli, test coverage, different languages and much more.

### Added

- Send full websocket url in player_info.
- ">90"% code coverage in tests
- i18n for the gui
- Controller hot-plugging
- Hook when returning items to dispensers
- Displaying image of served meals on game conclusion screen
- Pathfinding in random agent
- Level layouts from 2d-grid-overcooked-literature
- Caching of graph recipe layouts
- Score label changes color when score changes
- Scoring optionally dependent on remaining times of orders
- Score of order is shown in game
- Possible max score is shown next to recipe graphs
- Control screenshot, replay, single game/study server, gui via cli (sub commands)

### Changed

- cli control changed `cooperative_cuisine` is now `cooperative-cuisine` (`-` instead of `_`)
- cli now uses commands. Use `cooperative-cuisine start` now for the same behaviour as before (add arguments).
- `cooperative-cuisine` with arguments does not work anymore. The "no arguments" call results in running with defaults.
  If you want to add arguments use the `start` subcommand
- `cooperative-cuisine -h` provides an overview of all sub commands. For individual help for each coomand,
  run `cooperative-cuisine COMMAND -h`.
- `extra_setup_functions` now only contain hook callback classes (`hook_callbacks`). Therefore, the `func` and `kwargs`
  fields were
  removed and the kwargs are now the standard values for defined hooks. This reduced the complexity of the config.
- The study config can now overwrite environment config attributes. This reduces the need to copy environment configs
  for each level in the config.
- Most time comparisons (countdowns, etc.) are now compared with `<=` and not `<`. This is handy for testing and
  definition.
- Player config in the environment class is now a dataclass and not a dict. The content remains the same. Just the
  access changes from dict access to normal object like access.
- Some type hint additions
- Better drawing of orders, now in a pygame_gui UIImage
- Buttons for setting player controls in the GUI disappear depending on number of players
- Icon for serving window, now a star
- Additional state content is stored in own variable. Is no longer passed via a kwarg to the step function.

### Deprecated

### Removed

### Fixed

- Orders are sampled correctly again
- Orders with no time limit, sample_on_serving=true works again

### Security

## [1.0.0] (2024-03-08)

Release of v1. Now we move the development process to incremental updates with use of the changelog and semantic
versioning.
_Cooperative Cuisine_ **v1** is feature complete. It contains a usable _Overcooked!_-like environment, game server,
study server, 2D visualization, etc.

### Added

- continuous-like movement and pushing other players around
- extending recording, scoring, msgs via hooks at different programm points
- implemented random-action agent
- players connect via websockets
- 2D visualization with pygame
- Everything is configurable with config files: item info (ingredients, meals, etc.), layout, environment,
  visualization, study
- Game server that handles active environments
- Study environment with study server and study config. Does the match making
- For study participants: simple control tutorial, recipe guides, end screen.
- Controller support

### Security

- To manage environments on the game server, the "manager" needs to have a valid `manager_id` which is registered in the
  game server.
- Registered players (humans and agents) need to connect to websockets which were preregistered with a `client_id`.
- Each player actions need to reference a `player_hash` that should only be known by the player / client that
  controls the player.

## 0.1.0 (2023-11-23)

- Created