
# Authors
- Florian Schröder - https://gitlab.ub.uni-bielefeld.de/florian.schroeder
- Fabian Heinrich - https://gitlab.ub.uni-bielefeld.de/fheinrich
- Annika Österdiekhoff - https://gitlab.ub.uni-bielefeld.de/annika
- Dominik Battefeld - https://gitlab.ub.uni-bielefeld.de/dominik.battefeld
- Christoph Kowalski - https://gitlab.ub.uni-bielefeld.de/ckowalski 
