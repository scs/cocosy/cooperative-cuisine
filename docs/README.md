## Additional Documentation

The main documentation is done vie pdoc. It extracts the doc strings from the library and creates a running website via gitlab pages.
You can find it here: [Documentation](https://scs.pages.ub.uni-bielefeld.de/cocosy/cooperative-cuisine).

In this directory, you can find additional material for understanding cooperative cuisine and its code.

### Sequence Diagrams

To understand the workflow / messages between components, sequence diagrams provide an easy to follow structure to represent code execution.
We provide a source file and an image file for each sequence diagram. You can use the source file to edit and plot the diagram at https://sequencediagram.org/.

For the interaction between Study and Game Server and one or more clients (e.g., the pygame gui client) in a study setup, we designed the *"Cooperative Cuisine Study Setup Network Messages"* diagram.

The *"Cooperative Cuisine Environment classes interaction"* shows the interaction between the Environment class and the relevant other classes (Player, Counter, Item, ...) during *each step* and when a new action instruction is called.
